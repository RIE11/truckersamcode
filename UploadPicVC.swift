//
//  UploadPicVC.swift
//  TruckerSam
//
//  Created by Monika on 12/26/16.
//  Copyright © 2016 Monica. All rights reserved.
//

import Foundation
import UIKit
import Alamofire



class UploadPicVC: BaseVC
{
    @IBOutlet weak var headerBackgroundView:UIView!
    @IBOutlet weak var headerLogoView:UIImageView!
    @IBOutlet weak var profileImgView:UIImageView!
    @IBOutlet weak var clickHereLbl:UILabel!
    @IBOutlet weak var agreeTermsBtn:UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.findHamburguerViewController()?.gestureEnabled = false // To disable side view bar        //set background color
        headerBackgroundView.backgroundColor = Constant.GlobalConstants.kColor_ScreenBackground
        self.view.backgroundColor = Constant.GlobalConstants.kColor_ScreenBackground
        
      
        // Round Rect of userimage
        profileImgView.layer.cornerRadius = profileImgView.frame.size.width/2
        profileImgView.clipsToBounds = true
        profileImgView.image = UIImage(named:"placeholder_UserImg")

        //add action on imageview
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(ChooseProfileImageAction))
        profileImgView.isUserInteractionEnabled = true
        profileImgView.addGestureRecognizer(tapGestureRecognizer)
        
    }
    
    @IBAction func ContinueAction(sender: AnyObject)
    {
        if agreeTermsBtn.imageView?.image == UIImage(named:"uncheck_cb")
        {
            self.AlertViewOneButtonPress(ButtonTitle: "OK", message: "Please select terms & condition first")
        }
        else
        {
            UpdateUserImageApi()
        }
    }
    
    
    func UpdateUserImageApi()
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let parameter1 = [ "userId":"\(self.GetIntegerUserDefault(ForKey: "UserID"))"] as [String : String]
        
        let URL = try! URLRequest(url: Constant.GlobalConstants.k_baseUrl+Constant.GlobalConstants.k_UpdateImageApi, method: .post, headers: nil)
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(UIImagePNGRepresentation(self.profileImgView.image!)!, withName: "image", fileName: "image.png", mimeType: "image/png")
            // import parameters
            for (key, value) in parameter1
            {
                multipartFormData.append((value as String).data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!, withName: key)
            }
            
        }, with: URL, encodingCompletion: {
            encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                   
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    print(response)
                    let json = response.result.value as? NSDictionary
                    
                    if (json?["status"] as! String == "success")
                    {
                        let defaults = UserDefaults.standard
                        defaults.set(json?["image"] as! String, forKey: "UserImage")
                        
                        
                        self.SetIntegerUserDefault(ForKey:"isTermsCondtion", ForValue:1)
                        
                        let viewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
                        self.navigationController?.pushViewController(viewControllerObj!, animated: true)
                        
                    }
                    debugPrint("SUCCESS RESPONSE: \(response)")
                }
            case .failure(let encodingError):
                // hide progressbas here
                print("ERROR RESPONSE: \(encodingError)")
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)

            }
        })
        
   
    }
    
    
    @IBAction func AgreeTermsAction(sender: UIButton)
    {
        if agreeTermsBtn.imageView?.image == UIImage(named:"uncheck_cb")
        {
            agreeTermsBtn.setImage(UIImage(named:"checked_cb"), for: UIControlState.normal)
        }else{
            agreeTermsBtn.setImage(UIImage(named:"uncheck_cb"), for: UIControlState.normal)
        }
    }
    
    func ChooseProfileImageAction()
    {
         buttonOpenCameraActionSheet()
    }
    
    //UIImagePickerControllerDelegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        {
            let rect = CGRect(x:0 , y:0 , width:image.size.width/6 , height:image.size.height/6)
            UIGraphicsBeginImageContext(rect.size)
            image.draw(in: rect)
            let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            let compressedImageData = UIImageJPEGRepresentation(resizedImage!, 0.1)
            profileImgView.image = UIImage(data: compressedImageData!)!
            clickHereLbl.isHidden = true
        }
        else
        {
            print("Something went wrong")
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        picker.dismiss(animated: true, completion: nil)
    }
   
    
    @IBAction func TermsConditionAction(sender: UIButton)
    {
        let webViewObj = self.storyboard?.instantiateViewController(withIdentifier: "WebViewVC") as? WebViewVC
        webViewObj?.titleStr = "Terms & Condition"
        webViewObj?.linkStr = Constant.GlobalConstants.k_WebViewTermsString
        self.navigationController?.pushViewController(webViewObj!, animated: true)
    }
}
