//
//  TruckerPicsVC.m
//  DemoMap
//
//  Created by Monica on 2/10/15.
//  Copyright (c) 2015 Monica. All rights reserved.
//

#import "TruckerPicsVC.h"
#import "TruckerSam_Connection.h"

#define baseUrl                         @"https://www.truckersam.com/webservice/"
#define SaveImageLike                   @"save_image_like"
#define GetAllImage                     @"get_all_uploaded_images"

@interface TruckerPicsVC ()
@property(nonatomic,retain)IBOutlet UITableView *tblImagelist;
@property(nonatomic)NSIndexPath *indexPathSel;
@property (nonatomic) BOOL loading;
@property(nonatomic)int tag;
@property(nonatomic)int pageNumber;
@property(nonatomic,weak)IBOutlet UILabel *titleLbl;

@property(nonatomic,retain)NSMutableArray *allImages;

@property(nonatomic,retain)IBOutlet UIImageView *headerImg;
@property(nonatomic,retain)IBOutlet UIButton *menuBtn;
@property(nonatomic,retain)IBOutlet UIButton *backBtn;
@property(nonatomic,retain)IBOutlet UIImageView *menuImg;
@property(nonatomic,retain)IBOutlet UIImageView *backImg;
@end

@implementation TruckerPicsVC

-(NSMutableArray *)allImages
{
    if (!_allImages) {
        _allImages = [NSMutableArray new];
    }
    return _allImages;
}

- (void)viewDidLoad
{
    UITabBar *tabBar = self.tabBarController.tabBar;
    UITabBarItem *tabBarItem1 = [tabBar.items objectAtIndex:0];
    UITabBarItem *tabBarItem2 = [tabBar.items objectAtIndex:1];
    UITabBarItem *tabBarItem3 = [tabBar.items objectAtIndex:2];
    UITabBarItem *tabBarItem4 = [tabBar.items objectAtIndex:3];
    
    tabBarItem1.image = [[UIImage imageNamed:@"upload"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    tabBarItem2.image = [[UIImage imageNamed:@"my-pics"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    tabBarItem3.image = [[UIImage imageNamed:@"pics"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    tabBarItem4.image = [[UIImage imageNamed:@"win_cup"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    tabBarItem1.selectedImage = [[UIImage imageNamed:@"h2"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    tabBarItem2.selectedImage = [[UIImage imageNamed:@"h3"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    tabBarItem3.selectedImage = [[UIImage imageNamed:@"h4"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    tabBarItem4.selectedImage = [[UIImage imageNamed:@"h1"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    
    [super viewDidLoad];
  
    _titleLbl.font=[UIFont fontWithName:@"Oswald-Regular" size:26];
   

    [self.tblImagelist registerNib:[UINib nibWithNibName:@"TruckerPicsCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"filterCell"];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    _pageNumber=0;

    [self.allImages removeAllObjects];

    //set garbage value for tag
    _tag=27345435;
    
    HUD=[MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
    [self GetALlUploadedImage];
}



#pragma mark-Back Action
- (IBAction)BackAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark-Menu Action


-(void)GetALlUploadedImage
{
    self.loading=NO;

    int userID = [[NSUserDefaults standardUserDefaults] stringForKey:@"UserID"].intValue;
    
    NSDictionary *parameters = @{@"user_id":(userID)?[NSString stringWithFormat:@"%i",userID]:@"",
                                 @"page" :[NSString stringWithFormat:@"%i",_pageNumber]
                                 };
    
    [[TruckerSam_Connection sharedManager]getDataForUrl:[baseUrl stringByAppendingString:GetAllImage] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"%@",responseObject);
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"failure"])
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"No image found" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
             [alert show];
         }
         else{
             NSArray* allImgesData = [responseObject objectForKey:@"images"];
             
             for (int i=0; i<[allImgesData count];i++)
             {
                 ImageData * imagedata = [ImageData new];
                 [imagedata updateValuesWithDictionary:allImgesData[i]];
                 imagedata.imageDownloaded = [[AsyncImageLoader sharedLoader].cache objectForKey:imagedata.imageURL];
                 [self.allImages addObject:imagedata];
             }
         }
         _pageNumber=[[responseObject objectForKey:@"nextPageCount"] integerValue];
         
         [_tblImagelist reloadData];
         
         [HUD setHidden:YES];
     }
      failure:^(AFHTTPRequestOperation *operation, NSError *error){
          [HUD setHidden:YES];
          NSLog(@"Enter into failure is %@",error.localizedDescription);
          UIAlertView  *alert=nil;
          alert=[[UIAlertView alloc]initWithTitle:@"Error!" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
          [alert show];
      }];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark-tableview delegate and datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_tblImagelist)
    {
        if (indexPath.row ==self.allImages.count)
        {
            return 70;
        }
        else
        {
            ImageData * imagedata = self.allImages[indexPath.row];
            if (imagedata)
            {
                UIImage * image = imagedata.imageDownloaded;
                if (image)
                {
                    float scale = (image.size.width/self.tblImagelist.frame.size.width);
                    
                    float height = image.size.height/scale;
                    
                    float defaultHeight = 184;
                    float heightDiff = height - defaultHeight;
                    
                    height = 360 + heightDiff;
                    
                   // NSLog(@"index %d \n height %f \n image size = %@", indexPath.row, height, NSStringFromCGSize(image.size));
                    
                    return MAX(height, 360);
                }
            }
            
            return 360;
        }
    }else
    {
        return 50;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        if (self.allImages.count==0)
        {
            return 0;
        }else
        {
            if (_pageNumber!=0)
            {
                return self.allImages.count + 1;
            }else{
                return self.allImages.count;
            }
        }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row ==self.allImages.count && _pageNumber!=0)
    {
        return [self loadingCell];
    }
    else
    {
        static NSString *CellIdentifier = @"filterCell";
        
        //====================CUSTOM CELL WITH VIDEO=============================
        TruckerPicsCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        [cell.btnLike addTarget:self action:@selector(LikeAction:) forControlEvents:UIControlEventTouchUpInside];

        [cell.imgUploaded setDelegate:self];

        ImageData * currentData = self.allImages[indexPath.row];
        
        cell.lblUserName.text    =   currentData.username;
        cell.lblTime.text        =   [self HourCalculation:currentData.uploadTime];
        
        NSString *str = [NSString stringWithFormat:@"%@",currentData.caption];
        cell.lblCaption.text         =  [str stringByReplacingOccurrencesOfString:@"\\'" withString:@"'"];
        cell.lbllikeUserCount.text   =   [NSString stringWithFormat:@"%@ Trukers like this",currentData.allLikeCount];
        cell.imgUploaded.image = nil;
        [cell.imgUploaded setImageURL:currentData.imageURL indexpath:indexPath];
        
        cell.indexPath = indexPath;
        
        if ([currentData.userImage isEqualToString:@""] || [currentData.userImage length]==0)
        {
            cell.imgUser.image=[UIImage imageNamed:@"picDefault.png"];
        }
        else{
            cell.imgUser.imageURL=[NSURL URLWithString:currentData.userImage];
        }
        
        if ([currentData.like isEqualToString:@"yes"])
        {
            cell.btnLike.userInteractionEnabled=NO;
            cell.btnLike.imageView.image=[UIImage imageNamed:@"like-active"];
        }
        else
        {
            if (indexPath.row==_tag)
            {
                cell.btnLike.userInteractionEnabled=NO;
                cell.btnLike.imageView.image=[UIImage imageNamed:@"like-active"];
            }
            else{
                cell.btnLike.userInteractionEnabled=YES;
                cell.btnLike.imageView.image=[UIImage imageNamed:@"like-inactive"];
            }
        }
        
        if ([currentData.userId integerValue]==[[NSUserDefaults standardUserDefaults] stringForKey:@"UserID"].intValue)
        {
            cell.imgShare.hidden=NO;
        }else{
            cell.imgShare.hidden=YES;
        }
        
        cell.btnLike.tag=indexPath.row;
        
        //Hide winner image and label
        [cell.imgWinner setHidden:YES];
        [cell.lblWinner setHidden:YES];
        
        
        [cell setNeedsLayout];
        
        return cell;
    }
}


- (void)TestMethod:(AsyncImageConnection *)asyncimg image:(UIImage *)image
{
    ImageData * imagedata = self.allImages[asyncimg.indexPath.row];
    imagedata.imageDownloaded = image;

    NSArray* allvisibleCells = [self.tblImagelist indexPathsForVisibleRows];
    if ([allvisibleCells containsObject:asyncimg.indexPath])
    {
        [_tblImagelist reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}


#pragma mark- Scrolling Delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    float endScrolling = scrollView.contentOffset.y + scrollView.frame.size.height;
    if (endScrolling >= scrollView.contentSize.height)
    {
        if (!self.loading && _pageNumber!=0)
        {
            self.loading=YES;
            [self performSelector:@selector(GetALlUploadedImage) withObject:nil afterDelay:0];
        }
    }
}

- (UITableViewCell *)loadingCell
{
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:nil];
    UIActivityIndicatorView *activityIndicator =[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    activityIndicator.center = cell.center;
    [cell addSubview:activityIndicator];
    
    UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(0,40,320,20)];
    lbl.textColor=[UIColor whiteColor];
    lbl.textAlignment=NSTextAlignmentCenter;
    lbl.font=[UIFont systemFontOfSize:14];
    lbl.text=@"loading more images..";
    [cell addSubview:lbl];
    
    [activityIndicator startAnimating];
    cell.backgroundColor=[UIColor clearColor];
    //cell.tag = kLoadingCellTag;
    return cell;
}


#pragma mark-Like Action
-(void)LikeAction:(UIButton *)sender
{
    _tag=[sender tag];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_tag inSection:0];
    _indexPathSel=indexPath;
    
    HUD=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    int userID = [[NSUserDefaults standardUserDefaults] stringForKey:@"UserID"].intValue;
    
    ImageData * imageData  = self.allImages[_tag];
    
    NSDictionary *parameters = @{@"user_id":(userID)?[NSString stringWithFormat:@"%i",userID]:@"",
                                 @"image_id":imageData.imageId,
                                 @"like":@"yes",};
    
    [[TruckerSam_Connection sharedManager]getDataForUrl:[baseUrl stringByAppendingString:SaveImageLike] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"%@",responseObject);
         [sender setUserInteractionEnabled:NO];
         [sender setImage:[UIImage imageNamed:@"like-active"] forState:UIControlStateNormal];
         
         imageData.allLikeCount = [responseObject objectForKey:@"total_like_count"];
         
         NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
         [_tblImagelist reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
         
         [HUD setHidden:YES];
     }
        failure:^(AFHTTPRequestOperation *operation, NSError *error){
            [HUD setHidden:YES];
            NSLog(@"Enter into failure is %@",error.localizedDescription);
            UIAlertView  *alert=nil;
            alert=[[UIAlertView alloc]initWithTitle:@"Error!" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }];
}

-(NSString*)HourCalculation:(NSString*)PostDate
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormat setLocale:[NSLocale systemLocale]];
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [dateFormat setTimeZone:gmt];
    NSDate *ExpDate = [dateFormat dateFromString:PostDate];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSDayCalendarUnit|NSWeekCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit|NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit) fromDate:ExpDate toDate:[NSDate date] options:0];
    NSString *time;
    if(components.year!=0)
    {
        if(components.year==1)
        {
            time=[NSString stringWithFormat:@"%ld year",(long)components.year];
        }
        else
        {
            time=[NSString stringWithFormat:@"%ld years",(long)components.year];
        }
    }
    else if(components.month!=0)
    {
        if(components.month==1)
        {
            time=[NSString stringWithFormat:@"%ld month",(long)components.month];
        }
        else
        {
            time=[NSString stringWithFormat:@"%ld months",(long)components.month];
        }
    }
    else if(components.week!=0)
    {
        if(components.week==1)
        {
            time=[NSString stringWithFormat:@"%ld week",(long)components.week];
        }
        else
        {
            time=[NSString stringWithFormat:@"%ld weeks",(long)components.week];
        }
    }
    else if(components.day!=0)
    {
        if(components.day==1)
        {
            time=[NSString stringWithFormat:@"%ld day",(long)components.day];
        }
        else
        {
            time=[NSString stringWithFormat:@"%ld days",(long)components.day];
        }
    }
    else if(components.hour!=0)
    {
        if(components.hour==1)
        {
            time=[NSString stringWithFormat:@"%ld hour",(long)components.hour];
        }
        else
        {
            time=[NSString stringWithFormat:@"%ld hours",(long)components.hour];
        }
    }
    else if(components.minute!=0)
    {
        if(components.minute==1)
        {
            time=[NSString stringWithFormat:@"%ld min",(long)components.minute];
        }
        else
        {
            time=[NSString stringWithFormat:@"%ld mins",(long)components.minute];
        }
    }
    else if(components.second>=0)
    {
        if(components.second==0)
        {
            time=[NSString stringWithFormat:@"1 sec"];
        }
        else
        {
            time=[NSString stringWithFormat:@"%ld secs",(long)components.second];
        }
    }
    return [NSString stringWithFormat:@"%@ ago",time];
}

@end

@implementation ImageData

-(void)updateValuesWithDictionary:(NSDictionary*)values
{
    self.userId     = values[@"user_id"];
    self.username   = values[@"username"];
    self.userImage  = values[@"user_image"];
    self.caption    = values[@"caption"];
    self.image      = values[@"image"];
    self.allLikeCount = values[@"all_like_count"];
    self.like       = values[@"like"];
    self.imageId    = values[@"image_id"];
    self.uploadTime = values[@"upload_time"];
    self.imageURL   = [NSURL URLWithString:self.image];
}

@end
