//
//  MyPicsVC.h
//  DemoMap
//
//  Created by Monica on 2/13/15.
//  Copyright (c) 2015 Monica. All rights reserved.
//


#import "TruckerPicsCell.h"
#import "AsyncImageView.h"
#import "MBProgressHUD.h"

@interface MyPicsVC : UIViewController<PicsImageViewSizeDelegate>
{
   MBProgressHUD *HUD; 
}
- (IBAction)BackAction:(id)sender;


@end

@interface ImageUserPicsData : NSObject

@property (nonatomic, retain) NSString* username;
@property (nonatomic, retain) NSString* userImage;
@property (nonatomic, retain) NSString* caption;
@property (nonatomic, retain) NSString* image;
@property (nonatomic, retain) NSURL* imageURL;
@property (nonatomic, retain) NSString* uploadTime;
@property (nonatomic, retain) NSString* allLikeCount;
@property (nonatomic, retain) NSString* like;
@property (nonatomic, retain) NSString* imageId;
@property (nonatomic, assign) UIImage* imageDownloaded;
@property (nonatomic, retain) NSString* userId;

-(void)updateValuesWithDictionary:(NSDictionary*)values;

@end
