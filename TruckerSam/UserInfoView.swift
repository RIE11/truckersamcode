//
//  UserInfoView.swift
//  TruckerSam
//
//  Created by Rini Mital on 1/30/17.
//  Copyright © 2017 Monica. All rights reserved.
//

import UIKit
import PASImageView

class UserInfoView: UIView
{
    @IBOutlet weak var placePhoto: PASImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
}
