//
//  HiringHalfCell.m
//  DemoMap
//
//  Created by Monica on 2/2/15.
//  Copyright (c) 2015 Monica. All rights reserved.
//

#import "HiringHalfCell.h"


@implementation HiringHalfCell

- (void)awakeFromNib {
    // Initialization code
    
    _webJobDescp.scrollView.scrollEnabled = NO;
    _webJobDescp.scrollView.bounces = NO;
    
    //APPLYING CUSTOM FONT
   /* [self applyCustomFontToAllLabelsOfView:self];
    _lblCompanyName.font=[UIFont RobotoLightFontWithSize:_lblCompanyName.font.pointSize];
    _lblJobType.font=[UIFont RobotoLightFontWithSize:_lblJobType.font.pointSize];
    _lblJobLocation.font=[UIFont RobotoLightFontWithSize:_lblJobLocation.font.pointSize]; */
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark-Apply Custom Font
/*-(void)applyCustomFontToAllLabelsOfView:(UIView*)view
{
    for (UIView* v in view.subviews) {
        if ([v isKindOfClass:[UILabel class]]) {
            [(UILabel*)v setFont:[UIFont OswaldRegularFontWithSize:((UILabel*)v).font.pointSize]];
        }
        if ([v isKindOfClass:[UITextView class]]) {
            [(UITextView*)v setFont:[UIFont RobotoLightFontWithSize:((UITextField*)v).font.pointSize]];
        }
        if (v.subviews.count) {
            [self applyCustomFontToAllLabelsOfView:v];
        }
    }
} */

@end
