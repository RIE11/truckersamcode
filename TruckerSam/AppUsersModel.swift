//
//  AppUsersModel.swift
//  TruckerSam
//
//  Created by Monika on 1/2/17.
//  Copyright © 2017 Monica. All rights reserved.
//

import Foundation
import UIKit

class AppUsersModel:NSObject
{
    var App_UserIdStr           :String!
    var App_UserNameStr         :String!
    var App_UserImgStr          :String!
    var App_UserPhoneStr        :String!
}
