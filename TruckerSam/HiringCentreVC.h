//
//  HiringCentreVC.h
//  DemoMap
//
//  Created by Monica on 2/2/15.
//  Copyright (c) 2015 Monica. All rights reserved.
//

#import "HiringCell.h"
#import "HiringHalfCell.h"
#import "MBProgressHUD.h"
@class WebViewVC;
@interface HiringCentreVC : UIViewController
{
    NSMutableDictionary *expandedIndexes;
    BOOL isLastindex;
    MBProgressHUD *HUD;
}

@property(nonatomic,retain)IBOutlet HiringCell* customCell;
@property(nonatomic,retain)IBOutlet HiringHalfCell* customHalfCell;

- (IBAction)logoAction:(id)sender;
- (IBAction)BackAction:(id)sender;
- (IBAction)ReadMoreAction:(id)sender;
- (IBAction)CloseAction:(id)sender;
- (IBAction)WebsiteAction:(id)sender;
- (IBAction)FilterAction:(id)sender;
- (IBAction)CrossAction:(id)sender;

- (IBAction)StateBaseAction:(id)sender;
- (IBAction)DoneAction:(id)sender;
- (IBAction)CancelAction:(id)sender;

- (IBAction)MenuBtnAction:(id)sender;
@end
