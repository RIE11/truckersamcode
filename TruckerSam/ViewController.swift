//
//  ViewController.swift
//  TruckerSam
//
//  Created by Gurleen Singh on 12/26/16.
//  Copyright © 2016 Gurleen Singh. All rights reserved.
//

import UIKit
import Contacts
import GoogleMaps
import Alamofire
import UserNotifications
import SKMaps
import PASImageView

class ViewController: BaseVC,EPPickerDelegate, SKMapViewDelegate, SKRoutingDelegate, SKNavigationDelegate
{
    @IBOutlet weak var mapView: GMSMapView?
    @IBOutlet weak var blackView: UIView?

    @IBOutlet weak var headerBackgroundView:UIView!
    @IBOutlet weak var headerLogoView:UIImageView!
    
    
    @IBOutlet weak var navigationTopBackgroundView:UIView!
    @IBOutlet weak var navigationBottomBackgroundView:UIView!
   
    @IBOutlet var navigationTopToLabel:UILabel!
    @IBOutlet var navigationTopInstructionLabel:UILabel!

    @IBOutlet var navigationBottomDistanceLabel:UILabel!
    @IBOutlet var navigationBottomDurationLabel:UILabel!

    @IBOutlet var navigationGoButton:UIButton!

    var cLLocationManger: CLLocationManager!
    let dataProvider = GoogleDataProvider()
    var SideMenuView = SideMenuViewController()
    var infoView = MarkerInfoView()
    var userInfoView = UserInfoView()

    let searchRadius: Double = 3000
    var searchedTypes = [String]()
  
    var phoneNumberInfoView = String()
    let path = GMSMutablePath()
    
    
    var directionsAPI:                 PXGoogleDirections!
    var waypoints: [PXLocation]   =      [PXLocation]()
    var routeIndex: Int           =     0
    
    var destinationCoordinates    =     CLLocationCoordinate2D()
    var originCoordinates         =     CLLocationCoordinate2D()

    var marker = GMSMarker()
    

    var seletedMarkerAddress  = NSString()
    var seletedMarkerLogo     = NSString()
    var seletedMarkerName     = NSString()
    var seletedMarkerType     = String()
    var seletedMarkerImage     = String()

    var UpView = PASImageView()
    var markerView = UIImageView()

    
    var skMapView: SKMapView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        ///set background color
        headerBackgroundView.backgroundColor = Constant.GlobalConstants.kColor_ScreenBackground
        self.view.backgroundColor = Constant.GlobalConstants.kColor_ScreenBackground
        self.findHamburguerViewController()?.menuDirection = .right
        self.findHamburguerViewController()?.gestureEnabled = true // To enable side view bar
        toShowGoogleMap()
        toConfigureLocation()
        
        /// Define identifier
        let notificationNameGoogle = Notification.Name("NotificationGoogle")
        let notificationNameServer = Notification.Name("NotificationServer")
        let notificationNameBuddies = Notification.Name("NotificationBuddies")


        /// Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.methodOfReceivedNotificationGoogle), name: notificationNameGoogle, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.methodOfReceivedNotificationServer), name: notificationNameServer, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.methodOfReceivedNotificationBuddies), name: notificationNameBuddies, object: nil)
       
        directionEnabled = false
        navigationTopBackgroundView.isHidden = true
        navigationBottomBackgroundView.isHidden = true
        navigationGoButton.isHidden = true
        if UserDefaults.standard.object(forKey: "NearByBuddies") as? Bool == true
        {
            self.methodOfReceivedNotificationBuddies()
        }
        
    }

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
  
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //TODO: Bottom Button Actions
    
    @IBAction func actionContactsButton(_ sender: AnyObject)
    {
        let viewController = self.storyboard!.instantiateViewController(withIdentifier: "EPContactsPicker") as! EPContactsPicker
        self.navigationController?.pushViewController(viewController, animated: true)
    }
   
    
    @IBAction func actionTruckButton(_ sender: AnyObject)
    {
        let truckObj = self.storyboard?.instantiateViewController(withIdentifier: "TruckAppControllerVC") as? TruckAppControllerVC
        self.navigationController?.pushViewController(truckObj!, animated: true)

    }
    
    @IBAction func actionMessageButton(_ sender: AnyObject)
    {
        let webViewObj = self.storyboard?.instantiateViewController(withIdentifier: "NotificationCenterVC") as? NotificationCenterVC
        self.navigationController?.pushViewController(webViewObj!, animated: true)
    }
    
    //TODO: Menu Button Action
    @IBAction func actionLayersButton(_ sender: AnyObject)
    {
        if let hamburguerViewController = self.findHamburguerViewController()
        {
            if  hamburguerViewController.menuVisible
            {
                hamburguerViewController.hideMenuViewControllerWithCompletion(nil)
            }
            else
            {
                hamburguerViewController.showMenuViewController()
            }
            
        }
        
        
    }
    
    //TODO: To Setup Map View
    func toConfigureLocation()
    {
        cLLocationManger                           = CLLocationManager()
        cLLocationManger.requestWhenInUseAuthorization()
        cLLocationManger.delegate                  = self
        cLLocationManger.distanceFilter            = kCLDistanceFilterNone
        cLLocationManger.desiredAccuracy           = kCLLocationAccuracyBest
        cLLocationManger.startUpdatingLocation()
        
      
        
        self.mapView!.camera = GMSCameraPosition(target: (cLLocationManger.location?.coordinate)!, zoom: 17, bearing: 0, viewingAngle: 0)
        
   
         self.marker.icon = UIImage(named: "user_position")!
         self.marker.map = mapView
         self.marker.isTappable = false
         self.marker.position = (cLLocationManger.location?.coordinate)!

        var point = mapView?.projection.point(for: self.marker.position)
        point?.y = (point?.y)! - 150
        let camera = GMSCameraUpdate.setTarget((mapView?.projection.coordinate(for: point!))!)
        mapView?.animate(with: camera)

    }
    //TODO: To Setup Google
    func toShowGoogleMap()
    {
        self.mapView!.settings.compassButton      = true
        self.mapView!.settings.myLocationButton   = true
        self.mapView!.isMyLocationEnabled         = false
        self.mapView!.delegate                    = self
        self.mapView!.mapType                     = kGMSTypeNormal
        self.mapView!.isTrafficEnabled            = false
        self.mapView!.settings.rotateGestures     = true
        self.mapView!.settings.zoomGestures       = true
        self.mapView!.isBuildingsEnabled          = false
        self.mapView!.settings.tiltGestures       = true
    }
    
    //TODO: To Set marker from Google
    func fetchNearbyPlaces(_ coordinate: CLLocationCoordinate2D)
    {
        self.mapView!.clear()
        MBProgressHUD.showAdded(to: self.view, animated: true)

        dataProvider.fetchPlacesNearCoordinate(coordinate, radius:searchRadius, types: searchedTypes)
        { places in
            //print(places)
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)

            self.path.removeAllCoordinates()
            for place: GooglePlace in places
            {
                let marker = PlaceMarker(place: place)
                marker.map = self.mapView
                self.path.add(CLLocationCoordinate2DMake(place.coordinate.latitude, place.coordinate.longitude))
            }
            self.marker.map = self.mapView
            let bounds = GMSCoordinateBounds(path: self.path)
            self.mapView!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 40.0))
            self.methodOfReceivedNotificationBuddies()

        }
    }
    //TODO: To Set marker from Server

    func fetchNearbyPlacesFromServer(_ coordinate: CLLocationCoordinate2D)
    {
        self.mapView!.clear()
        MBProgressHUD.showAdded(to: self.view, animated: true)

        dataProvider.fetchPlacesNearCoordinateFromServer(coordinate, radius:searchRadius, types: searchedTypes)
        { places in
            //print(places)
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)

            self.path.removeAllCoordinates()
            for place: GooglePlace in places
            {
                let marker = PlaceMarker(place: place)
                marker.map = self.mapView
                self.path.add(CLLocationCoordinate2DMake(place.coordinate.latitude, place.coordinate.longitude))
            }
            self.marker.map = self.mapView
            let bounds = GMSCoordinateBounds(path: self.path)
            self.mapView!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 40.0))
            self.methodOfReceivedNotificationBuddies()
        }
    }
    
    //TODO: To Set marker from Near By users

    func fetchNearbyBuddiesFromServer(_ coordinate: CLLocationCoordinate2D)
    {
        //NearByBuddies
        MBProgressHUD.showAdded(to: self.view, animated: true)
        dataProvider.fetchPlacesNearBuddiesFromServer(coordinate, radius:searchRadius, types: ["NearByBuddies"])
        { places in
            
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
            for place: GooglePlace in places
            {
                let marker = PlaceMarker(place: place)
                marker.map = self.mapView
           
            }
            self.marker.map = self.mapView
      
        }
    }
    func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D)
    {
        //let geocoder = GMSGeocoder()
        
        // geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
        //self.addressLabel.unlock()
        // if let address = response?.firstResult() {
        //  let lines = address.lines as! [String]
        //self.addressLabel.text = lines.joined(separator: "\n")
        
        //let labelHeight = self.addressLabel.intrinsicContentSize.height
        // self.mapView.padding = UIEdgeInsets(top: self.topLayoutGuide.length, left: 0, bottom: labelHeight, right: 0)
        
        // UIView.animate(withDuration: 0.25, animations: {
        //  self.pinImageVerticalConstraint.constant =((labelHeight - self.topLayoutGuide.length) * 0.5)
        // self.view.layoutIfNeeded()
        // })
        // }
        // }
    }
    
    
    //TODO: method of notifications
   
    func methodOfReceivedNotificationGoogle()
    {
        searchedTypes = [SelectedName]
        fetchNearbyPlaces(originCoordinates)
        
    }
    
    func methodOfReceivedNotificationServer()
    {
       
        searchedTypes = [SelectedName]
        fetchNearbyPlacesFromServer(originCoordinates)


    }
    func methodOfReceivedNotificationBuddies()
    {
        if SelectedName == ""
        {
            self.mapView!.clear()
            self.marker.map = self.mapView
        }
       
        let defaults = UserDefaults.standard
        if defaults.object(forKey: "NearByBuddies") as? Bool == true
        {
            fetchNearbyBuddiesFromServer(originCoordinates)
        }
        
    }
    //TODO:To close info popup
    @IBAction func closeInfoview(_ sender: AnyObject)
    {
        self.infoView.removeFromSuperview()
        self.userInfoView.removeFromSuperview()
        self.blackView?.isHidden = true
    }
    //TODO:On click button call from info popup
    @IBAction func callFromInfoview(_ sender: AnyObject)
    {
        if self.phoneNumberInfoView == ""
        {
        }
        else
        {
        let url:NSURL = NSURL(string: "tel://" + phoneNumberInfoView)!
        UIApplication.shared.openURL(url as URL)
        }
    }
    //TODO:On click button get direction from info popup
    @IBAction func getDirections(_ sender: AnyObject)
    {
       if directionEnabled == false
       {
        self.directionAPI(originLat: self.originCoordinates.latitude, originLong: self.originCoordinates.longitude, destinationLat: self.destinationCoordinates.latitude, destinationLong: self.destinationCoordinates.longitude)
        
 
        navigationTopToLabel.text = self.seletedMarkerName as String
       
       }
        else
        {
        
        self.AlertViewOneButtonPress(ButtonTitle: "OK", message: "Please close navigation if you want to go to" + "\(seletedMarkerAddress)")
        
        }
    }
   
    //TODO: to get direction from info popup
    func directionAPI(originLat: CLLocationDegrees, originLong :CLLocationDegrees ,destinationLat: CLLocationDegrees, destinationLong :CLLocationDegrees)
    {
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
       
        directionsAPI = PXGoogleDirections(apiKey: Constant.GlobalConstants.kGoogleApiKey)
        directionsAPI.delegate = self
        directionsAPI.from = PXLocation.coordinateLocation(CLLocationCoordinate2D(latitude: originLat, longitude: originLong))
        directionsAPI.to = PXLocation.coordinateLocation(CLLocationCoordinate2D(latitude: destinationLat, longitude: destinationLong))
        directionsAPI.alternatives = true
        directionsAPI.optimizeWaypoints = false
        directionsAPI.waypoints = []
        directionsAPI.calculateDirections
            { (response) -> Void in
                DispatchQueue.main.async(execute: { () -> Void in
                    switch response
                    {
                    case let .error(_, error):
                        
                        let alert = UIAlertController(title:"", message:"\(error.localizedDescription)", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                    case let .success(_, routes):
                       
                        self.infoView.removeFromSuperview()
                        self.userInfoView.removeFromSuperview()
                        self.blackView?.isHidden = true
                        directionEnabled = true
                        self.navigationTopBackgroundView.isHidden = false
                        self.navigationBottomBackgroundView.isHidden = false
                        self.navigationGoButton.isHidden = false

                        
                        for i in 1 ..< (routes).count
                        {
                            if i != self.routeIndex
                            {
                                routes[i].drawOnMap(self.mapView!, strokeColor: UIColor.lightGray, strokeWidth: 3.0)
                                
                            }
                        }
                        
                        let step = routes[0].legs[0].steps[0]
                        let leg = routes[0].legs[0]
                       
                        if let instr = step.rawInstructions
                        {
                            print(instr)
                            self.navigationTopInstructionLabel.text = instr
                        }
                        
                        if let dist = step.distance?.description, let dur = step.duration?.description
                        {
                            print(dist)
                            print(dur)
                        }
                        if let dist = leg.distance?.description, let dur = leg.duration?.description
                        {
                            print(dist)
                            print(dur)
                            self.navigationBottomDistanceLabel.text = dist
                            self.navigationBottomDurationLabel.text = dur

                        }
                        
                        
                        self.mapView!.animate(with: GMSCameraUpdate.fit(routes[self.routeIndex].bounds!, withPadding: 40.0))
                        
                        routes[self.routeIndex].drawOnMap(self.mapView!, strokeColor: UIColor.init(red: (0.0/255.0), green: (181.0/255.0), blue: (249.0/255.0), alpha: 1.0), strokeWidth: 4.0)

                    }
                })
        }
        
        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
    }
    
    // TODO: Go button functionality
    @IBAction func goButtonClicked(_ sender: AnyObject)
    {
        navigationGoButton.isHidden = true
        skMapView = SKMapView(frame:(mapView?.frame)!)
        skMapView.delegate = self
        skMapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        self.view.insertSubview(skMapView, aboveSubview: self.mapView!)
        
        skMapView.settings.showCurrentPosition = true
        SKRoutingService.sharedInstance().mapView = skMapView
        SKRoutingService.sharedInstance().routingDelegate = self
        SKRoutingService.sharedInstance().navigationDelegate = self
        
        
        DispatchQueue.global().async
            {
                if self.seletedMarkerType == "NearByBuddies"
                {
                    let markerImage = UIImage(named: "ballon")!.withRenderingMode(.alwaysOriginal)
                    self.markerView.frame = CGRect(x:0, y:0 , width: 35 , height:50)
                    self.markerView.image = markerImage
                    self.UpView.frame = CGRect(x:5, y:5 , width: 25 , height:25)
                    self.UpView.progressColor = UIColor.clear
                    self.UpView.backgroundProgressColor = UIColor.white
                    self.UpView.imageURL(URL: NSURL(string:self.seletedMarkerImage)! as URL)
                    self.UpView.layer.cornerRadius = self.UpView.frame.size.width/2
                    self.UpView.clipsToBounds = true
                }
                else if self.seletedMarkerType == "FromServer"
                {
                    let markerImage = UIImage(named: "boxBg")!.withRenderingMode(.alwaysOriginal)
                    self.markerView.frame = CGRect(x:0, y:0 , width: 35 , height:50)
                    self.markerView.image = markerImage
                    self.UpView.frame = CGRect(x:1, y:1 , width: 33 , height:39)
                    self.UpView.progressColor = UIColor.clear
                    self.UpView.backgroundProgressColor = UIColor.clear
                    self.UpView.backgroundColor = UIColor.white
                    self.UpView.imageURL(URL: NSURL(string: SelectedURL)! as URL)
                }
                else
                {
                    let markerImage = UIImage(named: "ballon")!.withRenderingMode(.alwaysOriginal)
                    self.markerView.frame = CGRect(x:0, y:0 , width: 35 , height:50)
                    self.markerView.image = markerImage
                    self.UpView.frame = CGRect(x:5, y:5 , width: 25 , height:25)
                    self.UpView.progressColor = UIColor.clear
                    self.UpView.backgroundProgressColor = UIColor.clear
                    self.UpView.imageURL(URL: NSURL(string: SelectedURL)! as URL)
                    
                }
                DispatchQueue.main.async
                    {
                        let DynamicView=UIView(frame:CGRect(x:0, y:0 , width: 35 , height:50))
                        
                        DynamicView.addSubview(self.markerView)
                        DynamicView.insertSubview( self.UpView, aboveSubview: self.markerView)
                        //create the SKAnnotationView
                        let view = SKAnnotationView(view: DynamicView, reuseIdentifier: "viewID")
                        //create the annotation
                        let viewAnnotation = SKAnnotation()
                        viewAnnotation.annotationView = view
                        viewAnnotation.identifier = 100
                        viewAnnotation.location = self.destinationCoordinates
                        self.skMapView.addAnnotation(viewAnnotation, with: SKAnimationSettings())
                        
                        
                        let route: SKRouteSettings = SKRouteSettings()
                        route.startCoordinate = self.originCoordinates
                        route.destinationCoordinate = self.destinationCoordinates
                        route.shouldBeRendered = true// If NO, the route will not be rendered.
                        route.requestAdvices = true
                        route.maximumReturnedRoutes = 1
                        route.requestExtendedRoutePointsInfo = true
                        SKRoutingService.sharedInstance().calculateRoute(route)
                        
                        let settings: SKAdvisorSettings = SKAdvisorSettings()
                        let currentLanguage: String = "en_us"
                        settings.advisorVoice = currentLanguage
                        SKRoutingService.sharedInstance().advisorConfigurationSettings = settings
                        
                        let navSettings: SKNavigationSettings = SKNavigationSettings()
                        //navSettings.navigationType = SKNavigationType.real
                        navSettings.navigationType = SKNavigationType.simulation
                        navSettings.distanceFormat = SKDistanceFormat.milesFeet
                        self.skMapView?.settings.displayMode = SKMapDisplayMode.mode3D
                        navSettings.showStreetNamePopUpsOnRoute = true
                        SKRoutingService.sharedInstance().startNavigation(with: navSettings)
                       
                }
        }

        
    }
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        //AudioService.sharedInstance().cancel()
        SKRoutingService.sharedInstance().stopNavigation()
        SKRoutingService.sharedInstance().clearCurrentRoutes()
        
    }
    // TODO: Close navigation functionality
    @IBAction func closeNavigationView(_ sender: AnyObject)
    {
        
        //AudioService.sharedInstance().cancel()
        SKRoutingService.sharedInstance().stopNavigation()
        SKRoutingService.sharedInstance().clearCurrentRoutes()
        skMapView.removeFromSuperview()
        
        
        directionEnabled = false
        navigationTopBackgroundView.isHidden = true
        navigationBottomBackgroundView.isHidden = true
        navigationGoButton.isHidden = true

            if SelectedParentId == ""
            {
                self.methodOfReceivedNotificationBuddies()
            }
            else
            {
                
                if SelectedParentId == "86"
                {
                    self.methodOfReceivedNotificationGoogle()
                }
                else
                {
                    self.methodOfReceivedNotificationServer()
                }
                
        }
    }

//TODO: Get Marker details from webservice
   
    func toGetDetailsForServer(marker : GMSMarker, completionHandler: @escaping (NSDictionary) -> ())
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        //https://www.truckersam.com/webservice/get_branch_details?branch_id=1679&longitude=-89.8764102314&latitude=35.9422551675&user_id=1219&user_id=1219
        
        let placeMarker = marker as! PlaceMarker
        
        WebServicesVC.sharedInstance.CallGetAPIFunction(urlString: Constant.GlobalConstants.K_GetBranchDetail, parameter:"user_id=\(self.GetIntegerUserDefault(ForKey: "UserID"))" + "&" + "branch_id=\(placeMarker.place.place_id!)" + "&" + "latitude=\(placeMarker.place.coordinate.latitude)" + "&" + "longitude=\(placeMarker.place.coordinate.longitude)"  , success: {
            
            
            (responseObject) in
            
            //print(responseObject!)
            
            if (responseObject?["status"] as! String == "success")
            {
                
                if let Json = responseObject?["branch"] as?  NSDictionary
                {
                    self.destinationCoordinates = placeMarker.place.coordinate
                    /*{"branch":{"around_me_logo":"1484643407130375.png","id":"1679","advertiser_id":"4529","category_id":"10","branch_name":"Rest Area","address":"","longitude":"-89.8764102314","latitude":"35.9422551675","premium_listing":"no","free_text":"","phone":"","discount":"","website":"","rate":"4","advertiser_logo":"yes","avg_rating":null,"userRateValue":null,"distance":"0","logo":"https:\/\/www.truckersam.com\/uploaded_images\/logos\/thumb\/1426714871147780.png","marker_logo":"https:\/\/www.truckersam.com\/uploaded_images\/appimages\/appbanner\/thumb\/1484643407130375.png"},"status":"success","message":"success"}*/
                    
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    completionHandler(Json)
                    
                    
                }
                
            }
        else
            {
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)

            
            }})
        { (NSError) in
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
        }
    }
    
    func toGetDetailsForGoogle(marker : GMSMarker, completionHandler: @escaping (NSDictionary) -> ())
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        //https://maps.googleapis.com/maps/api/place/details/json?placeid="
        
        let placeMarker = marker as! PlaceMarker
        
        WebServicesVC.sharedInstance.CallGetAPIFunctionWithoutBaseURL(urlString: "https://maps.googleapis.com/maps/api/place/details/json", parameter: "placeid=\(placeMarker.place.place_id!)" + "&" + "key=\(Constant.GlobalConstants.kGoogleApiKey)" , success: {
            
            (responseObject) in
            
            //print(responseObject!)
            
            if (responseObject?["status"] as! String == "OK")
            {
                
                if let Json = responseObject?["result"] as?  NSDictionary
                {
                     self.destinationCoordinates = placeMarker.place.coordinate
                    /* {
                     "html_attributions" =     (
                     );
                     result =     {
                     "address_components" =         (
                     {
                     "long_name" = "Himalaya Marg";
                     "short_name" = "Himalaya Marg";
                     types =                 (
                     route
                     );
                     },
                     {
                     "long_name" = 22B;
                     "short_name" = 22B;
                     types =                 (
                     "sublocality_level_2",
                     sublocality,
                     political
                     );
                     },
                     {
                     "long_name" = "Sector 22";
                     "short_name" = "Sector 22";
                     types =                 (
                     "sublocality_level_1",
                     sublocality,
                     political
                     );
                     },
                     {
                     "long_name" = Chandigarh;
                     "short_name" = Chandigarh;
                     types =                 (
                     locality,
                     political
                     );
                     },
                     {
                     "long_name" = Chandigarh;
                     "short_name" = Chandigarh;
                     types =                 (
                     "administrative_area_level_2",
                     political
                     );
                     },
                     {
                     "long_name" = Chandigarh;
                     "short_name" = CH;
                     types =                 (
                     "administrative_area_level_1",
                     political
                     );
                     },
                     {
                     "long_name" = India;
                     "short_name" = IN;
                     types =                 (
                     country,
                     political
                     );
                     },
                     {
                     "long_name" = 160022;
                     "short_name" = 160022;
                     types =                 (
                     "postal_code"
                     );
                     }
                     );
                     "adr_address" = "SCO 1076-77, <span class=\"street-address\">Himalaya Marg</span>, <span class=\"extended-address\">22B, Sector 22</span>, <span class=\"locality\">Chandigarh</span>, <span class=\"postal-code\">160022</span>, <span class=\"country-name\">India</span>";
                     "formatted_address" = "SCO 1076-77, Himalaya Marg, 22B, Sector 22, Chandigarh, 160022, India";
                     "formatted_phone_number" = "1800 425 5556";
                     geometry =         {
                     location =             {
                     lat = "30.7328774";
                     lng = "76.77809689999999";
                     };
                     viewport =             {
                     northeast =                 {
                     lat = "30.73295695";
                     lng = "76.77829490000001";
                     };
                     southwest =                 {
                     lat = "30.73263875";
                     lng = "76.77803089999999";
                     };
                     };
                     };
                     icon = "https://maps.gstatic.com/mapfiles/place_api/icons/atm-71.png";
                     id = bf4440397ec191cc52b07c7babed58107fddb4c4;
                     "international_phone_number" = "+91 1800 425 5556";
                     name = "Punjab & Sind Bank ATM";
                     "place_id" = ChIJlZ2SvKftDzkRWukCovWdQ1c;
                     reference = "CmRRAAAARbmMKUr_-LXCK-7FyVHqJr_smg1HS8QAgJJKClViAOdPkJmGNXhJMVYe65wHNSxXwEXo9rP-LfjYvx1hwOb3NW_mMnweaM9Y57AX5oF9orRu7LcKs4U2NfK_hjbyhIV3EhDzmYyMeEq7_ad5ddBj47ysGhRKfJ5jIGhqKNf63s-uQ3p1NEyI5g";
                     scope = GOOGLE;
                     types =         (
                     atm,
                     finance,
                     "point_of_interest",
                     establishment
                     );
                     url = "https://maps.google.com/?cid=6288043183049992538";
                     "utc_offset" = 330;
                     vicinity = "SCO 1076-77, Himalaya Marg, 22B, Sector 22, Chandigarh";
                     website = "http://www.psbindia.com";
                     };
                     status = OK;
                     }
 */
                    
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    completionHandler(Json)
                    
                    
                }
                
            }
        else
            {
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)

            }})
        { (NSError) in
           
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
        }
    }
    
    
    //TODO: Conversion

    func RadiansToDegrees(radians: Double) -> Double {
        return radians * 180.0/M_PI
    }
    
    func DegreesToRadians(degrees: Double) -> Double {
        return degrees * M_PI / 180.0
    }
    func imageRotatedByDegrees(degrees: CGFloat, image: UIImage) -> UIImage{
        let size = image.size
        
        UIGraphicsBeginImageContext(size)
        let context = UIGraphicsGetCurrentContext()
        
        context!.translateBy(x: 0.5*size.width, y: 0.5*size.height)
        context!.rotate(by: CGFloat(DegreesToRadians(degrees: Double(degrees))))
        
        image.draw(in: CGRect(origin: CGPoint(x: -size.width*0.5, y: -size.height*0.5), size: size))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    //TODO: locationManager Delegates

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse
        {
            
            cLLocationManger.startUpdatingLocation()
            cLLocationManger.startUpdatingHeading()
            
        }
    }
    
    //TODO: - To Get Current Location on google map
    override func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        originCoordinates = (locations.first?.coordinate)!
        
        if let location = locations.first
        {
            self.marker.position = location.coordinate
            cLLocationManger.stopUpdatingLocation()
        }

        
//        //TODO: Getting from firebase
//        geoFire.getLocationForKey("tracksamWithUserId1114", withCallback: { (location, error) in
//            if (error != nil) {
//                print("An error occurred getting the location for \"firebase-hq\": \(error?.localizedDescription)")
//            } else if (location != nil) {
//                print("Location for \"firebase-hq\" is [\(location?.coordinate.latitude), \(location?.coordinate.longitude)]")
//                //self.long.text = "\(location.coordinate.longitude)"
//                //self.lat.text = "\(location.coordinate.latitude)"
//            } else {
//                print("GeoFire does not contain a location for \"firebase-hq\"")
//            }
//        })
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error while updating location " + error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        
        let direction = -newHeading.trueHeading as Double
        self.marker.icon = self.imageRotatedByDegrees(degrees: CGFloat(direction), image: UIImage(named: "user_position")!)
    }
    
}



/// TODO: - GMSMapViewDelegate
extension ViewController: GMSMapViewDelegate
{
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition)
    {
        reverseGeocodeCoordinate(position.target)
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool)
    {
        
        if (gesture)
        {
            self.mapView!.selectedMarker = nil
        }
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoContents marker: GMSMarker) -> UIView?
    {
        let placeMarker = marker as! PlaceMarker
        
        if (placeMarker.place.selectedType == "FromServer")
        {
             infoView = Bundle.main.loadNibNamed("MarkerInfoView", owner: self, options: nil)?.first! as! MarkerInfoView
            self.toGetDetailsForServer(marker: marker) { Json in
                
                let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
                let underlineAttributedString = NSAttributedString(string:  (Json["branch_name"] as? String)!, attributes: underlineAttribute)
                
                self.infoView.nameLabel.attributedText = underlineAttributedString
                self.seletedMarkerName =  (Json["branch_name"] as? NSString)!
               
                self.seletedMarkerType = placeMarker.place.selectedType!
                self.infoView.addressLabel.text = Json["address"] as? String
                self.seletedMarkerAddress = self.infoView.addressLabel.text as NSString
                let contentSize = self.infoView.addressLabel.sizeThatFits(self.self.infoView.addressLabel.bounds.size)
                var frame = self.infoView.addressLabel.frame
                frame.size.height = contentSize.height
                self.infoView.addressLabel.frame = frame
                
                let aspectRatioTextViewConstraint = NSLayoutConstraint(item: self.infoView.addressLabel, attribute: .height, relatedBy: .equal, toItem: self.infoView.addressLabel, attribute: .width, multiplier: self.infoView.addressLabel.bounds.height/self.infoView.addressLabel.bounds.width, constant: 1)
                self.infoView.addressLabel.addConstraint(aspectRatioTextViewConstraint)
                

                self.infoView.phoneLabel.text = Json["phone"] as? String
                self.phoneNumberInfoView = self.infoView.phoneLabel.text!
                self.infoView.floatRatingView.emptyImage = UIImage(named: "ic_star_empty")
                self.infoView.floatRatingView.fullImage = UIImage(named: "ic_star_full")
                self.infoView.floatRatingView.contentMode = UIViewContentMode.scaleAspectFit
                self.infoView.floatRatingView.maxRating = 5
                self.infoView.floatRatingView.minRating = 0
                self.infoView.floatRatingView.rating = 5
                self.infoView.floatRatingView.editable = false
                self.infoView.floatRatingView.halfRatings = true
                self.infoView.floatRatingView.floatRatings = false
                self.seletedMarkerLogo = (Json["logo"] as? NSString)!
                //self.infoView.placePhoto.cacheEnabled = false
                self.infoView.placePhoto.progressColor = UIColor.clear
                self.infoView.placePhoto.backgroundProgressColor = UIColor.clear
                
                self.infoView.placePhoto.imageURL(URL: NSURL(string:self.seletedMarkerLogo as String)! as URL)

                self.infoView.center = CGPoint(x: self.view.bounds.midX, y: self.view.bounds.midY)
                
                
                var  frameInfo : CGRect = self.infoView.frame
                
                frameInfo.size = CGSize(width: 260 , height:self.infoView.addressLabel.frame.origin.y+self.infoView.addressLabel.frame.size.height+150)
                
                self.infoView.frame = frameInfo
                self.view?.addSubview(self.infoView)
                self.blackView?.isHidden = false
                
            }
        }
        else if (placeMarker.place.selectedType == "FromGoogle")
        {
             infoView = Bundle.main.loadNibNamed("MarkerInfoView", owner: self, options: nil)?.first! as! MarkerInfoView
           
            self.toGetDetailsForGoogle(marker: marker) { Json in
                
                let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
                let underlineAttributedString = NSAttributedString(string:  (Json["name"] as? String)!, attributes: underlineAttribute)
                self.seletedMarkerName = (Json["name"] as? NSString)!

                self.seletedMarkerType = placeMarker.place.selectedType!

                
                self.infoView.nameLabel.attributedText = underlineAttributedString
                
                self.infoView.addressLabel.text = Json["formatted_address"] as? String
                self.seletedMarkerAddress = self.infoView.addressLabel.text as NSString

                let contentSize = self.infoView.addressLabel.sizeThatFits(self.self.infoView.addressLabel.bounds.size)
                var frame = self.infoView.addressLabel.frame
                frame.size.height = contentSize.height
                self.infoView.addressLabel.frame = frame
                
                let aspectRatioTextViewConstraint = NSLayoutConstraint(item: self.infoView.addressLabel, attribute: .height, relatedBy: .equal, toItem: self.infoView.addressLabel, attribute: .width, multiplier: self.infoView.addressLabel.bounds.height/self.infoView.addressLabel.bounds.width, constant: 1)
                self.infoView.addressLabel.addConstraint(aspectRatioTextViewConstraint)
                
                
                if (Json["formatted_phone_number"] as? String) != nil
                {
                self.infoView.phoneLabel.text = Json["formatted_phone_number"] as? String
                self.phoneNumberInfoView = self.infoView.phoneLabel.text!
                }
                else
                {
                self.infoView.phoneLabel.text = ""
                self.phoneNumberInfoView  = ""
                }

                self.infoView.floatRatingView.emptyImage = UIImage(named: "ic_star_empty")
                self.infoView.floatRatingView.fullImage = UIImage(named: "ic_star_full")
                self.infoView.floatRatingView.contentMode = UIViewContentMode.scaleAspectFit
                self.infoView.floatRatingView.maxRating = 5
                self.infoView.floatRatingView.minRating = 0
                self.infoView.floatRatingView.rating = 5
                self.infoView.floatRatingView.editable = false
                self.infoView.floatRatingView.halfRatings = true
                self.infoView.floatRatingView.floatRatings = false
                self.seletedMarkerLogo = (Json["icon"] as? NSString)!
               
              //self.infoView.placePhoto.cacheEnabled = false
              self.infoView.placePhoto.progressColor = UIColor.clear
              self.infoView.placePhoto.backgroundProgressColor = UIColor.clear
              self.infoView.placePhoto.imageURL(URL: NSURL(string:self.seletedMarkerLogo as String)! as URL)
               
               
               self.infoView.center = CGPoint(x: self.view.bounds.midX, y: self.view.bounds.midY)
               var  frameInfo : CGRect = self.infoView.frame
               frameInfo.size = CGSize(width: 260 , height:self.infoView.addressLabel.frame.origin.y+self.infoView.addressLabel.frame.size.height+150)
                self.infoView.frame = frameInfo
               
                self.view?.addSubview(self.infoView)
                self.blackView?.isHidden = false
       
            }
        
        }
        else
        {
            
            userInfoView = Bundle.main.loadNibNamed("UserInfoView", owner: self, options: nil)?.first! as! UserInfoView
            userInfoView.layer.cornerRadius = 3
            userInfoView.clipsToBounds = true
            
            userInfoView.nameLabel.text = placeMarker.place.name
            seletedMarkerName =  placeMarker.place.name as NSString
            
            self.seletedMarkerType = placeMarker.place.selectedType!
            self.seletedMarkerImage = placeMarker.place.photoReference!

            self.seletedMarkerAddress = placeMarker.place.address as NSString
            self.phoneNumberInfoView = placeMarker.place.phone!
            
            //self.infoView.placePhoto.cacheEnabled = false
            userInfoView.placePhoto.progressColor = UIColor.clear
            userInfoView.placePhoto.backgroundProgressColor = UIColor.white
           

            userInfoView.placePhoto.imageURL(URL: NSURL(string:placeMarker.place.photoReference!) as! URL)
            userInfoView.placePhoto.layer.cornerRadius = userInfoView.placePhoto.frame.size.width/2
            userInfoView.placePhoto.clipsToBounds = true
            userInfoView.center = CGPoint(x: self.view.bounds.midX, y: self.view.bounds.midY)
            var  frameInfo : CGRect = userInfoView.frame
            frameInfo.size = CGSize(width: 260 , height:354)
            userInfoView.frame = frameInfo
            self.view?.addSubview(userInfoView)
            self.blackView?.isHidden = false
           
        }
        return nil
    }
  
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool
    {
              return false
    }
   
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool
    {
        
        self.mapView!.camera = GMSCameraPosition(target: self.marker.position, zoom: 17, bearing: 0, viewingAngle: 0)
        
         mapView.selectedMarker = nil
       
        var point = mapView.projection.point(for: self.marker.position)
        point.y = (point.y) - 150
        let camera = GMSCameraUpdate.setTarget((mapView.projection.coordinate(for: point)))
        mapView.animate(with: camera)
        return false
    }
    
    //TODO: Audio Player
    
    private func configureAudioPlayer() {
//        let mainBundlePath: String = Bundle.main.resourcePath! + ("/SKAdvisorResources.bundle")
//        let advisorResourcesBundle: Bundle =  Bundle(path:mainBundlePath)!
//        let soundFilesFolder: String = advisorResourcesBundle.path(forResource: "Languages", ofType: "")!
        let currentLanguage: String = "en_us"
//        let audioFilesFolderPath: String = soundFilesFolder + "/" +  currentLanguage + "/" + "sound_files"
        
        //AudioService.sharedInstance().audioFilesFolderPath = audioFilesFolderPath
        let settings: SKAdvisorSettings = SKAdvisorSettings()
        settings.advisorVoice = currentLanguage
        SKRoutingService.sharedInstance().advisorConfigurationSettings = settings
    }
    
    //TODO: SKRoutingService delegate - Routing
    
    func routingService(routingService: SKRoutingService!, didFinishRouteCalculationWithInfo routeInformation: SKRouteInformation!)
    {

        routingService.zoomToRoute(with: UIEdgeInsets.zero, duration:1)
        
        let advices: Array<SKRouteAdvice> =  SKRoutingService.sharedInstance().routeAdviceList(with: SKDistanceFormat.metric) as! Array<SKRouteAdvice>
        for advice: SKRouteAdvice in advices
        {
            print(advice.adviceInstruction)
            navigationTopInstructionLabel.text = advice.adviceInstruction

        }
        
        let routeID: Int32 = Int32(routeInformation.routeID)
        let coords: Array<CLLocation>  =  SKRoutingService.sharedInstance().routeCoordinatesForRoute(withId: routeID) as! Array<CLLocation>
        let countSentence: String = "Route contains " + String(coords.count) + " elements"
        print(countSentence)
    }
    
    func routingService(routingService: SKRoutingService!, didFailWithErrorCode errorCode: SKRoutingErrorCode) {
        print("Route calculation failed.")
    }
    
    
    //TODO: SKRoutingService delegate - Navigation
    
    func routingService(routingService: SKRoutingService!, didChangeDistanceToDestination distance: Int32, withFormattedDistance formattedDistance: String!) {
        let distance = "distanceToDestination " + formattedDistance
        print(distance)
        navigationBottomDistanceLabel.text = distance
    }
    
    func routingService(routingService: SKRoutingService!, didChangeEstimatedTimeToDestination time: Int32) {
        print("timeToDestination " + String(time))
        
        navigationBottomDurationLabel.text = String(time)

    }
    
    func routingService(routingService: SKRoutingService!, didChangeCurrentStreetName currentStreetName: String!, streetType: SKStreetType, countryCode: String!)
    {
        let streetTypeString: String = String(streetType.rawValue)
        let sentence: String = "Current street name changed to name=" + currentStreetName + " type=" + streetTypeString + " countryCode=" + countryCode
        print(sentence)
    }
    
    func routingService(routingService: SKRoutingService!, didChangeNextStreetName nextStreetName: String!, streetType: SKStreetType, countryCode: String!) {
        let streetTypeString: String = String(streetType.rawValue)
        let sentence: String = "Next street name changed to name=" + nextStreetName + " type=" + streetTypeString + " countryCode=" + countryCode
        print(sentence)
    }
    
    func routingService(routingService: SKRoutingService!, didChangeCurrentAdviceImage adviceImage: UIImage!, withLastAdvice isLastAdvice: Bool) {
        print("Current visual advice image changed.")
    }
    
    func routingService(routingService: SKRoutingService!, didChangeCurrentVisualAdviceDistance distance: Int32, withFormattedDistance formattedDistance: String!) {
        let sentence: String = "Current visual advice distance changed to distance=" + formattedDistance
        print(sentence)
    }
    
    func routingService(routingService: SKRoutingService!, didChangeSecondaryAdviceImage adviceImage: UIImage!, withLastAdvice isLastAdvice: Bool) {
        print("Secondary visual advice image changed.")
    }
    
    
    func routingService(routingService: SKRoutingService!, didChangeSecondaryVisualAdviceDistance distance: Int32, withFormattedDistance formattedDistance: String!) {
        let sentence: String = "Secondary visual advice distance changed to distance=" + formattedDistance
        print(sentence)
    }
    
    func routingService(routingService: SKRoutingService!, didUpdateFilteredAudioAdvices audioAdvices: [AnyObject]!) {
        //AudioService.sharedInstance().play(audioAdvices as! Array<String>)
    }
    
    func routingService(routingService: SKRoutingService!, didUpdateUnfilteredAudioAdvices audioAdvices: [AnyObject]!, withDistance distance: Int32) {
        print("Unfiltered audio advice updated.")
    }
    
    func routingService(routingService: SKRoutingService!, didChangeCurrentSpeed speed: Double) {
        let speedString: String = "Current speed: " + String(format:"%.2f", speed)
        print(speedString)
    }
    
    func routingService(routingService: SKRoutingService!, didChangeCurrentSpeedLimit speedLimit: Double) {
        let speedString: String = "Current speedlimit: " + String(format:"%.2f", speedLimit)
        print(speedString)
    }
    
    func routingServiceDidStartRerouting(routingService: SKRoutingService!) {
        print("Rerouting started.")
        navigationTopInstructionLabel.text = "Rerouting started."

    }
    
    func routingService(routingService: SKRoutingService!, didUpdateSpeedWarningToStatus speedWarningIsActive: Bool, withAudioWarnings audioWarnings: [AnyObject]!, insideCity isInsideCity: Bool) {
        print("Speed warning status updated.")
    }
    
    func routingServiceDidReachDestination(routingService: SKRoutingService!) {
        let message: String? = NSLocalizedString("navigation_screen_destination_reached_alert_message", comment: "")
        let cancelButtonTitle: String? = NSLocalizedString("navigation_screen_destination_reached_alert_ok_button_title", comment: "")
        
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: cancelButtonTitle, style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}
extension ViewController: PXGoogleDirectionsDelegate
{
    func googleDirectionsWillSendRequestToAPI(_ googleDirections: PXGoogleDirections, withURL requestURL: URL) -> Bool {
        NSLog("googleDirectionsWillSendRequestToAPI:withURL:")
        return true
    }
    
    func googleDirectionsDidSendRequestToAPI(_ googleDirections: PXGoogleDirections, withURL requestURL: URL) {
        NSLog("googleDirectionsDidSendRequestToAPI:withURL:")
        ///NSLog("\(requestURL.absoluteString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!)")
    }
    
    func googleDirections(_ googleDirections: PXGoogleDirections, didReceiveRawDataFromAPI data: Data) {
        NSLog("googleDirections:didReceiveRawDataFromAPI:")
        ///NSLog(NSString(data: data, encoding: NSUTF8StringEncoding) as! String)
    }
    
    func googleDirectionsRequestDidFail(_ googleDirections: PXGoogleDirections, withError error: NSError) {
        NSLog("googleDirectionsRequestDidFail:withError:")
        ///NSLog("\(error)")
    }
    
    func googleDirections(_ googleDirections: PXGoogleDirections, didReceiveResponseFromAPI apiResponse: [PXGoogleDirectionsRoute]) {
        NSLog("googleDirections:didReceiveResponseFromAPI:")
        NSLog("Got \(apiResponse.count) routes")
        for i in 0 ..< apiResponse.count {
            NSLog("Route \(i) has \(apiResponse[i].legs.count) legs")
        }
    }
}
