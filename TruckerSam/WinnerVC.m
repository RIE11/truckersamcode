//
//  WinnerVC.m
//  DemoMap
//
//  Created by Monica on 2/13/15.
//  Copyright (c) 2015 Monica. All rights reserved.
//

#import "WinnerVC.h"
#import "TruckerSam_Connection.h"

#define baseUrl                         @"https://www.truckersam.com/webservice/"
#define GetUserImage                    @"get_user_uploaded_images"
#define SaveImageLike                   @"save_image_like"
#define GetWinnerImage                  @"get_all_winner_images"

@interface WinnerVC ()
@property(nonatomic,weak)IBOutlet UILabel *titleLbl;
@property(nonatomic,retain)IBOutlet UITableView *tblImagelist;
@property(nonatomic)NSIndexPath *indexPathSel;
@property(nonatomic)int tag;

@property(nonatomic,retain)NSMutableArray *allImages;
@property(nonatomic,retain)IBOutlet UIImageView *headerImg;
@property(nonatomic,retain)IBOutlet UIButton *menuBtn;
@property(nonatomic,retain)IBOutlet UIButton *backBtn;
@property(nonatomic,retain)IBOutlet UIImageView *menuImg;
@property(nonatomic,retain)IBOutlet UIImageView *backImg;

@end

@implementation WinnerVC

-(NSMutableArray *)allImages
{
    if (!_allImages) {
        _allImages = [NSMutableArray new];
    }
    return _allImages;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    _titleLbl.font=[UIFont fontWithName:@"Oswald-Regular" size:26];

    
    [self.tblImagelist registerNib:[UINib nibWithNibName:@"TruckerPicsCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"filterCell"];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    //set garbage value for tag
    _tag=27345435;
    [self.allImages removeAllObjects];
    
    HUD=[MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
    [self GetWinnerUploadedImage];
}


#pragma mark-Back Action
- (IBAction)BackAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)GetWinnerUploadedImage
{
    int userID = [[NSUserDefaults standardUserDefaults] stringForKey:@"UserID"].intValue;
    
    NSDictionary *parameters = @{@"user_id":(userID)?[NSString stringWithFormat:@"%i",userID]:@"",
                                 };
    
    [[TruckerSam_Connection sharedManager]getDataForUrl:[baseUrl stringByAppendingString:GetWinnerImage] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"%@",responseObject);
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"failure"])
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"No image found" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
             [alert show];
         }
         else
         {
             NSArray* allImgesData = [responseObject objectForKey:@"images"];
             
             for (int i=0; i<[allImgesData count];i++)
             {
                 ImageWinnerData * imagedata = [ImageWinnerData new];
                 [imagedata updateValuesWithDictionary:allImgesData[i]];
                 imagedata.imageDownloaded = [[AsyncImageLoader sharedLoader].cache objectForKey:imagedata.imageURL];
                 [self.allImages addObject:imagedata];
             }
         }
         
         [_tblImagelist reloadData];
         
         [HUD setHidden:YES];
     }
      failure:^(AFHTTPRequestOperation *operation, NSError *error){
          [HUD setHidden:YES]	;
          NSLog(@"Enter into failure is %@",error.localizedDescription);
          UIAlertView  *alert=nil;
          alert=[[UIAlertView alloc]initWithTitle:@"Error!" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
          [alert show];
      }];
}

#pragma mark-tableview delegate and datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_tblImagelist)
    {
        ImageWinnerData * imagedata = self.allImages[indexPath.row];
        if (imagedata) {
            UIImage * image = imagedata.imageDownloaded;
            
            if (image) {
                float scale = (image.size.width/self.tblImagelist.frame.size.width);
                
                float height = image.size.height/scale;
                
                float defaultHeight = 184;
                float heightDiff = height - defaultHeight;
                
                height = 360 + heightDiff;
                
                return MAX(height, 360);
            }
        }
        return 360;
    }
    else
    {
        return 50;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _allImages.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        static NSString *CellIdentifier = @"filterCell";
        
        //====================CUSTOM CELL WITH VIDEO=============================
        TruckerPicsCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        [cell.btnLike addTarget:self action:@selector(LikeAction:) forControlEvents:UIControlEventTouchUpInside];
        
        ImageWinnerData *curentdata=_allImages[indexPath.row];
    
        //Hide winner image and label
        [cell.imgWinner setHidden:NO];
        [cell.lblWinner setHidden:NO];
        
        cell.lblWinner.text=[NSString stringWithFormat:@"%@ Winner",curentdata.month];
    
        cell.lblUserName.text   =   curentdata.username;
        cell.lblTime.text=[self HourCalculation:curentdata.uploadTime];
        cell.lblCaption.text=curentdata.caption;;
        cell.lbllikeUserCount.text=[NSString stringWithFormat:@"%@ Trukers like this",curentdata.allLikeCount];
        cell.imgUploaded.imageURL=[NSURL URLWithString:curentdata.image];
        
        if ([curentdata.userImage isEqualToString:@""] || [curentdata.image length]==0) {
            cell.imgUser.image=[UIImage imageNamed:@"picDefault.png"];
        }else{
            cell.imgUser.imageURL=[NSURL URLWithString:curentdata.image];
        }
        
        if ([curentdata.like isEqualToString:@"yes"])
        {
            cell.btnLike.userInteractionEnabled=NO;
            cell.btnLike.imageView.image=[UIImage imageNamed:@"like-active"];
        }
        else
        {
            if (indexPath.row==_tag)
            {
                cell.btnLike.userInteractionEnabled=NO;
                cell.btnLike.imageView.image=[UIImage imageNamed:@"like-active"];
            }
            else{
                cell.btnLike.userInteractionEnabled=YES;
                cell.btnLike.imageView.image=[UIImage imageNamed:@"like-inactive"];
            }
        }
        cell.btnLike.tag=indexPath.row;
        
        if ([curentdata.userId integerValue]==[[NSUserDefaults standardUserDefaults] stringForKey:@"UserID"].intValue)
        {
            cell.imgShare.hidden=NO;
        }else{
            cell.imgShare.hidden=YES;
        }
        
        return cell;
}


#pragma mark-Like Action
-(void)LikeAction:(UIButton *)sender
{
    _tag=[sender tag];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_tag inSection:0];
    _indexPathSel=indexPath;
    
    HUD=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    int userID = [[NSUserDefaults standardUserDefaults] stringForKey:@"UserID"].intValue;
    
    ImageWinnerData *curentdata=_allImages[indexPath.row];
    
    NSDictionary *parameters = @{@"user_id":(userID)?[NSString stringWithFormat:@"%i",userID]:@"",
                                 @"image_id":curentdata.imageId,
                                 @"like":@"yes",};
    
    [[TruckerSam_Connection sharedManager]getDataForUrl:[baseUrl stringByAppendingString:SaveImageLike] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"%@",responseObject);
         [sender setUserInteractionEnabled:NO];
         [sender setImage:[UIImage imageNamed:@"like-active"] forState:UIControlStateNormal];
         
         curentdata.allLikeCount = [responseObject objectForKey:@"total_like_count"];
         
         NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
         [_tblImagelist reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
         
         [HUD setHidden:YES];
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error){
         [HUD setHidden:YES];
         NSLog(@"Enter into failure is %@",error.localizedDescription);
         UIAlertView  *alert=nil;
         alert=[[UIAlertView alloc]initWithTitle:@"Error!" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
         [alert show];
     }];
}

-(NSString*)HourCalculation:(NSString*)PostDate
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormat setLocale:[NSLocale systemLocale]];
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [dateFormat setTimeZone:gmt];
    NSDate *ExpDate = [dateFormat dateFromString:PostDate];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSDayCalendarUnit|NSWeekCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit|NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit) fromDate:ExpDate toDate:[NSDate date] options:0];
    NSString *time;
    if(components.year!=0)
    {
        if(components.year==1)
        {
            time=[NSString stringWithFormat:@"%ld year",(long)components.year];
        }
        else
        {
            time=[NSString stringWithFormat:@"%ld years",(long)components.year];
        }
    }
    else if(components.month!=0)
    {
        if(components.month==1)
        {
            time=[NSString stringWithFormat:@"%ld month",(long)components.month];
        }
        else
        {
            time=[NSString stringWithFormat:@"%ld months",(long)components.month];
        }
    }
    else if(components.week!=0)
    {
        if(components.week==1)
        {
            time=[NSString stringWithFormat:@"%ld week",(long)components.week];
        }
        else
        {
            time=[NSString stringWithFormat:@"%ld weeks",(long)components.week];
        }
    }
    else if(components.day!=0)
    {
        if(components.day==1)
        {
            time=[NSString stringWithFormat:@"%ld day",(long)components.day];
        }
        else
        {
            time=[NSString stringWithFormat:@"%ld days",(long)components.day];
        }
    }
    else if(components.hour!=0)
    {
        if(components.hour==1)
        {
            time=[NSString stringWithFormat:@"%ld hour",(long)components.hour];
        }
        else
        {
            time=[NSString stringWithFormat:@"%ld hours",(long)components.hour];
        }
    }
    else if(components.minute!=0)
    {
        if(components.minute==1)
        {
            time=[NSString stringWithFormat:@"%ld min",(long)components.minute];
        }
        else
        {
            time=[NSString stringWithFormat:@"%ld mins",(long)components.minute];
        }
    }
    else if(components.second>=0)
    {
        if(components.second==0)
        {
            time=[NSString stringWithFormat:@"1 sec"];
        }
        else
        {
            time=[NSString stringWithFormat:@"%ld secs",(long)components.second];
        }
    }
    return [NSString stringWithFormat:@"%@ ago",time];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

@implementation ImageWinnerData

-(void)updateValuesWithDictionary:(NSDictionary*)values
{
    self.userId     = values[@"user_id"];
    self.username   = values[@"username"];
    self.userImage  = values[@"user_image"];
    self.caption    = values[@"caption"];
    self.image      = values[@"image"];
    self.allLikeCount = values[@"all_like_count"];
    self.like       = values[@"like"];
    self.month       = values[@"winner_month"];
    self.imageId    = values[@"image_id"];
    self.uploadTime = values[@"upload_time"];
    self.imageURL   = [NSURL URLWithString:self.image];
}

@end
