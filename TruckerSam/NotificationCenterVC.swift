//
//  NotificationCenterVC.swift
//  TruckerSam
//
//  Created by Monika on 12/29/16.
//  Copyright © 2016 Monica. All rights reserved.
//

//gurie start
import Foundation
import UIKit
import PASImageView


class NotificationCenterVC: BaseVC,UITableViewDelegate,UITableViewDataSource,DLHamburguerViewControllerDelegate
{
    @IBOutlet weak var headerBackgroundView:UIView!
    @IBOutlet weak var headerLogoView:UIImageView!
    var SideMenuView = SideMenuViewController()

    var arrayNotifications = NSMutableArray()
    @IBOutlet weak var tableView: UITableView!


    override func viewDidLoad()
    {
       
        
        super.viewDidLoad()
        ///set background color
        headerBackgroundView.backgroundColor = Constant.GlobalConstants.kColor_ScreenBackground
        self.view.backgroundColor = Constant.GlobalConstants.kColor_ScreenBackground
        
        self.findHamburguerViewController()?.menuDirection = .right
        self.findHamburguerViewController()?.delegate = self
        getNotificationsAPI()
        
    }
    
    @IBAction func actionLayersButton(_ sender: AnyObject)
    {
        self.findHamburguerViewController()?.showMenuViewController()
        
    }
    
    func hamburguerViewController(_ hamburguerViewController: DLHamburguerViewController, didHideMenuViewController menuViewController: UIViewController)
    {
        DispatchQueue.main.async {
            _ = self.navigationController?.popViewController(animated: true)
            
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: -
    
    func getNotificationsAPI()
    {
        self.arrayNotifications.removeAllObjects()
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        //https://www.truckersam.com/webservice/select_buddies?userid=1245
        WebServicesVC.sharedInstance.CallGetAPIFunction(urlString: Constant.GlobalConstants.k_AllNotifications, parameter:"userid=" + "\(self.GetIntegerUserDefault(ForKey: "UserID"))", success: { (responseObject) in
            print(responseObject!)
            
            for detail in responseObject as! NSArray
            {
                let notifications = Notifications()
                notifications.getAllNotifications(dictionary: detail as! NSDictionary)
                self.arrayNotifications.add(notifications)
            }
           
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
            self.tableView.reloadData()
            
        }) { (NSError) in
            
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
        }
    }
    
    @IBAction func actionContactsButton(_ sender: AnyObject)
    {
        let viewController = self.storyboard!.instantiateViewController(withIdentifier: "EPContactsPicker") as! EPContactsPicker
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func actionComposeButton(_ sender: AnyObject)
    {
        let composeMsgObj = self.storyboard?.instantiateViewController(withIdentifier: "ComposeMessageVC") as? ComposeMessageVC
        self.navigationController?.pushViewController(composeMsgObj!, animated: true)
    }
    
    @IBAction func actionCrossButton(_ sender: AnyObject)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    private func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrayNotifications.count
    }
    
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell: NotificationViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! NotificationViewCell
        
        //to set corner radius
        let notifications : Notifications
        notifications = self.arrayNotifications[indexPath.row] as! Notifications
        cell.labelTitle?.text = "\(notifications.Name!)"
        
        cell.imageViewNotification.backgroundColor = UIColor.clear
        cell.imageViewNotification.layer.cornerRadius = cell.imageViewNotification.frame.size.width/2
        cell.imageViewNotification.clipsToBounds = true
        cell.imageViewNotification.imageURL(URL: NSURL(string: notifications.image) as! URL)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        //Saving data locally in user default
        let notifications : Notifications
        notifications = self.arrayNotifications[indexPath.row] as! Notifications
        
        for i in 0..<self.arrayNotifications.count
        {
            let anotherNotifications : Notifications
            anotherNotifications = self.arrayNotifications[i] as! Notifications
            
            if anotherNotifications.id == notifications.id
            {
                print(anotherNotifications.ownerID)
                print(notifications.ownerID)
                
                let key = "\(anotherNotifications.ownerID!)" + "\(self.GetIntegerUserDefault(ForKey: "UserID"))"
                
                print(key)
                
                var namesDictionary: Dictionary<String, String> = [:]
                namesDictionary["from"] = "\(anotherNotifications.message!)"
                
                
                if UserDefaults.standard.object(forKey: key) != nil
                {
                    let placesData = UserDefaults.standard.object(forKey: key) as? NSData
                    let placesArray = NSKeyedUnarchiver.unarchiveObject(with: placesData as! Data) as? NSMutableArray
                    placesArray?.add(namesDictionary)
                    let placesDatanew = NSKeyedArchiver.archivedData(withRootObject: placesArray!)
                    UserDefaults.standard.set(placesDatanew, forKey: key)
                    
                }
                else
                {
                    let placesArray = NSMutableArray()
                    placesArray.add(namesDictionary)
                    let placesDatanew = NSKeyedArchiver.archivedData(withRootObject: placesArray)
                    UserDefaults.standard.set(placesDatanew, forKey:key)
                }
                self.toDeleteNotificationsAPI(stringNotificationId:anotherNotifications.id,stringOwnerId:notifications.ownerID,stringUserName:notifications.Name,stringUserImage:notifications.image)
            }
        }
        
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
    }
    
       
}
class NotificationViewCell: UITableViewCell
{
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var imageViewNotification: PASImageView!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
}
//end
