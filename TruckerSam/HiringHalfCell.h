//
//  HiringHalfCell.h
//  DemoMap
//
//  Created by Monica on 2/2/15.
//  Copyright (c) 2015 Monica. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HiringHalfCell : UITableViewCell
@property(nonatomic,retain)IBOutlet UIButton *readMoreBtn;
@property(nonatomic,retain)IBOutlet UILabel *lblJobTitle;
@property(nonatomic,retain)IBOutlet UILabel *lblCompanyName;
@property(nonatomic,retain)IBOutlet UILabel *lblJobType;
@property(nonatomic,retain)IBOutlet UILabel *lblJobLocation;
@property(nonatomic,retain)IBOutlet UILabel *lblCompanyName1;
@property(nonatomic,retain)IBOutlet UILabel *lblJobType1;
@property(nonatomic,retain)IBOutlet UILabel *lblJobLocation1;
@property(nonatomic,retain)IBOutlet UILabel *lblJobDescp;
@property(nonatomic,retain)IBOutlet UIWebView *webJobDescp;
@end
