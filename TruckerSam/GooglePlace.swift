//
//  GooglePlace.swift
//  Feed Me
//
/*
* Copyright (c) 2015 Razeware LLC
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

import UIKit
import Foundation
import CoreLocation

class GooglePlace
{
  let name: String
  let address: String
  let coordinate: CLLocationCoordinate2D
  let placeType: String
  var photoReference: String?
  var photo: UIImage?
  var distance: String?
  var phone: String?
  var place_id: String?

  var selectedType: String?

  
  init(dictionary:[String : AnyObject], acceptedTypes: [String])
  {
    
    if acceptedTypes.contains("NearByBuddies")
    {
        let json = JSON(dictionary)
        print(json)
        
        //{"id":"906","full_name":"David","image":"https:\/\/www.truckersam.com\/images\/noimage.png","phone":"9720548183520","latitude":"31.9962343","longitude":"34.9418334","distance":"0.58"}
        
        name      = json["full_name"].stringValue
        address   = json["phone"].stringValue
        phone     = json["phone"].stringValue
        distance  = json["distance"].stringValue
        place_id  = json["id"].stringValue
       
        let lat  = json["latitude"].stringValue
        let lng  = json["longitude"].stringValue
        selectedType = "NearByBuddies"
        coordinate = CLLocationCoordinate2DMake(Double(lat)! as CLLocationDegrees, Double(lng)! as CLLocationDegrees)
        
        photoReference = json["image"].string?.replacingOccurrences(of: "//", with: "/")
        
        let foundType = SelectedName.lowercased().replacingOccurrences(of: " ", with: "_")
        
        placeType = foundType
    }
    else
    {
    if SelectedParentId == "86"
    {
    
    let json = JSON(dictionary)
    print(json)
    name = json["name"].stringValue
    address = json["formatted_address"].stringValue
    
    let lat = json["geometry"]["location"]["lat"].doubleValue as CLLocationDegrees
    let lng = json["geometry"]["location"]["lng"].doubleValue as CLLocationDegrees
    coordinate = CLLocationCoordinate2DMake(lat, lng)
    
        photoReference = json["photos"][0]["photo_reference"].string
        phone   = ""
        distance   = ""
        place_id = json["place_id"].stringValue
        selectedType = "FromGoogle"

   var foundType = SelectedName.lowercased().replacingOccurrences(of: " ", with: "_")
    
    let possibleTypes =
        ["accounting","airport","amusement_park","aquarium","art_gallery","atm","bakery","bank","bar","beauty_salon","bicycle_store","book_store","bowling_alley","bus_station","cafe","campground","car_dealer","car_rental","car_repair","car_wash","casino","cemetery","church","city_hall","clothing_store","convenience_store","courthouse","dentist","department_store","doctor","electrician","electronics_store","embassy","fire_station","florist","funeral_home","furniture_store","gas_station","gym","hair_care","hardware_store","hindu_temple","home_goods_store","hospital","insurance_agency","jewelry_store","laundry","lawyer","library","liquor_store","local_government_office","locksmith","lodging","meal_delivery","meal_takeaway","mosque","movie_rental","movie_theater","moving_company","museum","night_club","painter","park","parking","pet_store","pharmacy","physiotherapist","plumber","police","post_office","real_estate_agency","restaurant","roofing_contractor","rv_park","school","shoe_store","shopping_mall","spa","stadium","storage","store","subway_station","synagogue","taxi_stand","train_station","transit_station","travel_agency","university","veterinary_care","zoo"]
    
    
    for type in json["types"].arrayObject as! [String]
    {
      if possibleTypes.contains(type)
      {
        foundType = type
        break
      }
     }
    
        placeType = foundType
     
    
    }
    else
    {
        
        let json = JSON(dictionary)
        print(json)
      
        /*  {
            "around_me_logo" : "1483879809680151.jpg",
            "longitude" : "-147.688948197",
            "latitude" : "64.8578780942",
            "address" : "",
            "company_name" : "Wal-Mart Supercenter",
            "movingsos_logo" : "1426715410997944.png",
            "distance" : "5412.69",
            "branch_name" : "Wal-Mart Supercenter",
            "id" : "5169",
            "category_name" : "Wal-Mart Supercenter",
            "logo" : "https:\/\/www.truckersam.com\/uploaded_images\/logos\/1426715410997944.png"
        }*/

        name    = json["branch_name"].stringValue
        address = json["address"].stringValue
        
        let lat = json["latitude"].stringValue
        let lng = json["longitude"].stringValue
        
        coordinate = CLLocationCoordinate2DMake(Double(lat)! as CLLocationDegrees, Double(lng)! as CLLocationDegrees)
        
        photoReference = json["logo"].string
       
        phone      = ""
        distance   = json["distance"].string
        place_id   = json["id"].string
        selectedType = "FromServer"

        
        let foundType = SelectedName.lowercased().replacingOccurrences(of: " ", with: "_")
       
        placeType = foundType
    }
    }
}
}
