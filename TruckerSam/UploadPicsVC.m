//
//  UploadPicsVC.m
//  DemoMap
//
//  Created by Monica on 2/10/15.
//  Copyright (c) 2015 Monica. All rights reserved.
//

#import "UploadPicsVC.h"
#import "TruckerSam_Connection.h"
#import "AFHTTPRequestOperationManager.h"


#define baseUrl                         @"https://www.truckersam.com/webservice/"
#define SaveImageLike                   @"save_image_like"
#define UploadUserImage                 @"upload_user_image"

@interface UploadPicsVC ()
@property(nonatomic,retain)UIImageView *imgUpload;
@property(nonatomic,retain)UITextView *txtCaption;
@property(nonatomic,retain)UIButton *btnImg;
@property(nonatomic,retain)UIButton *btnUpload;
@property(nonatomic,retain) UILabel *lblText;
@property(nonatomic,retain) UILabel *lblLine;
@property(nonatomic,retain) UIButton *btnTOS;

@property(nonatomic,retain)UIImageView *imgCancel;
@property(nonatomic,retain)UIButton *btnCancel;

@property(nonatomic,weak)IBOutlet UILabel *titleLbl;

@property(nonatomic) BOOL isFromGallery;
@property(nonatomic) int yOffset;

@property(nonatomic,retain)IBOutlet UIImageView *headerImg;
@property(nonatomic,retain)IBOutlet UIButton *menuBtn;
@property(nonatomic,retain)IBOutlet UIButton *backBtn;
@property(nonatomic,retain)IBOutlet UIImageView *menuImg;
@property(nonatomic,retain)IBOutlet UIImageView *backImg;
@property(nonatomic,retain)IBOutlet UIScrollView *scroll;

@end

@implementation UploadPicsVC

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    
    [self DesignOfView];
   

    
    _titleLbl.font=[UIFont fontWithName:@"Oswald-Regular" size:26];
    
    _isFromGallery=NO;
    
    
   // UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] //initWithTarget:self action:@selector(hideKeyboard)];
   // tapGesture.cancelsTouchesInView = NO;
   // [self.view addGestureRecognizer:tapGesture];
    
    
    [_txtCaption setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"text-view_UPLOAD"]]];
    
    _yOffset=190;

    // Do any additional setup after loading the view.
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.scroll endEditing:YES];
}
-(void)DesignOfView
{
    _imgUpload=[[UIImageView alloc]initWithFrame:CGRectMake(20,28,280,170)];
    [_scroll addSubview:_imgUpload];
    
    _btnImg=[UIButton buttonWithType:UIButtonTypeCustom];
    _btnImg.frame=CGRectMake(0,28,320,170);
    [_btnImg addTarget:self action:@selector(ImageUploadAction:) forControlEvents:UIControlEventTouchUpInside];
    [_scroll addSubview:_btnImg];
    
    _lblText=[[UILabel alloc]initWithFrame:CGRectMake(0,257,320,21)];
    _lblText.font=[UIFont systemFontOfSize:12];
    _lblText.textColor=[UIColor whiteColor];
    _lblText.backgroundColor=[UIColor clearColor];
    _lblText.textAlignment=NSTextAlignmentCenter;
    _lblText.text=@"by pressing 'upload' I agree to the terms of service";
    [_scroll addSubview:_lblText];
    
    _lblLine=[[UILabel alloc]initWithFrame:CGRectMake(209,274,85,1)];
    _lblLine.backgroundColor=[UIColor whiteColor];
    [_scroll addSubview:_lblLine];
    
    _btnTOS=[UIButton buttonWithType:UIButtonTypeCustom];
    _btnTOS.frame=CGRectMake(183,250,127,26);
    [_btnTOS addTarget:self action:@selector(TermOfServiceAction:) forControlEvents:UIControlEventTouchUpInside];
    [_scroll addSubview:_btnTOS];
    
    _txtCaption=[[UITextView alloc]initWithFrame:CGRectMake(25,214,270,42)];
    _txtCaption.delegate=self;
    _txtCaption.text=@"Add a Caption";
    _txtCaption.textColor=[UIColor whiteColor];
    _txtCaption.backgroundColor=[UIColor clearColor];
    [_scroll addSubview:_txtCaption];
    
    _btnUpload=[UIButton buttonWithType:UIButtonTypeCustom];
    _btnUpload.frame=CGRectMake(25,286,270,36);
    [_btnUpload setImage:[UIImage imageNamed:@"uploadBtn"] forState:UIControlStateNormal];
    [_btnUpload addTarget:self action:@selector(UploadAction:) forControlEvents:UIControlEventTouchUpInside];
    [_scroll addSubview:_btnUpload];
    
    _imgCancel=[[UIImageView alloc]initWithFrame:CGRectMake(142,335,36,13)];
    [_imgCancel setImage:[UIImage imageNamed:@"cancel"]];
    [_scroll addSubview:_imgCancel];
    
    _btnCancel=[UIButton buttonWithType:UIButtonTypeCustom];
    _btnCancel.frame=CGRectMake(107,330,106,29);
    [_btnCancel addTarget:self action:@selector(CancelAction:) forControlEvents:UIControlEventTouchUpInside];
    [_scroll addSubview:_btnCancel];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    if (!_isFromGallery) {
        _scroll.contentSize=CGSizeMake(320,402);
        _imgUpload.frame=CGRectMake(20,28,280,170);
        _btnImg.frame=CGRectMake(0,28,320,170);
        _lblText.frame=CGRectMake(0,257,320,21);
        _lblLine.frame=CGRectMake(209,274,85,1);
        _btnTOS.frame=CGRectMake(183,250,127,26);
        _txtCaption.frame=CGRectMake(25,214,270,42);
        _btnUpload.frame=CGRectMake(25,286,270,36);
        _imgCancel.frame=CGRectMake(142,335,36,13);
        _btnCancel.frame=CGRectMake(107,330,106,29);
        
        _imgUpload.image=[UIImage imageNamed:@"click-picture"];
        _txtCaption.text=@"Add a Caption";
        _txtCaption.textAlignment=NSTextAlignmentCenter;
    }
}

#pragma mark-Back Action
- (IBAction)BackAction:(id)sender
{
  [self.navigationController popViewControllerAnimated:YES];
}


-(void)ImageUploadAction:(id)sender
{
    UIActionSheet *picSheet= [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Use camera" otherButtonTitles:@"Gallery", nil];
    [picSheet showInView:_scroll];
}

#pragma mark-Action sheet delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    imagePicker.delegate = self;
    
    if (buttonIndex==0)
    {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }else if (buttonIndex==1)
    {
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)actionSheetCancel:(UIActionSheet *)actionSheet
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIImagePickerController Delegate
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if (info != nil)
    {
        NSString* strMediaType = (NSString*)[info objectForKey:UIImagePickerControllerMediaType];
        if (strMediaType != nil)
        {
            _isFromGallery=YES;
            
            UIImage *image=info[UIImagePickerControllerOriginalImage];
            
            float scale = (image.size.width/320);
            
            float height = image.size.height/scale;
            
            float defaultHeight = 168;
            float heightDiff = height - defaultHeight;
       
            _imgUpload.frame=CGRectMake(5,28,310,height);
            _btnImg.frame=CGRectMake(5,28,310,height);
            _txtCaption.frame=CGRectMake(25,height+45,270,42);
            _lblText.frame=CGRectMake(0,height+100,320,21);
            _lblLine.frame=CGRectMake(205,height+120,85,1);
            _btnTOS.frame=CGRectMake(183,height+100,127,26);
            _btnUpload.frame=CGRectMake(25,height+130,270,36);
            _imgCancel.frame=CGRectMake(142,height+180,36,13);
            _btnCancel.frame=CGRectMake(107,height+170,106,29);
            
            _scroll.contentSize=CGSizeMake(320,402+heightDiff);
            
            _imgUpload.image=info[UIImagePickerControllerOriginalImage];
        }
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    _txtCaption.text=@"";
    _txtCaption.textAlignment=NSTextAlignmentLeft;
    
   // if ([_txtCaption resignFirstResponder]) {
   //     [_txtCaption becomeFirstResponder];
   // }
//    
//    [UIView beginAnimations:nil context:NULL];
//    [UIView setAnimationDuration:0.30f];
//    CGRect frame = self.view.frame;
//    frame.origin.y = -85;
//    [self.view setFrame:frame];
//    [UIView commitAnimations];
    
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSString *text_field = [textView.text stringByReplacingCharactersInRange:range withString:text];
    
    if (text_field.length > 60)
    {
        return NO;
    }
    return YES;
}

-(void)hideKeyboard
{
    [_txtCaption resignFirstResponder];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.30];
    CGRect frame = self.view.frame;
    frame.origin.y = 0;
    [self.view setFrame:frame];
    [UIView commitAnimations];
}

#pragma mark-Upload action
-(void)UploadAction:(id)sender
{
  //  UIImage *imgScaled=[[AppManager sharedManager]scaleImage:_imgUpload.image];
    
    NSData *imageData = UIImageJPEGRepresentation(_imgUpload.image, 0.5);
    
    if ([_imgUpload.image isEqual:[UIImage imageNamed:@"click-picture"]])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Please choose the image" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
    }
    else{
        
        if ([_txtCaption.text isEqualToString:@"Add a Caption"])
        {
            _txtCaption.text=@"";
        }
        
        HUD=[MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
        
        //get current gmt time
        NSDate *date = [NSDate date];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
        [dateFormat setTimeZone:gmt];
        NSString *timeCurrentStr=[dateFormat stringFromDate:date];
        
        
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSDictionary *parameters = @{@"user_id":[NSString stringWithFormat:@"%i",[[NSUserDefaults standardUserDefaults] stringForKey:@"UserID"].intValue],
                                 @"caption":(_txtCaption.text)?_txtCaption.text:@"",
                                 @"upload_time":timeCurrentStr
                                 };
    
    [manager POST:[baseUrl stringByAppendingString:UploadUserImage] parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData){
        if (imageData!=nil)
        {
            [formData appendPartWithFileData:imageData
                                        name:@"image"
                                    fileName:@"picture.jpg"
                                    mimeType:@"image/jpg"];
        }
    }
    success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [HUD setHidden:YES];
         
         NSLog(@"response object is %@",responseObject);
         
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"success"])
         {
             [self.tabBarController setSelectedIndex:2];
         }
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
          [HUD setHidden:YES];
           NSLog(@"Enter into failure is %@",error.localizedDescription);
           UIAlertView  *alert=nil;
           alert=[[UIAlertView alloc]initWithTitle:@"Error!" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
           [alert show];
     }];
    }
}

-(IBAction)CancelAction:(id)sender
{
    [self.tabBarController setSelectedIndex:2];
}

-(IBAction)TermOfServiceAction:(id)sender
{
  //  PageVC *pageObj=[self.storyboard instantiateViewControllerWithIdentifier:@"PageVC"];
  //  pageObj.pageNameStr=@"Terms of Services";
  //  [self.navigationController pushViewController:pageObj animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    _isFromGallery=NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
