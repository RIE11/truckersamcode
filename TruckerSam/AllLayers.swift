//
//  AllLayers.swift
//  TruckerSam
//
//  Created by Gurleen Singh on 1/13/17.
//  Copyright © 2017 Monica. All rights reserved.
//

import UIKit

class AllLayers: NSObject
{
    var idString            :String!
    var name                :String!
    var parent_id           :String!
    var around_me_logo      :String!
    var keyword             :String!
    var masterOfWL          :String!
    var status              :String!
    
    func getAllLayers(dictionary:NSDictionary)
    {
        print(dictionary)
        
        
        idString          = ((dictionary["id"] == nil) ? "" : dictionary["id"]) as? String
        
        name       = ((dictionary["name"]  == nil) ? "" : dictionary["name"]) as? String
        
        parent_id         = ((dictionary["parent_id"]  == nil) ? "" : dictionary["parent_id"]) as? String
        
        around_me_logo     = ((dictionary["around_me_logo"]  == nil) ? "" : dictionary["around_me_logo"]) as? String
        
        keyword           = ((dictionary["keyword"]  == nil) ? "" : dictionary["keyword"]) as? String
        
        masterOfWL     = ((dictionary["masterOfWL"]  == nil) ? "" : dictionary["masterOfWL"]) as? String
        
        status = ((dictionary["status"]  == nil) ? "" : dictionary["status"]) as? String
        
    }
}
