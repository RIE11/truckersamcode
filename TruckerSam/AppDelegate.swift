//
//  AppDelegate.swift
//  TruckerSam
//
//  Created by Monika on 12/23/16.
//  Copyright © 2016 Monica. All rights reserved.
//

import UIKit
import UserNotifications
import IQKeyboardManagerSwift
import GoogleMaps
import CoreData
import Firebase
import SKMaps

let API_KEY: String = "a2614fa34c08395247771a05f17bd02b81317ab1dc0c21e289b326a0f0d13421"


var SelectedParentId = String()
var SelectedId = String()
var SelectedIndex = Int()

var SelectedName = String()
var SelectedURL = String()
var directionEnabled = Bool()
var arrayLayers : NSMutableArray!

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate ,UNUserNotificationCenterDelegate,SKMapVersioningDelegate
{
    
    var window: UIWindow?
    var deviceTokenToPass: String! = nil
    var contentDic:            NSDictionary!
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        // Override point for customization after application launch.
        //FIRApp.configure()
        
        
        SelectedParentId = ""
        SelectedId = ""
        SelectedName = ""
        SelectedURL = ""
        SelectedIndex = -1
        arrayLayers = NSMutableArray()
        getMenuControlsAPI()

        
        let initSettings: SKMapsInitSettings = SKMapsInitSettings()
        initSettings.mapDetailLevel = SKMapDetailLevel.full //Use Full version of maps.
        //Can be set to a light version.
        
        SKMapsService.sharedInstance().initializeSKMaps(withAPIKey: API_KEY, settings: initSettings)
        SKPositionerService.sharedInstance().startLocationUpdate()
        SKMapsService.sharedInstance().mapsVersioningManager.delegate = self

        
        GMSServices.provideAPIKey(Constant.GlobalConstants.kGoogleApiKey)
        IQKeyboardManager.sharedManager().enable = true
        
        //call push notification
        registerForPushNotifications(application: application)
        
        application.registerForRemoteNotifications()
        
        // UIApplication.sharedApplication.statusBarHidden = false
        
        return true
    }
    func getMenuControlsAPI()
    {
        
        WebServicesVC.sharedInstance.CallGetAPIFunction(urlString: Constant.GlobalConstants.k_MenuControls, parameter:"appID=22", success: { (responseObject) in
            if (responseObject?["status"] as! String == "success")
            {
                for detail in responseObject?["layers"] as! NSArray
                    /// check json result as dictionary and copy that dictionary in aStatus object
                {
                    let allLayers = AllLayers()
                    allLayers.getAllLayers(dictionary: detail as! NSDictionary)
                    arrayLayers.add(allLayers)
                }
                let notificationName = Notification.Name("ToReloadTableView")
                NotificationCenter.default.post(name: notificationName, object: nil)
            
            }
            
        }) { (NSError) in
            
        }
    }
    func registerForPushNotifications(application: UIApplication) {
        
        if #available(iOS 10.0, *){
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert], completionHandler: {(granted, error) in
                if (granted)
                {
                    UIApplication.shared.registerForRemoteNotifications()
                }
                else{
                    //Do stuff if unsuccessful...
                }
            })
        }
            
        else{ //If user is not on iOS 10 use the old methods we've been using
            // let notificationSettings = UIUserNotificationSettings(forTypes: [.Badge, .Sound, .Alert], categories: nil)
            // application.registerUserNotificationSettings(notificationSettings)
            
        }
        
    }
    
    public func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Swift.Void)
    {
        
        contentDic = userInfo["aps"] as! NSDictionary!
        let string = contentDic["alert"] as! String
        let seperatedString = string.components(separatedBy: "from")
        if seperatedString[0] as String == "You have one message "
        {
            // Post a notification
            let dict = convertToDictionary(text: contentDic["message"] as! String)
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: messsageIndentifier), object: dict)
        }
        /*["type_name": message, "ownerID": 1114, "id": 1101, "image": https://www.truckersam.com/images/noimage.png, "Name": kamal1, "appID": 22, "message": 0fcrkliplatz, "type": 1]
         */
    }
    
    func convertToDictionary(text: String) -> [String: Any]?
    {
        if let data = text.data(using: .utf8)
        {
            do
            {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            }
            catch
            {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        deviceTokenToPass = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print(deviceTokenToPass)
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        print("i am not available in simulator \(error)")
        
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        self.saveContext()
    }
    //MARK: SKMapVersioningDelegate
    
    func mapsVersioningManager(versioningManager: SKMapsVersioningManager!, detectedNewAvailableMapVersion latestMapVersion: String!, currentMapVersion: String!) {
        print("Current map version: " + currentMapVersion + " \n Latest map version: " + latestMapVersion)
        
        let message: String? = "A new map version is available on the server: " + latestMapVersion + "\n Current map version: " + currentMapVersion
        
        DispatchQueue.main.async() {
            let alert = UIAlertController(title: "New map version available", message: message, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "Update", style:  UIAlertActionStyle.default, handler: { (action: UIAlertAction!) -> Void in
                let availableVersions: Array = SKMapsService.sharedInstance().mapsVersioningManager.availableMapVersions as! Array<SKVersionInformation>
                let latestVersion = availableVersions[0]
                SKMapsService.sharedInstance().mapsVersioningManager.update(toVersion: latestVersion.version)
            }))
            
        }
        
    }
    
    func mapsVersioningManager(versioningManager: SKMapsVersioningManager!, loadedWithMapVersion currentMapVersion: String!) {
        print("Map version file download finished.\n")
        //needs to be updated for a new map version
    }
    
    func mapsVersioningManager(versioningManager: SKMapsVersioningManager!, loadedWithOfflinePackages packages: [AnyObject]!, updatablePackages: [AnyObject]!) {
        print(String(updatablePackages.count) + " updatable packages")
        for package: SKMapPackage in updatablePackages as! Array<SKMapPackage>
        {
            print(package.name)
        }
    }

    // MARK: - Core Data stack
    
    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.CoredataDemo" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1] as NSURL
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "TruckerSamModel", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    
}

