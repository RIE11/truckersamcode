//
//  UpdateAccountVC.swift
//  TruckerSam
//
//  Created by Rini Mital on 1/16/17.
//  Copyright © 2017 Monica. All rights reserved.
//

import UIKit
import Foundation
import MapKit
import Alamofire
import PASImageView


class UpdateAccountVC: BaseVC  , UITextFieldDelegate
{
    @IBOutlet weak var headerBackgroundView:UIView!
    @IBOutlet weak var headerLogoView:UIImageView!
    @IBOutlet weak var scrollView:UIScrollView!
    
    @IBOutlet weak var mynameTxt:UITextField!
    @IBOutlet weak var countrycodeTxt:UITextField!
    @IBOutlet weak var phoneTxt:UITextField!
    @IBOutlet weak var emailTxt:UITextField!
    @IBOutlet weak var ques1Lbl:UILabel!
    @IBOutlet weak var ques2Lbl:UILabel!
    @IBOutlet weak var ans1Btn:UIButton!
    @IBOutlet weak var ans2Btn:UIButton!
    @IBOutlet weak var continueBtn:UIButton!
    
    @IBOutlet weak var ques1Stackview:UIStackView!
    @IBOutlet weak var ques2Stackview:UIStackView!
    
    @IBOutlet weak var profileImgView      : UIImageView!
    @IBOutlet weak var imageViewProfile    : PASImageView!

    @IBOutlet weak var agreeTrackLocationBtn:UIButton!

   // var userModel = UserModel()

    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    var latitude: Double!
    var longitude: Double!
    let loc = GetLocation()

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        countrycodeTxt.delegate = self
        phoneTxt.delegate = self
        
        //set background color
        headerBackgroundView.backgroundColor = Constant.GlobalConstants.kColor_ScreenBackground
        scrollView.backgroundColor = Constant.GlobalConstants.kColor_ScreenBackground
        ans1Btn.tag = Constant.GlobalConstants.kAnswer1_ButtonTag
        ans2Btn.tag = Constant.GlobalConstants.kAnswer2_ButtonTag

        self.ques1Stackview.isHidden = false
        self.ques2Stackview.isHidden = false
       
        
        countrycodeTxt.isUserInteractionEnabled = false
        phoneTxt.isUserInteractionEnabled = false
        
         let defaults = UserDefaults.standard

        mynameTxt.text = defaults.object(forKey: "UserName") as? String
        emailTxt.text = defaults.object(forKey: "email") as? String
       
        
        let stringPhone = defaults.object(forKey: "phone") as? String
        let arrayPhone = stringPhone?.components(separatedBy: "-")
        countrycodeTxt.text = String(format: "%@",(arrayPhone?[0])!)
        phoneTxt.text =  String(format: "%@",(arrayPhone?[1])!)

       
        self.ques1Lbl.text = defaults.object(forKey: "question1") as? String
        self.ques2Lbl.text = defaults.object(forKey: "question2") as? String

        ans1Btn.setTitle(defaults.object(forKey: "answer1") as? String, for: UIControlState.normal)
        ans2Btn.setTitle(defaults.object(forKey: "answer2") as? String, for: UIControlState.normal)
        
        if defaults.object(forKey: "UserTrackLocation") as? Bool == true
        {
            agreeTrackLocationBtn.setImage(UIImage(named:"checked_cb"), for: UIControlState.normal)
        }
        else
        {
            agreeTrackLocationBtn.setImage(UIImage(named:"uncheck_cb"), for: UIControlState.normal)
        }
        
        
        // Round Rect of userimage
        profileImgView.layer.cornerRadius = profileImgView.frame.size.width/2
        profileImgView.clipsToBounds = true
        profileImgView.image = UIImage(named:"placeholder_UserImg")
        profileImgView.isHidden = true
        
        imageViewProfile.imageURL(URL: NSURL(string: (defaults.object(forKey: "UserImage") as! String?)!)! as URL)
        imageViewProfile.layer.cornerRadius = imageViewProfile.frame.size.width/2
        imageViewProfile.clipsToBounds = true
        imageViewProfile.isHidden = false
        


        latitude  =  0.00
        longitude =  0.00
        locManager.delegate = self
        locManager.desiredAccuracy = kCLLocationAccuracyBest
        locManager.requestWhenInUseAuthorization()
        locManager.requestAlwaysAuthorization()
        locManager.startMonitoringSignificantLocationChanges()
        
        if CLLocationManager.locationServicesEnabled()
        {
            locManager.startUpdatingLocation()
        }
        
 

    }
    override func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        currentLocation = manager.location
        print(currentLocation.coordinate.latitude)
        print(currentLocation.coordinate.longitude)
        
        latitude = currentLocation.coordinate.latitude
        longitude = currentLocation.coordinate.longitude
        
        self.locManager.stopUpdatingLocation()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
   
    
    
    @IBAction func AnswerAction(sender: UIButton)
    {
        switch  sender.tag {
        case Constant.GlobalConstants.kAnswer1_ButtonTag:
            let btnTitlestr:String = (sender.titleLabel?.text)! as String
            switch  btnTitlestr {
            case "?":
                sender .setTitle("YES", for: UIControlState.normal)
                break
            case "YES":
                sender .setTitle("NO", for: UIControlState.normal)
                break
            case "NO":
                sender .setTitle("YES", for: UIControlState.normal)
                break
            default:
                break
            }
        case Constant.GlobalConstants.kAnswer2_ButtonTag:
            let btnTitlestr:String = (sender.titleLabel?.text)! as String
            switch  btnTitlestr {
            case "?":
                sender .setTitle("YES", for: UIControlState.normal)
                break
            case "YES":
                sender .setTitle("NO", for: UIControlState.normal)
                break
            case "NO":
                sender .setTitle("YES", for: UIControlState.normal)
                break
            default:
                break
            }
        default:
            break
        }
        
    }
    @IBAction func ContinueAction(sender: AnyObject)
    {
        if mynameTxt.text!.isEmpty
        {
            self.AlertViewOneButtonPress(ButtonTitle: "OK", message: "Please fill the empty fields.")
            return
        }
        
        if ans1Btn.titleLabel?.text == "?"
        {
            self.AlertViewOneButtonPress(ButtonTitle: "OK", message: "Please answer first question")
            return
        }
        if ans2Btn.titleLabel?.text == "?"
        {
            self.AlertViewOneButtonPress(ButtonTitle: "OK", message: "Please answer second question")
            return
        }
        if !(emailTxt.text!.isEmpty)
        {
            if !isValidEmail(testStr: emailTxt.text!)
            {
                self.AlertViewOneButtonPress(ButtonTitle: "OK", message: "Please enter valid email id.")
                return
            }
        }
        
        UserUpdateAPI()
    }
    @IBAction func ChooseProfileImageAction(sender: AnyObject)
    {
        buttonOpenCameraActionSheet()
    }
    
    //UIImagePickerControllerDelegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject])
    {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        {
            let rect = CGRect(x:0 , y:0 , width:image.size.width/6 , height:image.size.height/6)
            UIGraphicsBeginImageContext(rect.size)
            image.draw(in: rect)
            let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            let compressedImageData = UIImageJPEGRepresentation(resizedImage!, 0.1)
            profileImgView.image = UIImage(data: compressedImageData!)!
            profileImgView.isHidden = false
            imageViewProfile.isHidden = true
        }
        else
        {
            print("Something went wrong")
        }
        
         self.dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(picker: UIImagePickerController){
        picker.dismiss(animated: true, completion: nil)
    }
    

    func UserUpdateAPI()
    {
        
            MBProgressHUD.showAdded(to: self.view, animated: true)
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let defaults = UserDefaults.standard
        
            var parameter1 = [String:String]()
            
            if agreeTrackLocationBtn.imageView?.image == UIImage(named:"uncheck_cb")
            {
                parameter1 = [ "truck_type":"",
                               "email":self.emailTxt.text!,
                               "i_am_a":"",
                               "username":"",
                               "full_name":self.mynameTxt.text!,
                               "state_base":"",
                               "dob":"",
                               "app_id":"\(Constant.GlobalConstants.kAppID)",
                    "answer1":(self.ans1Btn.titleLabel?.text!)! as String,
                    "answer2":(self.ans2Btn.titleLabel?.text!)! as String,
                    "question1":self.ques1Lbl.text!,
                    "question2":self.ques2Lbl.text!,
                    "phone": self.countrycodeTxt.text! + "-" + self.phoneTxt.text!,
                    "id":"\(self.GetIntegerUserDefault(ForKey: "UserID"))",
                    "device_user_agent":"",
                    "web_user_id":"",
                    "how_hear_about_us":"",
                    "deviceID":UIDevice.current.identifierForVendor!.uuidString,
                    "latitude":"0.0",
                    "longitude":"0.0",
                    "token":((appDelegate.deviceTokenToPass) != nil) ? (appDelegate.deviceTokenToPass) as String:"123456",
                    "appName":Constant.GlobalConstants.kAppName
                ]
                defaults.set(false, forKey: "UserTrackLocation")
                
            }
            else
            {
                parameter1 = [ "truck_type":"",
                               "email":self.emailTxt.text!,
                               "i_am_a":"",
                               "username":"",
                               "full_name":self.mynameTxt.text!,
                               "state_base":"",
                               "dob":"",
                               "app_id":"\(Constant.GlobalConstants.kAppID)",
                    "answer1":(self.ans1Btn.titleLabel?.text!)! as String,
                    "answer2":(self.ans2Btn.titleLabel?.text!)! as String,
                    "question1":self.ques1Lbl.text!,
                    "question2":self.ques2Lbl.text!,
                    "phone": self.countrycodeTxt.text! + "-" + self.phoneTxt.text!,
                    "id":"\(self.GetIntegerUserDefault(ForKey: "UserID"))",
                    "device_user_agent":"",
                    "web_user_id":"",
                    "how_hear_about_us":"",
                    "deviceID":UIDevice.current.identifierForVendor!.uuidString,
                    "latitude":"\(latitude!)",
                    "longitude":"\(longitude!)",
                    "token":((appDelegate.deviceTokenToPass) != nil) ? (appDelegate.deviceTokenToPass) as String:"123456",
                    "appName":Constant.GlobalConstants.kAppName
                ]
                defaults.set(true, forKey: "UserTrackLocation")
                
            }

            
            let URL = try! URLRequest(url: Constant.GlobalConstants.k_baseUrl+Constant.GlobalConstants.k_UpdateProfileApi, method: .post, headers: nil)
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(UIImagePNGRepresentation(self.profileImgView.image!)!, withName: "image", fileName: "image.png", mimeType: "image/png")
                // import parameters
                for (key, value) in parameter1
                {
                    multipartFormData.append((value as String).data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!, withName: key)
                }
                
            }, with: URL, encodingCompletion: {
                encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                        print(response)
                        let json =  response.result.value as! NSDictionary
                        
                        if (json["status"] as! String == "success")
                        {
                            defaults.set(json["full_name"] as! String, forKey: "UserName")
                            defaults.set(json["email"] as! String, forKey: "email")
                            defaults.set(self.countrycodeTxt.text! + "-" + self.phoneTxt.text!, forKey: "phone")
                            defaults.set(json["question1"] as! String, forKey: "question1")
                            defaults.set(json["question2"] as! String, forKey: "question2")
                            defaults.set((self.ans1Btn.titleLabel?.text!)! as String, forKey: "answer1")
                            defaults.set((self.ans2Btn.titleLabel?.text!)! as String, forKey: "answer2")
                            defaults.set(json["image"] as! String, forKey: "UserImage")
                            self.navigationController!.popViewController(animated: true)
                            
                        }
                        debugPrint("SUCCESS RESPONSE: \(response)")
                    }
                case .failure(let encodingError):
                    // hide progressbas here
                    print("ERROR RESPONSE: \(encodingError)")
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    
                }
            })

    }
    
    @IBAction func AgreeTrackLocationAction(sender: UIButton)
    {
        if agreeTrackLocationBtn.imageView?.image == UIImage(named:"uncheck_cb")
        {
            agreeTrackLocationBtn.setImage(UIImage(named:"checked_cb"), for: UIControlState.normal)
        }
        else
        {
            agreeTrackLocationBtn.setImage(UIImage(named:"uncheck_cb"), for: UIControlState.normal)
        }
    }
    
    @IBAction func backAction(sender: UIButton)
    {
        self.navigationController!.popViewController(animated: true)

    }

}
struct Typealiases
{
    typealias JSONDict = [String:Any]
}

class GetLocation {
    
    let locManager = CLLocationManager()
    var currentLocation: CLLocation!
    
    func getAdress(completion: @escaping (Typealiases.JSONDict) -> ()) {
        
        locManager.requestWhenInUseAuthorization()
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
            currentLocation = locManager.location
            
            let geoCoder = CLGeocoder()
            geoCoder.reverseGeocodeLocation(currentLocation) { (placemarks, error) -> Void in
                
                if error != nil {
                    print("Error getting location: \(error)")
                } else {
                    let placeArray = placemarks as [CLPlacemark]!
                    var placeMark: CLPlacemark!
                    placeMark = placeArray?[0]
                    completion(placeMark.addressDictionary as! Typealiases.JSONDict)
                }
            }
        }
    }
}
