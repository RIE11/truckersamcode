//
//  ContentPageVC.swift
//  TruckerSam
//
//  Created by Rini Mital on 2/7/17.
//  Copyright © 2017 Monica. All rights reserved.
//

import UIKit

class ContentPageVC: BaseVC, UITableViewDelegate , UITableViewDataSource
{
    @IBOutlet weak var headerBackgroundView:UIView!
    @IBOutlet weak var headerLogoView:UIImageView!
    @IBOutlet weak var tableView : UITableView!
    var titleStr: String!
    var linkStr: String!
    
    override func viewDidLoad()
    {
      
        super.viewDidLoad()
        //set background color
        
        headerBackgroundView.backgroundColor = Constant.GlobalConstants.kColor_ScreenBackground
        self.view.backgroundColor = Constant.GlobalConstants.kColor_ScreenBackground
        
//        self.tableView.delegate = self
//        self.tableView.dataSource = self
       
        GetPageContentList()


        // Do any additional setup after loading the view.
    }
    //MARK: - table Delegates
    private func numberOfSectionsInTableView(tableView: UITableView!) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell: AppUsersVCCell = tableView.dequeueReusableCell(withIdentifier: "AppUsersVCCell") as! AppUsersVCCell
        
//        let appUserModel = (entities[indexPath.item] as! AppUsersModel)
//        
//        cell.app_userName!.text  = appUserModel.App_UserNameStr
//        cell.app_userPhone!.text = appUserModel.App_UserPhoneStr
//        
//        if(indexPath.item == 0)
//        {
//            cell.app_userImage.placeHolderImage = UIImage(named: "splash_logo")
//        }
//        else
//        {
//            DispatchQueue.main.async
//                {
//                    cell.app_userImage.backgroundColor = UIColor.clear
//                    cell.app_userImage.layer.cornerRadius = cell.app_userImage.frame.size.width/2
//                    cell.app_userImage.clipsToBounds = true
//                    cell.app_userImage.imageURL(URL: NSURL(string: self.userImageAry.object(at: indexPath.row) as! String)! as URL)
//            }
//        }
//        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        
    }
    
    //MARK: - API method
    func GetPageContentList()
    {
        //https://www.truckersam.com/webservice/get_static_page?page=DAT%20TruckersEdge
        /* {
         "page": [{
         "id": "1050",
         "section_name": "Business Booster",
         "category_name": "Get Freight",
         "page_name": "DAT TruckersEdge",
         "page_title": "Get One Month Free",
         "video_url": "",
         "webview_link": "",
         "type": "Content",
         "image": "https:\/\/www.truckersam.com\/uploaded_images\/appimages\/user\/1477896221205430.jpg",
         "image_link": "http:\/\/www.truckersedge.net\/promo734",
         "content": " ** *
         Click on the picture andget One Month Free < \/strong> *** <\/p>",
         "submission_time": "2016-10-31 06:44:28",
         "articles": [{
         "page_title": "Trucking News",
         "page_id": "1050",
         "video_url": "",
         "image": "https:\/\/www.truckersam.com\/uploaded_images\/appimages\/user\/1473937993259337.jpg",
         "image_link": "http:\/\/www.truckersedge.net\/trucking-blog\/",
         "content": "
         
         Read The Most Updated Trucking Articles < \/p>\n
         
         **
         *
         Click on the picture to read the articles ** * < \/p>",
         "submission_time": "2016-09-15 11:13:13"
         }]
         }],
         "status": "success",
         "message": "success"
         }*/
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        WebServicesVC.sharedInstance.CallGetAPIFunction(urlString: Constant.GlobalConstants.K_GetStaticPage, parameter:"page=\(linkStr.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!)", success: { (responseObject) in
            
            print(responseObject!)
            
            if (responseObject?["status"] as! String == "success")
            {
                print(responseObject?["page"] as! NSDictionary)
                let page = responseObject?["page"] as! NSDictionary
                print(page["articles"] as! NSDictionary)

            }
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
            //self.tableView.reloadData()
            
        })
        { (NSError) in
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
        }
    }
    
   
    @IBAction func BackAction(sender: AnyObject)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
