//
//  HiringCentreVC.m
//  DemoMap
//
//  Created by Monica on 2/2/15.
//  Copyright (c) 2015 Monica. All rights reserved.
//

#import "HiringCentreVC.h"
#import "TruckerSam_Connection.h"
#import <TruckerSam-Swift.h>


#define baseUrl                         @"https://www.truckersam.com/webservice/"
#define GetHiringCenter                 @"get_hiring_center_ads?page="
#define GetHiringCenterFilter           @"get_hiring_center_filter_ads?page="
#define GetStateBaseName                @"get_all_state_name"

@interface HiringCentreVC ()

@property(nonatomic,retain)IBOutlet UITableView *tblHiring;
@property(nonatomic,weak)IBOutlet UILabel *titleLbl;
@property(nonatomic,weak)IBOutlet UITextField *txtFreeSearch;
@property(nonatomic,weak)IBOutlet UILabel *lblState;
@property(nonatomic,weak)IBOutlet UIButton *btnCross;
@property(nonatomic,retain)NSDictionary *dic_State;

@property(nonatomic,retain)NSMutableDictionary *dic_Hiring;
@property(nonatomic,retain)NSMutableArray *aryJobtitle;
@property(nonatomic,retain)NSMutableArray *aryJobtype;
@property(nonatomic,retain)NSMutableArray *aryCompnayName;
@property(nonatomic,retain)NSMutableArray *aryCompAddres;
@property(nonatomic,retain)NSMutableArray *aryJobDescp;
@property(nonatomic,retain)NSMutableArray *aryContactPhone;
@property(nonatomic,retain)NSMutableArray *aryContactName;
@property(nonatomic,retain)NSMutableArray *aryContactEmail;
@property(nonatomic,retain)NSMutableArray *aryRefrence;
@property(nonatomic,retain)NSMutableArray *arywebLink;

@property(nonatomic)int pageNumber;
@property (nonatomic) BOOL loading;
@property (nonatomic) BOOL isSearch;
@property(nonatomic)UIActivityIndicatorView *spinner;
@property(nonatomic)NSUInteger pickerrow;

@property(nonatomic,weak)IBOutlet UIView *pickerView;
@property(nonatomic,weak)IBOutlet UIPickerView *picker;

@property(nonatomic,retain)IBOutlet UIImageView *headerImg;
@property(nonatomic,retain)IBOutlet UIButton *menuBtn;
@property(nonatomic,retain)IBOutlet UIButton *backBtn;
@property(nonatomic,retain)IBOutlet UIImageView *menuImg;
@property(nonatomic,retain)IBOutlet UIImageView *backImg;
@end

@implementation HiringCentreVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    expandedIndexes=[[NSMutableDictionary alloc]init];
    // Do any additional setup after loading the view.
    
    _dic_Hiring=[[NSMutableDictionary alloc]init];
    _aryJobtitle=[[NSMutableArray alloc]init];
    _aryJobtype=[[NSMutableArray alloc]init];
    _aryCompnayName=[[NSMutableArray alloc]init];
    _aryCompAddres=[[NSMutableArray alloc]init];
    _aryJobDescp=[[NSMutableArray alloc]init];
    _aryContactName=[[NSMutableArray alloc]init];
    _aryContactPhone=[[NSMutableArray alloc]init];
    _aryContactEmail=[[NSMutableArray alloc]init];
    _aryRefrence=[[NSMutableArray alloc]init];
    _arywebLink=[[NSMutableArray alloc]init];
    
    _pageNumber=0;
    
   [self StateBaseAPiCall];
    
    _txtFreeSearch.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Free Search" attributes:@{NSFontAttributeName:_txtFreeSearch.font, NSForegroundColorAttributeName:_txtFreeSearch.textColor}];
    
    _btnCross.hidden=YES;

    [self.view bringSubviewToFront:_headerImg];
    [self.view bringSubviewToFront:_menuBtn];
    [self.view bringSubviewToFront:_backBtn];
    [self.view bringSubviewToFront:_menuImg];
    [self.view bringSubviewToFront:_backImg];
}


#pragma mark-State Base Api
-(void)StateBaseAPiCall
{
    HUD=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [[TruckerSam_Connection sharedManager]getDataForUrl:[baseUrl stringByAppendingString:GetStateBaseName] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
       
         NSLog(@"%@",responseObject);
         _dic_State=responseObject;
         [_picker reloadAllComponents];
         [self HiringCentre];
     }
      failure:^(AFHTTPRequestOperation *operation, NSError *error){
          [HUD setHidden:YES];
          NSLog(@"Enter into failure is %@",error.localizedDescription);
          UIAlertView  *alert=nil;
          alert=[[UIAlertView alloc]initWithTitle:@"Error!" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
          [alert show];
      }];
}

-(void)SearchHiringCentre
{
    NSLog(@"search hiring center");
    
    if ([_lblState.text isEqualToString:@"All States"])
    {
        _lblState.text=@"";
    }
    
    NSDictionary *parameters =@{
                               @"state":(_lblState.text)?_lblState.text:@"",
                               @"free_text":(_txtFreeSearch.text)?_txtFreeSearch.text:@""
                               };
    
    [[TruckerSam_Connection sharedManager]getDataForUrl:[baseUrl stringByAppendingString:[NSString stringWithFormat:@"%@%i",GetHiringCenterFilter,_pageNumber]] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         self.loading=NO;
         _dic_Hiring=responseObject;
         NSLog(@"_dic_Hiring %@",_dic_Hiring);
         
         for (int i=0; i<[[_dic_Hiring objectForKey:@"hiring_center"] count];i++)
         {
             NSString *jobTitleStr=[[[_dic_Hiring objectForKey:@"hiring_center"]objectAtIndex:i]objectForKey:@"job_title"];
             if ([jobTitleStr isEqualToString:@""] || jobTitleStr==(id)[NSNull null] || jobTitleStr.length==0) {
                 [_aryJobtitle addObject:@""];
             }else{
                 [_aryJobtitle addObject:[[[_dic_Hiring objectForKey:@"hiring_center"]objectAtIndex:i]objectForKey:@"job_title"]];}
             
             NSString *CompnyNameStr=[[[_dic_Hiring objectForKey:@"hiring_center"]objectAtIndex:i]objectForKey:@"company_name"];
             if ([jobTitleStr isEqualToString:@""] || jobTitleStr==(id)[NSNull null] || jobTitleStr.length==0) {
                 [_aryCompnayName addObject:@""];
             }else{
                 [_aryCompnayName addObject:CompnyNameStr];}
             
             NSString *cmpnyAddStr=[[[_dic_Hiring objectForKey:@"hiring_center"]objectAtIndex:i]objectForKey:@"company_address"];
             if ([cmpnyAddStr isEqualToString:@""] || cmpnyAddStr==(id)[NSNull null] || cmpnyAddStr.length==0) {
                 [_aryCompAddres addObject:@""];
             }else{
                 [_aryCompAddres addObject:cmpnyAddStr];}
             
             NSString *jobTypeStr=[[[_dic_Hiring objectForKey:@"hiring_center"]objectAtIndex:i]objectForKey:@"job_type"];
             if ([jobTypeStr isEqualToString:@""] || jobTypeStr==(id)[NSNull null] || jobTypeStr.length==0) {
                 [_aryJobtype addObject:@""];
             }else{
                 [_aryJobtype addObject:jobTypeStr];}
             
             NSString *jobDescpStr=[[[_dic_Hiring objectForKey:@"hiring_center"]objectAtIndex:i]objectForKey:@"job_description"];
             if ([jobDescpStr isEqualToString:@""] || jobDescpStr==(id)[NSNull null] || jobDescpStr.length==0) {
                 [_aryJobDescp addObject:@""];
             }else{
                 [_aryJobDescp addObject:jobDescpStr];}
             
             NSString *contactNameStr=[[[_dic_Hiring objectForKey:@"hiring_center"]objectAtIndex:i]objectForKey:@"company_name"];
             if ([contactNameStr isEqualToString:@""] || contactNameStr==(id)[NSNull null] || contactNameStr.length==0) {
                 [_aryContactName addObject:@""];
             }else{
                 [_aryContactName addObject:contactNameStr];}
             
             NSString *contactPhoneStr=[[[_dic_Hiring objectForKey:@"hiring_center"]objectAtIndex:i]objectForKey:@"contact_phone"];
             if ([contactPhoneStr isEqualToString:@""] || contactPhoneStr==(id)[NSNull null] || contactPhoneStr.length==0) {
                 [_aryContactPhone addObject:@""];
             }else{
                 [_aryContactPhone addObject:contactPhoneStr];}
             
             NSString *contactEmailStr=[[[_dic_Hiring objectForKey:@"hiring_center"]objectAtIndex:i]objectForKey:@"contact_email"];
             if ([contactEmailStr isEqualToString:@""] || contactEmailStr==(id)[NSNull null] || contactEmailStr.length==0) {
                 [_aryContactEmail addObject:@""];
             }else{
                 [_aryContactEmail addObject:contactEmailStr];}
             
             NSString *refStr=[[[_dic_Hiring objectForKey:@"hiring_center"]objectAtIndex:i]objectForKey:@"reference_number"];
             if ([refStr isEqualToString:@""] || refStr==(id)[NSNull null] || refStr.length==0) {
                 [_aryRefrence addObject:@""];
             }else{
                 [_aryRefrence addObject:refStr];}
             
             NSString *webStr=[[[_dic_Hiring objectForKey:@"hiring_center"]objectAtIndex:i]objectForKey:@"website"];
             if ([webStr isEqualToString:@""] || webStr==(id)[NSNull null] || webStr.length==0)
             {
                 [_arywebLink addObject:@""];
             }else{
                 [_arywebLink addObject:webStr];
             }
         }
         _pageNumber=[[responseObject objectForKey:@"nextPageCount"]integerValue];
         [_tblHiring reloadData];
         
         [HUD setHidden:YES];
     }
      failure:^(AFHTTPRequestOperation *operation, NSError *error){
          [HUD setHidden:YES];
          NSLog(@"Enter into failure is %@",error.localizedDescription);
          UIAlertView  *alert=nil;
          alert=[[UIAlertView alloc]initWithTitle:@"Error!" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
          [alert show];
      }];
}

-(void)HiringCentre
{
    NSLog(@"hiring center");
    NSLog(@"pageNumber %i",_pageNumber);
    
    _isSearch=NO;
    
    NSDictionary *parameters =nil;
    
    [[TruckerSam_Connection sharedManager]getDataForUrl:[baseUrl stringByAppendingString:[NSString stringWithFormat:@"%@%i",GetHiringCenter,_pageNumber]] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         self.loading=NO;
         _dic_Hiring=responseObject;
         NSLog(@"_dic_Hiring %@",_dic_Hiring);
         
         for (int i=0; i<[[_dic_Hiring objectForKey:@"hiring_center"] count];i++)
         {
             NSString *jobTitleStr=[[[_dic_Hiring objectForKey:@"hiring_center"]objectAtIndex:i]objectForKey:@"job_title"];
             if ([jobTitleStr isEqualToString:@""] || jobTitleStr==(id)[NSNull null] || jobTitleStr.length==0) {
                 [_aryJobtitle addObject:@""];
             }else{
                 [_aryJobtitle addObject:jobTitleStr];}
             
             NSString *CompnyNameStr=[[[_dic_Hiring objectForKey:@"hiring_center"]objectAtIndex:i]objectForKey:@"company_name"];
             if ([jobTitleStr isEqualToString:@""] || jobTitleStr==(id)[NSNull null] || jobTitleStr.length==0) {
                 [_aryCompnayName addObject:@""];
             }else{
                 [_aryCompnayName addObject:CompnyNameStr];}
             
             NSString *cmpnyAddStr=[[[_dic_Hiring objectForKey:@"hiring_center"]objectAtIndex:i]objectForKey:@"company_address"];
             if ([cmpnyAddStr isEqualToString:@""] || cmpnyAddStr==(id)[NSNull null] || cmpnyAddStr.length==0) {
                 [_aryCompAddres addObject:@""];
             }else{
                 [_aryCompAddres addObject:cmpnyAddStr];}
             
             NSString *jobTypeStr=[[[_dic_Hiring objectForKey:@"hiring_center"]objectAtIndex:i]objectForKey:@"job_type"];
             if ([jobTypeStr isEqualToString:@""] || jobTypeStr==(id)[NSNull null] || jobTypeStr.length==0) {
                 [_aryJobtype addObject:@""];
             }else{
                 [_aryJobtype addObject:jobTypeStr];}
             
             NSString *jobDescpStr=[[[_dic_Hiring objectForKey:@"hiring_center"]objectAtIndex:i]objectForKey:@"job_description"];
             if ([jobDescpStr isEqualToString:@""] || jobDescpStr==(id)[NSNull null] || jobDescpStr.length==0) {
                 [_aryJobDescp addObject:@""];
             }else{
                 [_aryJobDescp addObject:jobDescpStr];}
             
             NSString *contactNameStr=[[[_dic_Hiring objectForKey:@"hiring_center"]objectAtIndex:i]objectForKey:@"company_name"];
             if ([contactNameStr isEqualToString:@""] || contactNameStr==(id)[NSNull null] || contactNameStr.length==0) {
                 [_aryContactName addObject:@""];
             }else{
                 [_aryContactName addObject:contactNameStr];}
             
             NSString *contactPhoneStr=[[[_dic_Hiring objectForKey:@"hiring_center"]objectAtIndex:i]objectForKey:@"contact_phone"];
             if ([contactPhoneStr isEqualToString:@""] || contactPhoneStr==(id)[NSNull null] || contactPhoneStr.length==0) {
                 [_aryContactPhone addObject:@""];
             }else{
                 [_aryContactPhone addObject:contactPhoneStr];}
             
             NSString *contactEmailStr=[[[_dic_Hiring objectForKey:@"hiring_center"]objectAtIndex:i]objectForKey:@"contact_email"];
             if ([contactEmailStr isEqualToString:@""] || contactEmailStr==(id)[NSNull null] || contactEmailStr.length==0) {
                 [_aryContactEmail addObject:@""];
             }else{
                 [_aryContactEmail addObject:contactEmailStr];}
             
             NSString *refStr=[[[_dic_Hiring objectForKey:@"hiring_center"]objectAtIndex:i]objectForKey:@"reference_number"];
             if ([refStr isEqualToString:@""] || refStr==(id)[NSNull null] || refStr.length==0) {
                 [_aryRefrence addObject:@""];
             }else{
                 [_aryRefrence addObject:refStr];
             }
             
             NSString *webStr=[[[_dic_Hiring objectForKey:@"hiring_center"]objectAtIndex:i]objectForKey:@"website"];
             if ([webStr isEqualToString:@""] || webStr==(id)[NSNull null] || webStr.length==0) {
                 [_arywebLink addObject:@""];
             }else{
                 [_arywebLink addObject:webStr];
             }
         }
         
         _pageNumber=[[responseObject objectForKey:@"nextPageCount"]integerValue];
         [_tblHiring reloadData];
         
         
         [HUD setHidden:YES];
     }
      failure:^(AFHTTPRequestOperation *operation, NSError *error){
          [HUD setHidden:YES];
          NSLog(@"Enter into failure is %@",error.localizedDescription);
          UIAlertView  *alert=nil;
          alert=[[UIAlertView alloc]initWithTitle:@"Error!" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
          [alert show];
      }];
}

#pragma mark-Logo Action
- (IBAction)logoAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark-Back Action
- (IBAction)BackAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark-Filter Action
- (IBAction)FilterAction:(id)sender
{
    [_txtFreeSearch resignFirstResponder];
    NSLog(@"%@",_txtFreeSearch.text);
    NSLog(@"%@",_lblState.text);
    if ([_txtFreeSearch.text isEqualToString:@""] && _txtFreeSearch.text.length==0 && [_lblState.text isEqualToString:@"All States"])
    {
    }
    else
    {
        _pageNumber=0;
        
        _isSearch=YES;
        
        [_aryJobtitle removeAllObjects];
        [_aryJobtype removeAllObjects];
        [_aryCompnayName removeAllObjects];
        [_aryCompAddres removeAllObjects];
        [_aryJobDescp removeAllObjects];
        [_aryContactName removeAllObjects];
        [_aryContactPhone removeAllObjects];
        [_aryContactEmail removeAllObjects];
        [_aryRefrence removeAllObjects];
        [_arywebLink removeAllObjects];
        
        HUD=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [self SearchHiringCentre];
    }
}

#pragma mark-Read More Action
- (IBAction)ReadMoreAction:(id)sender
{
    int tag=[sender tag];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:tag inSection:0];
    
    BOOL isExpanded = ![[expandedIndexes objectForKey:indexPath] boolValue];
    NSNumber *expandedIndex = [NSNumber numberWithBool:isExpanded];
    [expandedIndexes setObject:expandedIndex forKey:indexPath];
    
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
    [_tblHiring reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark-Close Action
- (IBAction)CloseAction:(id)sender
{
    int tag=[sender tag];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:tag inSection:0];
    
    BOOL isExpanded = ![[expandedIndexes objectForKey:indexPath] boolValue];
    NSNumber *expandedIndex = [NSNumber numberWithBool:isExpanded];
    [expandedIndexes setObject:expandedIndex forKey:indexPath];
    
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
    [_tblHiring reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
}

- (IBAction)WebsiteAction:(id)sender
{
    int tag=[sender tag];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:tag inSection:0];

    WebViewVC *webViewObj = [WebViewVC new];
    webViewObj = [self.storyboard instantiateViewControllerWithIdentifier:@"WebViewVC"];
    webViewObj.titleStr=@"WebSite";
    webViewObj.linkStr=[_arywebLink objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:webViewObj animated:YES];
}

- (IBAction)CrossAction:(id)sender
{
    _btnCross.hidden=YES;
    
    [_txtFreeSearch resignFirstResponder];
    
    _txtFreeSearch.text=@"";
    _lblState.text=@"All States";
    
    if (_isSearch)
    {
        _pageNumber=0;
        
        [_aryJobtitle removeAllObjects];
        [_aryJobtype removeAllObjects];
        [_aryCompnayName removeAllObjects];
        [_aryCompAddres removeAllObjects];
        [_aryJobDescp removeAllObjects];
        [_aryContactName removeAllObjects];
        [_aryContactPhone removeAllObjects];
        [_aryContactEmail removeAllObjects];
        [_aryRefrence removeAllObjects];
        [_arywebLink removeAllObjects];
        
        HUD=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [self HiringCentre];
    }
}



#pragma mark-tableview delegate and datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_tblHiring)
    {
        if (indexPath.row == [_aryJobtitle count])
        {
            return 70;
        }
        else
        {
            NSIndexPath *indexPathKey = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
            BOOL isExpanded = [[expandedIndexes objectForKey:indexPathKey] boolValue];
            if(isExpanded)
            {
                return 382;
            }
            return 180;
        }
    }else
    {
        return 50;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==_tblHiring)
    {
    if ([_aryJobtitle count]==0)
    {
      return 0;
    }
    else{
        if (_pageNumber!=0)
        {
          return [_aryJobtitle count]+1;
        }else
        {
            return [_aryJobtitle count];
        }
    }}
    else{
        return 50;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == [_aryJobtitle count] && _pageNumber!=0)
    {
        return [self loadingCell];
    }
    else
    {
        static NSString *CellIdentifier = @"filterCell";
        NSIndexPath *indexPathKey = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
        BOOL isExpanded = [[expandedIndexes objectForKey:indexPathKey] boolValue];
        
        if(isExpanded)
        {
            _customCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            if (_customCell == nil)
            {
                [[NSBundle mainBundle] loadNibNamed:@"HiringCell" owner:self options:nil];
            }
            _customCell.webBtn.tag=indexPath.row;
            _customCell.webJobDescp.tag=indexPath.row;
            
            _customCell.closeBtn.tag=indexPath.row;
            _customCell.lblJobTitle.text=[_aryJobtitle objectAtIndex:indexPath.row];
            _customCell.lblCompanyName.text=[_aryCompnayName objectAtIndex:indexPath.row];
            _customCell.lblJobType.text=[_aryJobtype objectAtIndex:indexPath.row];
            _customCell.lblJobLocation.text=[_aryCompAddres objectAtIndex:indexPath.row];
            _customCell.lblJobDescp.text=@"Job Description:";
            
            [_customCell.webJobDescp loadHTMLString:[NSString stringWithFormat:@"<html><body style='ba	ckground-color: transparent'text=\"#FFFFFF\"><div style='font-size: 14px;''font-family:%@;' >%@</body></html>",@"Roboto-Light",[_aryJobDescp objectAtIndex:indexPath.row]] baseURL:nil];
            _customCell.lblPhone.text=[_aryContactPhone objectAtIndex:indexPath.row];
            _customCell.lblEmail.text=[_aryContactEmail objectAtIndex:indexPath.row];
            _customCell.lblName.text=[_aryContactName objectAtIndex:indexPath.row];
            _customCell.lblRefrence.text=[_aryRefrence objectAtIndex:indexPath.row];
            
            return _customCell;
            
           //[NSString stringWithFormat:@"<html><body style='background-color: transparent'><div style=' font-size:%@px;'>""</body></html>", [UIFont MaxOTFontWithSize:(IS_IPAD)?25.0:18.0]
        }
        else
        {
            _customHalfCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            if (_customHalfCell == nil)
            {
                [[NSBundle mainBundle] loadNibNamed:@"HiringHalfCell" owner:self options:nil];
            }
            _customHalfCell.webJobDescp.tag=indexPath.row;
            
            _customHalfCell.readMoreBtn.tag=indexPath.row;
            _customHalfCell.lblJobTitle.text=[_aryJobtitle objectAtIndex:indexPath.row];
            _customHalfCell.lblCompanyName.text=[_aryCompnayName objectAtIndex:indexPath.row];
            _customHalfCell.lblJobType.text=[_aryJobtype objectAtIndex:indexPath.row];
            _customHalfCell.lblJobLocation.text=[_aryCompAddres objectAtIndex:indexPath.row];
            _customHalfCell.lblJobDescp.text=@"Job Description:";
            [_customHalfCell.webJobDescp loadHTMLString:[NSString stringWithFormat:@"<html><body style='ba	ckground-color: transparent'text=\"#FFFFFF\"><div style='font-size: 14px;''font-family:%@;' >%@</body></html>",@"Roboto-Light",[_aryJobDescp objectAtIndex:indexPath.row]] baseURL:nil];
            return _customHalfCell;
        }
    }
}

- (UITableViewCell *)loadingCell
{
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:nil];
    UIActivityIndicatorView *activityIndicator =[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    activityIndicator.center = cell.center;
    [cell addSubview:activityIndicator];
    
    UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(0,40,320,20)];
    lbl.textColor=[UIColor whiteColor];
    lbl.textAlignment=NSTextAlignmentCenter;
    lbl.font=[UIFont systemFontOfSize:14];
    lbl.text=@"loading more jobs..";
    [cell addSubview:lbl];
    
    [activityIndicator startAnimating];
    cell.backgroundColor=[UIColor clearColor];
    //cell.tag = kLoadingCellTag;
    return cell;
}


#pragma mark- Scrolling Delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
  float endScrolling = scrollView.contentOffset.y + scrollView.frame.size.height;
  if (endScrolling >= scrollView.contentSize.height)
  {
      if (!self.loading && _pageNumber!=0) {
          self.loading=YES;
          if (_isSearch==YES)
          {
              [self performSelector:@selector(SearchHiringCentre) withObject:nil afterDelay:0];
          }else{
              [self performSelector:@selector(HiringCentre) withObject:nil afterDelay:0];
          }
      
      }
  }
}
#pragma mark- Webview Delegate
- (void)webViewDidFinishLoad:(UIWebView *)aWebView
{
    [aWebView.scrollView setContentSize: CGSizeMake(aWebView.frame.size.width, aWebView.frame.size.height)];
}

#pragma mark- Picker view Delegate & datasource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [[_dic_State objectForKey:@"state"] count];
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [[[_dic_State objectForKey:@"state"]objectAtIndex:row]objectForKey:@"state_code"];
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    _pickerrow=row;
}

#pragma mark-State Action
- (IBAction)StateBaseAction:(id)sender
{
    [UIView animateWithDuration:0.5 animations:^{[_pickerView setFrame:CGRectMake (0,self.view.frame.size.height - _picker.frame.size.height,self.view.frame.size.width,185)];}];
    [UIView commitAnimations];
}

#pragma mark-Done Action
- (IBAction)DoneAction:(id)sender
{
    _lblState.text=[[[_dic_State objectForKey:@"state"]objectAtIndex:_pickerrow]objectForKey:@"state_code"];
    
    [UIView animateWithDuration:0.5 animations:^{[_pickerView setFrame:CGRectMake (0,self.view.frame.size.height,self.view.frame.size.width,185)];}];
    [UIView commitAnimations];
}

#pragma mark-Cancel Action
- (IBAction)CancelAction:(id)sender {
    [UIView animateWithDuration:0.5 animations:^{[_pickerView setFrame:CGRectMake (0,self.view.frame.size.height,self.view.frame.size.width,185)];}];
    [UIView commitAnimations];
}

#pragma mark- Text Field Delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *text_field = [textField.text stringByReplacingCharactersInRange:range withString:string];

    if (text_field.length > 30)
    {
        return NO;
    }
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    _btnCross.hidden=NO;
    
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
