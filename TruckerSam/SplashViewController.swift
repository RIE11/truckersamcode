//
//  SplashViewController.swift
//  TruckerSam
//
//  Created by Gurleen Singh on 1/16/17.
//  Copyright © 2017 Monica. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    override func viewDidLoad()
    {
        super.viewDidLoad()
        var initialViewController = UIViewController()

        if (UserDefaults.standard.integer(forKey: "UserID") != 0 && UserDefaults.standard.integer(forKey: "isVerified") != 0 && UserDefaults.standard.integer(forKey: "isTermsCondtion") != 0)
        {
                initialViewController = storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        }
           
            //Registration gets completed but user doesn't agree terms & condition
        else if (UserDefaults.standard.integer(forKey: "UserID") != 0 && UserDefaults.standard.integer(forKey: "isVerified") != 0 && UserDefaults.standard.integer(forKey: "isTermsCondtion") == 0)
        {
            initialViewController = storyboard?.instantiateViewController(withIdentifier: "UploadPicVC") as! UploadPicVC
        }
            //Registration gets completed but user doesn't verifiy code
        else if (UserDefaults.standard.integer(forKey: "UserID") != 0 && UserDefaults.standard.integer(forKey: "isVerified") == 0 && UserDefaults.standard.integer(forKey: "isTermsCondtion") == 0)
        {
            initialViewController = storyboard?.instantiateViewController(withIdentifier: "RegistrationVC") as! RegistrationVC
        }
            //Registration not completed
        else if (UserDefaults.standard.integer(forKey: "UserID") == 0 && UserDefaults.standard.integer(forKey: "isVerified") == 0 && UserDefaults.standard.integer(forKey: "isTermsCondtion") == 0)
        {
            initialViewController = storyboard?.instantiateViewController(withIdentifier: "RegistrationVC") as! RegistrationVC
        }
        
        self.navigationController?.pushViewController(initialViewController, animated: true)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
