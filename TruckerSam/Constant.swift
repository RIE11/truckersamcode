// dummy
import UIKit


class Constant: NSObject {
    
    struct GlobalConstants
    {
        // GLOBLE COLOR DEFINE
        static let kColor_ScreenBackground: UIColor = UIColor(red: 25.0/255.0, green: 64.0/255.0, blue: 120.0/255.0, alpha: 1.0)

        //GLOBAL BUTTON TAGS
        static let kAnswer1_ButtonTag: Int = 100
        static let kAnswer2_ButtonTag: Int = 101
        static let kAppID: Int = 22
        static let kAppName: String = "Trucker Sam"
        
        //GOOGLE API
        static let kGoogleApiKey: String = "AIzaSyATgjMzZ2DLkvIugLYaxayZt7PrAwwoWhY"
        
        // https://www.truckersam.com/webservice/menuControls
        static let k_baseUrl = "https://www.truckersam.com/webservice/"
        //"http://web2.anzleads.com/movingsos/dev/webservice/"
        static let k_QuestionApi       =    "wlQuestions"
        static let k_RegistrationApi   =    "get_user_registration?"
        static let k_UpdateProfileApi   =   "update_user_profile"

        static let k_ResendCodeApi     =    "resendCode"
        static let k_UpdateImageApi    =    "updateUserImage"
        static let k_InviteFriendAPi   =    "referLandingPage"
        static let k_GetAppUsers       =    "getuserdetail"
        static let k_GetAppControls    =    "menuControls"
        static let k_SendMessage       =    "sendMsgToBuddies_forios"
        static let k_MenuControls      =    "getAllCategories"
        static let k_AllNotifications  =    "select_buddies"
        
        static let k_WebViewTermsString =    "https://www.truckersam.com/staticpages/content_details/34"
        static let K_GetAllBranches     = "getAllBranches"
        //gurie start
        static let k_DeleteNotifications  =    "deleteBuddiesMessage"
        // end
        //https://www.truckersam.com/webservice/nearByMe1?id=1219&appID=22
        static let K_GetNearBuddies     = "nearByMe"
        //https://www.truckersam.com/webservice/get_branch_details
        static let K_GetBranchDetail    = "get_branch_details"
        
        //https://www.truckersam.com/webservice/get_static_page?page=DAT%20TruckersEdge
        static let K_GetStaticPage       = "get_static_page"

    }
    
}
