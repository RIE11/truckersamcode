//
//  Compassion_Connection.h
//  Compassion App
//
//  Created by Monica on 10/13/14.
//
//

#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperation.h"
@interface TruckerSam_Connection : NSObject
+ (TruckerSam_Connection *)sharedManager;
-(void)getDataForUrl:(NSString *)URLString
          parameters:(NSDictionary *)parameters
             success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
             failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;
@end
