//
//  ComposeMessageVC.swift
//  TruckerSam
//
//  Created by Monika on 12/29/16.
//  Copyright © 2016 Monica. All rights reserved.
//

import Foundation
import UIKit
import NWSTokenView
var isAdminbool : Bool!
class ComposeMessageVC: BaseVC, UITextViewDelegate, AppUsersListVCDelegate, NWSTokenDataSource, NWSTokenDelegate
{
    
    
    @IBOutlet weak var headerBackgroundView:UIView!
    @IBOutlet weak var headerLogoView:UIImageView!
    @IBOutlet weak var toTxtField:UITextField!
    @IBOutlet weak var msgTxtView:UITextView!
    var placeholderLabel : UILabel!
    var appUsersIDAry = NSMutableArray()
    var appUsersNameAry = NSMutableArray()
    @IBOutlet weak var tokenViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tokenBaseViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tokenView: NWSTokenView!
    let tokenViewMinHeight: CGFloat = 40.0
    let tokenViewMaxHeight: CGFloat = 150.0
    let tokenBackgroundColor = UIColor(red: 98.0/255.0, green: 203.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tokenView.layoutIfNeeded()
        tokenView.dataSource = self
        tokenView.delegate = self
        headerBackgroundView.backgroundColor = Constant.GlobalConstants.kColor_ScreenBackground
        
        appUsersIDAry = NSMutableArray()
        appUsersNameAry = NSMutableArray()
        
        // set border of textfield and textview
        tokenView.layer.borderWidth = 1.0
        tokenView.layer.borderColor = UIColor.lightGray.cgColor
        tokenView.layer.cornerRadius = 8.0
        msgTxtView.layer.borderWidth = 1.0
        msgTxtView.layer.borderColor = UIColor.lightGray.cgColor
        msgTxtView.layer.cornerRadius = 8.0
        
        msgTxtView.delegate = self
        placeholderLabel = UILabel()
        placeholderLabel.text = "Type your message here"
        placeholderLabel.font = UIFont.systemFont(ofSize: (msgTxtView.font?.pointSize)!)
        placeholderLabel.sizeToFit()
        msgTxtView.addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPoint(x: 5, y: (msgTxtView.font?.pointSize)! / 2)
        placeholderLabel.textColor = UIColor(white: 0, alpha: 0.3)
        placeholderLabel.isHidden = !msgTxtView.text.isEmpty
        isAdminbool = false
        
    }
    
    func textViewDidChange(_ textView: UITextView) {
        placeholderLabel.isHidden = !textView.text.isEmpty
    }
    
    //MARK: - button actions
    
    @IBAction func AddSenderAction(sender: UIButton)
    {
        let appUserListObj = self.storyboard?.instantiateViewController(withIdentifier: "AppUsersListVC") as? AppUsersListVC
        appUserListObj?.delegate = self
        self.navigationController?.pushViewController(appUserListObj!, animated: true)
    }
    
    func GetApp_UsersID(app_userID : String , app_userName : String , isAdmin : Bool)
    {
        if appUsersIDAry.contains(app_userID)
        {
        
        }
        else
        {
            if app_userID == "0"
            {
                isAdminbool = isAdmin
                appUsersNameAry.add(app_userName)
            }
            else
            {
                appUsersIDAry.add(app_userID)
                appUsersNameAry.add(app_userName)
            }
        }
        
        tokenView.reloadData()
    }
    
    @IBAction func SendMsgAction(sender: UIButton)
    {
        if appUsersNameAry.count == 0
        {
            self.AlertViewOneButtonPress(ButtonTitle: "OK", message: "Please add atleast one user")
            return
        }
        if msgTxtView.text == "Type your message here"
        {
            self.AlertViewOneButtonPress(ButtonTitle: "OK", message: "Kindly write some text to send")
            return
        }
        var Admin = String()
        
        if isAdminbool == true
        {
        Admin = "1"
        }
        else
        {
        Admin = "0"

        }
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let separatedUserid = appUsersIDAry.componentsJoined(by: ",")
        
        WebServicesVC.sharedInstance.CallPostAPIFunction(urlString:Constant.GlobalConstants.k_SendMessage , parameter:
            [ "message":"\(msgTxtView.text!)",
              "id":"\(self.GetIntegerUserDefault(ForKey: "UserID"))",
              "buddies":"\(separatedUserid)",
              "appId":"\(Constant.GlobalConstants.kAppID)",
              "hasAdmin":"\(Admin)",
              "appName":"\(Constant.GlobalConstants.kAppName)"]
            
            , success: { (responseObject) in
               
                print(responseObject!)
                
                if (responseObject?["status"] as! String == "success")
                {
                    //Saving data locally in user default
                    for section in 0..<self.appUsersIDAry.count
                    {
                        var namesDictionary: Dictionary<String, String> = [:]
                        namesDictionary["to"] = self.msgTxtView.text!
                        // with buddie id & user id key
                       
                        if UserDefaults.standard.dictionaryRepresentation().keys.contains( String("\(self.appUsersIDAry.object(at:section))" + "\(self.GetIntegerUserDefault(ForKey: "UserID"))"))
                        
                        {
                            
                            let placesData = UserDefaults.standard.object(forKey:  String("\(self.appUsersIDAry.object(at:section))" + "\(self.GetIntegerUserDefault(ForKey: "UserID"))")) as? NSData
                            
                            if let placesData = placesData
                            {
                                let placesArray = NSKeyedUnarchiver.unarchiveObject(with: placesData as Data) as? NSMutableArray
                                placesArray?.add(namesDictionary)
                                
                                let placesData = NSKeyedArchiver.archivedData(withRootObject: placesArray!)
                                UserDefaults.standard.set(placesData, forKey: String("\(self.appUsersIDAry.object(at:section))" + "\(self.GetIntegerUserDefault(ForKey: "UserID"))"))
                            }
                        }
                        else
                        {
                            let array = NSMutableArray()
                            array.add(namesDictionary)
                            let placesData = NSKeyedArchiver.archivedData(withRootObject: array)
                            UserDefaults.standard.set(placesData, forKey: String("\(self.appUsersIDAry.object(at:section))" + "\(self.GetIntegerUserDefault(ForKey: "UserID"))"))
                        }
                        
                        
                    }
                    
                    let alert = UIAlertController(title: "Trucker Sam", message: (responseObject?["msg"] as! NSString) as String, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {  action in
                      _ =  self.navigationController?.popViewController(animated: true)
                    }))
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)

                    self.present(alert, animated: true, completion: nil)
                    
                }
                else
                {
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)

                    self.AlertViewOneButtonPress(ButtonTitle: "OK", message: responseObject?["msg"] as! String)

                }
                
        }) { (NSError) in
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
   
        }
    }
    
    @IBAction func DiscardMsgAction(sender: UIButton)
    {
       _ =   self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func BackAction(sender: AnyObject)
    {
       _ =   self.navigationController?.popViewController(animated: true)
    }
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: NWSTokenDataSource
    func numberOfTokensForTokenView(_ tokenView: NWSTokenView) -> Int
    {
        return appUsersNameAry.count
    }
    
    func insetsForTokenView(_ tokenView: NWSTokenView) -> UIEdgeInsets?
    {
        return UIEdgeInsetsMake(5, 5, 5, 5)
    }
    
    func titleForTokenViewLabel(_ tokenView: NWSTokenView) -> String?
    {
        return "To:"
    }
    
    func titleForTokenViewPlaceholder(_ tokenView: NWSTokenView) -> String?
    {
        return "Search contacts..."
    }
    
    func tokenView(_ tokenView: NWSTokenView, viewForTokenAtIndex index: Int) -> UIView?
    {
        let contact = self.appUsersNameAry.object(at: index)
        if let token = NWSImageToken.initWithTitle(contact as! String, image: nil)
        {
            return token
        }
        
        return nil
    }
    
    // MARK: NWSTokenDelegate
    func tokenView(_ tokenView: NWSTokenView, didSelectTokenAtIndex index: Int)
    {
        let token = tokenView.tokenForIndex(index) as! NWSImageToken
        token.backgroundColor = UIColor.blue
    }
    
    func tokenView(_ tokenView: NWSTokenView, didDeselectTokenAtIndex index: Int)
    {
        let token = tokenView.tokenForIndex(index) as! NWSImageToken
        token.backgroundColor = tokenBackgroundColor
    }
    
    func tokenView(_ tokenView: NWSTokenView, didDeleteTokenAtIndex index: Int)
    {
        // Ensure index is within bounds
        if index < self.appUsersNameAry.count
        {
            //let contact = self.selectedContacts[Int(index)] as NWSTokenContact
            //contact.isSelected = false
            self.appUsersNameAry.removeObject(at: index)
            
            tokenView.reloadData()
            tokenView.layoutIfNeeded()
            //tokenView.textView.becomeFirstResponder()
            
            // Check if search text exists, if so, reload table (i.e. user deleted a selected token by pressing an alphanumeric key)
            if tokenView.textView.text != ""
            {
                //self.searchContacts(tokenView.textView.text)
            }
        }
    }
    
    func tokenView(_ tokenViewDidBeginEditing: NWSTokenView)
    {
        // Check if entering search field and it already contains text (ignore token selections)
        if tokenView.textView.isFirstResponder && tokenView.textView.text != ""
        {
            //self.searchContacts(tokenView.textView.text)
        }
    }
    
    @nonobjc internal func tokenViewDidEndEditing(_ tokenView: NWSTokenView)
    {
        
    }
    
    func tokenView(_ tokenView: NWSTokenView, didChangeText text: String)
    {
        
    }
    
    func tokenView(_ tokenView: NWSTokenView, didEnterText text: String)
    {
        
    }
    
    func tokenView(_ tokenView: NWSTokenView, contentSizeChanged size: CGSize)
    {
        self.tokenViewHeightConstraint.constant = max(tokenViewMinHeight,min(size.height, self.tokenViewMaxHeight))
        self.tokenBaseViewHeightConstraint.constant = max(tokenViewMinHeight,min(size.height, self.tokenViewMaxHeight))

        self.view.layoutIfNeeded()
    }
    
    func tokenView(_ tokenView: NWSTokenView, didFinishLoadingTokens tokenCount: Int)
    {
        
    }
}
