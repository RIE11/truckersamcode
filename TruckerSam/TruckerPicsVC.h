//
//  TruckerPicsVC.h
//  DemoMap
//
//  Created by Monica on 2/10/15.
//  Copyright (c) 2015 Monica. All rights reserved.
//

#import "TruckerPicsCell.h"
#import "AsyncImageView.h"
#import "MBProgressHUD.h"

@interface TruckerPicsVC : UIViewController<PicsImageViewSizeDelegate>
{
    MBProgressHUD *HUD;
}
@property(nonatomic)AsyncImageView *asyncImageView;

- (IBAction)BackAction:(id)sender;

@end

@interface ImageData : NSObject

@property (nonatomic, retain) NSString* username;
@property (nonatomic, retain) NSString* userImage;
@property (nonatomic, retain) NSString* caption;
@property (nonatomic, retain) NSString* image;
@property (nonatomic, retain) NSURL* imageURL;
@property (nonatomic, retain) NSString* uploadTime;
@property (nonatomic, retain) NSString* allLikeCount;
@property (nonatomic, retain) NSString* like;
@property (nonatomic, retain) NSString* imageId;
@property (nonatomic, retain) NSString* userId;
@property (nonatomic, assign) UIImage* imageDownloaded;

-(void)updateValuesWithDictionary:(NSDictionary*)values;

@end
