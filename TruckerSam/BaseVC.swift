//
//  BaseVC.swift
//  TruckerSam
//
//  Created by Monika on 12/26/16.
//  Copyright © 2016 Monica. All rights reserved.
//

import Foundation
import UIKit
import PASImageView
import FirebaseDatabase
import CoreLocation

let messsageIndentifier: String = "messsageIndentifier"


class BaseVC: UIViewController,UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CLLocationManagerDelegate
{
    var viewCustomNotification = CustomNotificationView()
    var progress: KDCircularProgress!
    var count = Int()
    var timer = Timer()
    var cllocationManger: CLLocationManager!
    var geoFire : GeoFire!
    var incomingNotification = Notifications()


    override func viewDidLoad()
    {
        
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(BaseVC.messageFromUser), name: NSNotification.Name(rawValue: messsageIndentifier), object: nil)
        cllocationManger                           = CLLocationManager()
        cllocationManger.requestWhenInUseAuthorization()
        cllocationManger.delegate                  = self
        cllocationManger.distanceFilter            = kCLDistanceFilterNone
        cllocationManger.desiredAccuracy           = kCLLocationAccuracyBest
        cllocationManger.startUpdatingLocation()
        
        //let geofireRef = FIRDatabase.database().reference()
       // geoFire = GeoFire(firebaseRef: geofireRef)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
//        geoFire.setLocation(CLLocation(latitude: manager.location!.coordinate.latitude, longitude: manager.location!.coordinate.longitude), forKey: "tracksamWithUserId1114") { (error) in
//            if (error != nil) {
//                print("An error occured: \(error)")
//            } else {
//                print("Saved location successfully!")
//            }
//        }
    }
    
    func messageFromUser(notification : NSNotification)
    {
        let notifications = Notifications()
        notifications.getAllNotifications(dictionary: notification.object as! NSDictionary)
        addCustomNotification(name: notifications.Name, message: notifications.message, image: notifications.image)
        incomingNotification = notifications
        
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        self.timer.invalidate()
        self.toRemoveViews(viewCustom: self.viewCustomNotification)
    }
    
    func addCustomNotification(name:String , message : String , image : String)
    {
        // TODO: Gurie
        // To Show circular progress view
        progress = KDCircularProgress(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        progress.startAngle = -90
        progress.progressThickness = 0.2
        progress.trackThickness = 0.1
        progress.clockwise = true
        progress.gradientRotateSpeed = 1
        progress.roundedCorners = false
        progress.glowMode = .forward
        progress.glowAmount = 0.1
        progress.set(UIColor.white)
        viewCustomNotification.viewProgress.addSubview(progress)
        viewCustomNotification.labelTimer.text = "0"
        count = 0 // Intially count will be 0 , Count is to set time of 10 seconds
        
        // To set timer of 10 seconds
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(runTimedCode), userInfo: nil, repeats: true)
        
        // Animating Circular progrees view for 10 seconds
        progress.animate(0, toAngle: 360, duration: 10) { (completed) in
            if completed
            {
                print("animation stopped, completed")
                //Stop Timer here and remove Notification view
                self.viewCustomNotification.labelTimer.text = "10"
                self.timer.invalidate()
                self.toRemoveViews(viewCustom: self.viewCustomNotification)
            }
            else
            {
                print("animation stopped, was interrupted")
            }
        }
        
        viewCustomNotification.imageViewProfile.layer.cornerRadius = viewCustomNotification.imageViewProfile.frame.size.width/2
        viewCustomNotification.imageViewProfile.clipsToBounds = true
        
        viewCustomNotification.imageViewProfile.imageURL(URL: NSURL(string: image.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!) as! URL)
        
        viewCustomNotification.labelName.text = name
        viewCustomNotification.labelMessage.text = message
        
        viewCustomNotification.viewBlack.layer.cornerRadius = 10
        viewCustomNotification.viewBlack.clipsToBounds = true
        
        viewCustomNotification.imageViewProfile.layer.cornerRadius = viewCustomNotification.imageViewProfile.frame.size.width/2
        viewCustomNotification.imageViewProfile.clipsToBounds = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(BaseVC.tapDownButton))
        viewCustomNotification.buttonDown.addGestureRecognizer(tapGesture)
        
        viewCustomNotification.insertSubview(viewCustomNotification.buttonDown, aboveSubview: viewCustomNotification)
        
        let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(BaseVC.tapDidSelectButton))
        viewCustomNotification.buttonDidSelect.addGestureRecognizer(tapGesture2)
        
       viewCustomNotification.insertSubview(viewCustomNotification.buttonDidSelect, aboveSubview: viewCustomNotification)
        
        
        viewCustomNotification.translatesAutoresizingMaskIntoConstraints = false
        toAddViews(viewCustom: viewCustomNotification)
        
        // Setting constraints programmatically
        NSLayoutConstraint(item: viewCustomNotification, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 320).isActive = true
        
        NSLayoutConstraint(item: viewCustomNotification, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 170).isActive = true
        
        NSLayoutConstraint(item: viewCustomNotification, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: self.view, attribute: NSLayoutAttribute.bottomMargin, multiplier: 1.0, constant:-80).isActive = true
        
        NSLayoutConstraint(item: viewCustomNotification, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: self.view, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0).isActive = true
        
    }
    
    func tapDownButton()
    {
        self.timer.invalidate()
        self.toRemoveViews(viewCustom: self.viewCustomNotification)
    }
    
    func tapDidSelectButton()
    {
            var namesDictionary: Dictionary<String, String> = [:]
            namesDictionary["from"] = incomingNotification.message!
            
            let key = "\(incomingNotification.ownerID!)" + "\(self.GetIntegerUserDefault(ForKey: "UserID"))"
        
            print(key)
            
            if UserDefaults.standard.object(forKey: key) != nil
            {
                let placesData = UserDefaults.standard.object(forKey: key) as? NSData
                
                if let placesData = placesData
                {
                    let placesArray = NSKeyedUnarchiver.unarchiveObject(with: placesData as Data) as? NSMutableArray
                    placesArray?.add(namesDictionary)
                    let placesData = NSKeyedArchiver.archivedData(withRootObject: placesArray!)
                    UserDefaults.standard.set(placesData, forKey: key)
                }
            }
            else
            {
                let array = NSMutableArray()
                array.add(namesDictionary)
                let placesData = NSKeyedArchiver.archivedData(withRootObject: array)
                UserDefaults.standard.set(placesData, forKey:key)
                
            }
        
        self.toDeleteNotificationsAPI(stringNotificationId:incomingNotification.id,stringOwnerId:incomingNotification.ownerID,stringUserName:incomingNotification.Name,stringUserImage:incomingNotification.image)
       
        
        }
        
     func toDeleteNotificationsAPI(stringNotificationId : String , stringOwnerId : String, stringUserName : String , stringUserImage : String)
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        //https://www.truckersam.com/webservice/deleteBuddiesMessage?id=
        WebServicesVC.sharedInstance.CallGetAPIFunction(urlString: Constant.GlobalConstants.k_DeleteNotifications, parameter:"id=" + "\(stringNotificationId)", success: { (responseObject) in
            
            print(responseObject!)
            let chatVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController
            chatVC?.selectedOwnerId  = stringOwnerId
            chatVC?.selectedUserName  = stringUserName
            chatVC?.selectedUserImage = stringUserImage
            chatVC?.fromNotificationView = false
            self.navigationController?.pushViewController(chatVC!, animated: true)
            
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
            
        }) { (NSError) in
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
        }
    }
  
    func runTimedCode()
    {
        count = count + 1
        viewCustomNotification.labelTimer.text = "\(count)"
    }

    func toAddViews(viewCustom : UIView)
    {
        let animation      = CATransition()
        animation.type     = kCATransitionFade
        animation.duration = 0.4
        viewCustom.layer .add(animation, forKey: nil)
        self.view.addSubview(viewCustom)
    }
    
    func toRemoveViews(viewCustom : UIView)
    {
        let animation      = CATransition()
        animation.type     = kCATransitionFade
        animation.duration = 0.4
        viewCustom.layer .add(animation, forKey: nil)
        viewCustom.removeFromSuperview()
    }
    
    func animateButtonTapped()
    {
        
        
    }
    func isValidEmail(testStr:String) -> Bool
    {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func AlertViewOneButtonPress (ButtonTitle:String , message:String)
    {
        let alert = UIAlertController(title:"", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func buttonOpenCameraActionSheet()
    {
        let alertController = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
        
        let CameraAction = UIAlertAction(title: "Camera", style: .default) { (action) in
            self.openCamera()
        }
        alertController.addAction(CameraAction)
        
        let GalleryAction = UIAlertAction(title: "Gallery", style: .default) { (action) in
            self.openGallery()
        }
        alertController.addAction(GalleryAction)
        
        let CancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
        }
        alertController.addAction(CancelAction)
        
        self.present(alertController, animated: true) {
            
        }

    }
    
    //=================USER DEFAULTS===================
    func SetIntegerUserDefault (ForKey:String, ForValue:Int)
    {
        let defaults = UserDefaults.standard
        defaults.set(ForValue, forKey: ForKey)
    }
    func GetIntegerUserDefault (ForKey:String) -> Int
    {
        return UserDefaults.standard.integer(forKey: ForKey)
    }
    
    //==================USER DEFAULTS===================
    
 
    func openGallery()
    {
        let picker : UIImagePickerController = UIImagePickerController()
        picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        picker.delegate = self
        picker.allowsEditing = false
        self.present(picker, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            //load the camera interface
            let picker : UIImagePickerController = UIImagePickerController()
            picker.sourceType = UIImagePickerControllerSourceType.camera
            picker.delegate = self
            picker.allowsEditing = false
            self.present(picker, animated: true, completion: nil)
        }
        else
        {
            //no camera available
            self.AlertViewOneButtonPress(ButtonTitle: "OK", message: "There is no camera available")
            
        }
    }
    
    func getCountryCallingCode(countryRegionCode:String)->String{
        
        let prefixCodes = ["AF": "93", "AE": "971", "AL": "355", "AN": "599", "AS":"1", "AD": "376", "AO": "244", "AI": "1", "AG":"1", "AR": "54","AM": "374", "AW": "297", "AU":"61", "AT": "43","AZ": "994", "BS": "1", "BH":"973", "BF": "226","BI": "257", "BD": "880", "BB": "1", "BY": "375", "BE":"32","BZ": "501", "BJ": "229", "BM": "1", "BT":"975", "BA": "387", "BW": "267", "BR": "55", "BG": "359", "BO": "591", "BL": "590", "BN": "673", "CC": "61", "CD":"243","CI": "225", "KH":"855", "CM": "237", "CA": "1", "CV": "238", "KY":"345", "CF":"236", "CH": "41", "CL": "56", "CN":"86","CX": "61", "CO": "57", "KM": "269", "CG":"242", "CK": "682", "CR": "506", "CU":"53", "CY":"537","CZ": "420", "DE": "49", "DK": "45", "DJ":"253", "DM": "1", "DO": "1", "DZ": "213", "EC": "593", "EG":"20", "ER": "291", "EE":"372","ES": "34", "ET": "251", "FM": "691", "FK": "500", "FO": "298", "FJ": "679", "FI":"358", "FR": "33", "GB":"44", "GF": "594", "GA":"241", "GS": "500", "GM":"220", "GE":"995","GH":"233", "GI": "350", "GQ": "240", "GR": "30", "GG": "44", "GL": "299", "GD":"1", "GP": "590", "GU": "1", "GT": "502", "GN":"224","GW": "245", "GY": "595", "HT": "509", "HR": "385", "HN":"504", "HU": "36", "HK": "852", "IR": "98", "IM": "44", "IL": "972", "IO":"246", "IS": "354", "IN": "91", "ID":"62", "IQ":"964", "IE": "353","IT":"39", "JM":"1", "JP": "81", "JO": "962", "JE":"44", "KP": "850", "KR": "82","KZ":"77", "KE": "254", "KI": "686", "KW": "965", "KG":"996","KN":"1", "LC": "1", "LV": "371", "LB": "961", "LK":"94", "LS": "266", "LR":"231", "LI": "423", "LT": "370", "LU": "352", "LA": "856", "LY":"218", "MO": "853", "MK": "389", "MG":"261", "MW": "265", "MY": "60","MV": "960", "ML":"223", "MT": "356", "MH": "692", "MQ": "596", "MR":"222", "MU": "230", "MX": "52","MC": "377", "MN": "976", "ME": "382", "MP": "1", "MS": "1", "MA":"212", "MM": "95", "MF": "590", "MD":"373", "MZ": "258", "NA":"264", "NR":"674", "NP":"977", "NL": "31","NC": "687", "NZ":"64", "NI": "505", "NE": "227", "NG": "234", "NU":"683", "NF": "672", "NO": "47","OM": "968", "PK": "92", "PM": "508", "PW": "680", "PF": "689", "PA": "507", "PG":"675", "PY": "595", "PE": "51", "PH": "63", "PL":"48", "PN": "872","PT": "351", "PR": "1","PS": "970", "QA": "974", "RO":"40", "RE":"262", "RS": "381", "RU": "7", "RW": "250", "SM": "378", "SA":"966", "SN": "221", "SC": "248", "SL":"232","SG": "65", "SK": "421", "SI": "386", "SB":"677", "SH": "290", "SD": "249", "SR": "597","SZ": "268", "SE":"46", "SV": "503", "ST": "239","SO": "252", "SJ": "47", "SY":"963", "TW": "886", "TZ": "255", "TL": "670", "TD": "235", "TJ": "992", "TH": "66", "TG":"228", "TK": "690", "TO": "676", "TT": "1", "TN":"216","TR": "90", "TM": "993", "TC": "1", "TV":"688", "UG": "256", "UA": "380", "US": "1", "UY": "598","UZ": "998", "VA":"379", "VE":"58", "VN": "84", "VG": "1", "VI": "1","VC":"1", "VU":"678", "WS": "685", "WF": "681", "YE": "967", "YT": "262","ZA": "27" , "ZM": "260", "ZW":"263"]
        let countryDialingCode = prefixCodes[countryRegionCode]
        return countryDialingCode!
        
    }

}
