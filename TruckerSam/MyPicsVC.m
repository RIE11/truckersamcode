//
//  MyPicsVC.m
//  DemoMap
//
//  Created by Monica on 2/13/15.
//  Copyright (c) 2015 Monica. All rights reserved.
//

#import "MyPicsVC.h"
#import "TruckerSam_Connection.h"


#define baseUrl                         @"https://www.truckersam.com/webservice/"
#define GetUserImage                    @"get_user_uploaded_images"
#define SaveImageLike                   @"save_image_like"
#define GetWinnerImage                  @"get_all_winner_images"

@interface MyPicsVC ()
@property(nonatomic,retain)IBOutlet UITableView *tblImagelist;
@property(nonatomic)NSIndexPath *indexPathSel;
@property (nonatomic) BOOL loading;
@property(nonatomic)int tag;
@property(nonatomic)int pageNumber;
@property(nonatomic,weak)IBOutlet UILabel *titleLbl;

@property(nonatomic,retain)IBOutlet UIImageView *headerImg;
@property(nonatomic,retain)IBOutlet UIButton *menuBtn;
@property(nonatomic,retain)IBOutlet UIButton *backBtn;
@property(nonatomic,retain)IBOutlet UIImageView *menuImg;
@property(nonatomic,retain)IBOutlet UIImageView *backImg;

@property(nonatomic,retain)NSMutableArray *allImages;
@end

@implementation MyPicsVC

-(NSMutableArray *)allImages
{
    if (!_allImages) {
        _allImages = [NSMutableArray new];
    }
    return _allImages;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _titleLbl.font=[UIFont fontWithName:@"Oswald-Regular" size:26];
    
    [self.tblImagelist registerNib:[UINib nibWithNibName:@"TruckerPicsCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"filterCell"];

    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    _pageNumber=0;
    [self.allImages removeAllObjects];
    
    //set garbage value for tag
    _tag=27345435;
    
    HUD=[MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
    [self GetUserUploadedImage];
}


#pragma mark-Back Action
- (IBAction)BackAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)GetUserUploadedImage
{
    int userID = [[NSUserDefaults standardUserDefaults] stringForKey:@"UserID"].intValue;
    
    NSDictionary *parameters = @{@"user_id":(userID)?[NSString stringWithFormat:@"%i",userID]:@"",
                                 };
    
    [[TruckerSam_Connection sharedManager]getDataForUrl:[baseUrl stringByAppendingString:GetUserImage] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"%@",responseObject);
         
         if ([[responseObject objectForKey:@"status"] isEqualToString:@"failure"])
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"No image found" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
             [alert show];
         }
         else
         {
             
             NSArray* allImgesData = [responseObject objectForKey:@"images"];
             
             for (int i=0; i<[allImgesData count];i++)
             {
                 ImageUserPicsData * imagedata = [ImageUserPicsData new];
                 [imagedata updateValuesWithDictionary:allImgesData[i]];
                 imagedata.imageDownloaded = [[AsyncImageLoader sharedLoader].cache objectForKey:imagedata.imageURL];
                 [self.allImages addObject:imagedata];
             }
             
             _pageNumber=[[responseObject objectForKey:@"nextPageCount"] integerValue];
             [_tblImagelist reloadData];
         }
         
         [HUD setHidden:YES];
         
     }
      failure:^(AFHTTPRequestOperation *operation, NSError *error){
          NSLog(@"Enter into failure is %@",error.localizedDescription);
          
          [HUD setHidden:YES];
          UIAlertView  *alert=nil;
          alert=[[UIAlertView alloc]initWithTitle:@"Error!" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
          [alert show];
      }];
}

#pragma mark-tableview delegate and datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_tblImagelist)
    {
        if (indexPath.row ==_allImages.count)
        {
            return 70;
        }
        else
        {
            ImageUserPicsData * imagedata = self.allImages[indexPath.row];
            if (imagedata) {
                UIImage * image = imagedata.imageDownloaded;
                
                if (image) {
                    float scale = (image.size.width/self.tblImagelist.frame.size.width);
                    
                    float height = image.size.height/scale;
                    
                    float defaultHeight = 184;
                    float heightDiff = height - defaultHeight;
                    
                    height = 360 + heightDiff;
                    
                    return MAX(height, 360);
                }
            }
            return 360;
        }
    }
    else
    {
        return 50;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_allImages.count==0)
        {
            return 0;
        }else
        {
            if (_pageNumber!=0)
            {
                return _allImages.count + 1;
            }
            else{
                return _allImages.count;
            }
        }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        if (indexPath.row ==_allImages.count && _pageNumber!=0)
        {
            return [self loadingCell];
        }
        else
        {
            static NSString *CellIdentifier = @"filterCell";
            
            //====================CUSTOM CELL WITH VIDEO=============================
            TruckerPicsCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            if (cell == nil)
            {
                [[NSBundle mainBundle] loadNibNamed:@"TruckerPicsCell" owner:self options:nil];
            }
            
            [cell.btnLike addTarget:self action:@selector(LikeAction:) forControlEvents:UIControlEventTouchUpInside];
            
            ImageUserPicsData *currentData  =   _allImages[indexPath.row];
            
            cell.lblUserName.text   =   currentData.username;
            cell.lblTime.text       =   [self HourCalculation:currentData.uploadTime];
            cell.lblCaption.text    =    currentData.caption;
            cell.lbllikeUserCount.text=[NSString stringWithFormat:@"%@ Trukers like this",currentData.allLikeCount];
            [cell.imgUploaded setImageURL:[NSURL URLWithString:currentData.image] indexpath:nil];
            
            
            if ([currentData.userImage isEqualToString:@""] || [currentData.userImage length]==0) {
                cell.imgUser.image=[UIImage imageNamed:@"picDefault.png"];
            }else{
                [cell.imgUser setImageURL:[NSURL URLWithString:currentData.userImage] indexpath:nil];
            }
            
            if ([currentData.like isEqualToString:@"yes"])
            {
                cell.btnLike.userInteractionEnabled=NO;
                cell.btnLike.imageView.image=[UIImage imageNamed:@"like-active"];
            }
            else
            {
                if (indexPath.row==_tag)
                {
                    cell.btnLike.userInteractionEnabled=NO;
                    cell.btnLike.imageView.image=[UIImage imageNamed:@"like-active"];
                }
                else{
                    cell.btnLike.userInteractionEnabled=YES;
                    cell.btnLike.imageView.image=[UIImage imageNamed:@"like-inactive"];
                }
            }
            
            cell.btnLike.tag=indexPath.row;
            
            if ([currentData.userId integerValue]== [[NSUserDefaults standardUserDefaults] stringForKey:@"UserID"].integerValue)
            {
                cell.imgShare.hidden=NO;
            }else{
                cell.imgShare.hidden=YES;
            }
            
            //Hide winner image and label
            [cell.imgWinner setHidden:YES];
            [cell.lblWinner setHidden:YES];
            
            return cell;
        }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_tblImagelist)
    {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}

#pragma mark- Scrolling Delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    float endScrolling = scrollView.contentOffset.y + scrollView.frame.size.height;
    if (endScrolling >= scrollView.contentSize.height)
    {
        if (!self.loading  && _pageNumber!=0)
        {
            self.loading=YES;
            [self performSelector:@selector(GetUserUploadedImage) withObject:nil afterDelay:0];
        }
    }
}
- (UITableViewCell *)loadingCell
{
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:nil];
    UIActivityIndicatorView *activityIndicator =[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    activityIndicator.center = cell.center;
    [cell addSubview:activityIndicator];
    
    UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(0,40,320,20)];
    lbl.textColor=[UIColor whiteColor];
    lbl.textAlignment=NSTextAlignmentCenter;
    lbl.font=[UIFont systemFontOfSize:14];
    lbl.text=@"loading more images..";
    [cell addSubview:lbl];
    
    [activityIndicator startAnimating];
    cell.backgroundColor=[UIColor clearColor];
    //cell.tag = kLoadingCellTag;
    return cell;
}

#pragma mark-Like Action
-(void)LikeAction:(UIButton *)sender
{
    _tag=[sender tag];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_tag inSection:0];
    _indexPathSel=indexPath;
    
    HUD=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    ImageUserPicsData * imageData  = self.allImages[_tag];
    
    int userID = [[NSUserDefaults standardUserDefaults] stringForKey:@"UserID"].intValue;
    
    ImageUserPicsData *currentData=_allImages[indexPath.row];
    
    NSDictionary *parameters = @{@"user_id":(userID)?[NSString stringWithFormat:@"%i",userID]:@"",
                                 @"image_id":currentData.imageId,
                                 @"like":@"yes",};
    
    [[TruckerSam_Connection sharedManager]getDataForUrl:[baseUrl stringByAppendingString:SaveImageLike] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"%@",responseObject);
         [sender setUserInteractionEnabled:NO];
         [sender setImage:[UIImage imageNamed:@"like-active"] forState:UIControlStateNormal];
         
         imageData.allLikeCount = [responseObject objectForKey:@"total_like_count"];
         
         NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
         [_tblImagelist reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
         
         [HUD setHidden:YES];
     }
      failure:^(AFHTTPRequestOperation *operation, NSError *error){
          [HUD setHidden:YES];
          NSLog(@"Enter into failure is %@",error.localizedDescription);
          UIAlertView  *alert=nil;
          alert=[[UIAlertView alloc]initWithTitle:@"Error!" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
          [alert show];
      }];
}

-(NSString*)HourCalculation:(NSString*)PostDate
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormat setLocale:[NSLocale systemLocale]];
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [dateFormat setTimeZone:gmt];
    NSDate *ExpDate = [dateFormat dateFromString:PostDate];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSDayCalendarUnit|NSWeekCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit|NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit) fromDate:ExpDate toDate:[NSDate date] options:0];
    NSString *time;
    if(components.year!=0)
    {
        if(components.year==1)
        {
            time=[NSString stringWithFormat:@"%ld year",(long)components.year];
        }
        else
        {
            time=[NSString stringWithFormat:@"%ld years",(long)components.year];
        }
    }
    else if(components.month!=0)
    {
        if(components.month==1)
        {
            time=[NSString stringWithFormat:@"%ld month",(long)components.month];
        }
        else
        {
            time=[NSString stringWithFormat:@"%ld months",(long)components.month];
        }
    }
    else if(components.week!=0)
    {
        if(components.week==1)
        {
            time=[NSString stringWithFormat:@"%ld week",(long)components.week];
        }
        else
        {
            time=[NSString stringWithFormat:@"%ld weeks",(long)components.week];
        }
    }
    else if(components.day!=0)
    {
        if(components.day==1)
        {
            time=[NSString stringWithFormat:@"%ld day",(long)components.day];
        }
        else
        {
            time=[NSString stringWithFormat:@"%ld days",(long)components.day];
        }
    }
    else if(components.hour!=0)
    {
        if(components.hour==1)
        {
            time=[NSString stringWithFormat:@"%ld hour",(long)components.hour];
        }
        else
        {
            time=[NSString stringWithFormat:@"%ld hours",(long)components.hour];
        }
    }
    else if(components.minute!=0)
    {
        if(components.minute==1)
        {
            time=[NSString stringWithFormat:@"%ld min",(long)components.minute];
        }
        else
        {
            time=[NSString stringWithFormat:@"%ld mins",(long)components.minute];
        }
    }
    else if(components.second>=0)
    {
        if(components.second==0)
        {
            time=[NSString stringWithFormat:@"1 sec"];
        }
        else
        {
            time=[NSString stringWithFormat:@"%ld secs",(long)components.second];
        }
    }
    return [NSString stringWithFormat:@"%@ ago",time];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

@implementation ImageUserPicsData

-(void)updateValuesWithDictionary:(NSDictionary*)values
{
    self.userId     = values[@"user_id"];
    self.username   = values[@"username"];
    self.userImage  = values[@"user_image"];
    self.caption    = values[@"caption"];
    self.image      = values[@"image"];
    self.allLikeCount = values[@"all_like_count"];
    self.like       = values[@"like"];
    self.imageId    = values[@"image_id"];
    self.uploadTime = values[@"upload_time"];
    self.imageURL   = [NSURL URLWithString:self.image];
}

@end
