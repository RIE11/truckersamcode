//
//  WebServicesVC.swift
//  APISampleDemo
//
//  Created by Monika on 11/24/16.
//  Copyright © 2016 Gurie. All rights reserved.
//

import Foundation
import Alamofire

class WebServicesVC: NSObject
{
    class var sharedInstance: WebServicesVC {
        struct Static {
            static let instance: WebServicesVC = WebServicesVC()
        }
        return Static.instance
    }
    
    func CallGetAPIFunction(urlString: String?,parameter:String?, success: @escaping ((AnyObject?) -> Void), failure: @escaping ((NSError) -> Void))
    {
        
        print((Constant.GlobalConstants.k_baseUrl) + (urlString!) + "?" + (parameter)!)
        
        Alamofire.request((Constant.GlobalConstants.k_baseUrl) + (urlString!) + "?" + (parameter)!, method: .get ,parameters: nil)
            .responseJSON
            {
                response in
                ///print(response.request!)
                ///print(response.response!)
                ///print(response.result.value!)
                
                if let responseObject = response.result.value
                {
                    print(responseObject)
                    success(responseObject as AnyObject?)
                    ///failure(responseObject as AnyObject?)
                }
               
             }
      
    }
    func CallGetAPIFunctionWithoutBaseURL(urlString: String?,parameter:String?, success: @escaping ((AnyObject?) -> Void), failure: @escaping ((NSError) -> Void))
    {
        
        print((urlString!) + "?" + (parameter)!)
        
        Alamofire.request( (urlString!) + "?" + (parameter)!, method: .get ,parameters: nil)
            .responseJSON
            {
                response in
                ///print(response.request!)
                ///print(response.response!)
                ///print(response.result.value!)
                
                if let responseObject = response.result.value
                {
                    print(responseObject)
                    success(responseObject as AnyObject?)
                    ///failure(responseObject as AnyObject?)
                }
                
        }
        
    }
    func CallPostAPIFunction(urlString: String?,parameter:NSDictionary, success: @escaping ((AnyObject?) -> Void), failure: ((NSError) -> Void))
    {
        
        Alamofire.request(Constant.GlobalConstants.k_baseUrl + urlString!, method: .post, parameters: parameter as? [String : AnyObject], encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            
            print(NSString(data:(response.request?.httpBody!)!, encoding:String.Encoding.utf8.rawValue) as! String)

           /// print(response.request!)
           /// print(response.response!)
           /// print(response.result.value!)
            
            if let responseObject = response.result.value
            {
                print(responseObject)
                success(responseObject as AnyObject?)
                /// failure(error: response.result.error)
            }
            
        }
        
        
    }
}
