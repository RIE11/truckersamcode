//
//  TruckerPicsCell.m
//  DemoMap
//
//  Created by Monica on 2/10/15.
//  Copyright (c) 2015 Monica. All rights reserved.
//

#import "TruckerPicsCell.h"

@implementation TruckerPicsCell

- (void)awakeFromNib
{
    // Initialization code
    
    _imgBg.layer.cornerRadius=4.0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setIndexPath:(NSIndexPath *)indexPath
{
    _indexPath = indexPath;
    self.imgUploaded.indexPath = indexPath;
}

@end
