//
//  NotificationView.swift
//  TruckerSam
//
//  Created by Vikas Singh on 1/5/17.
//  Copyright © 2017 Monica. All rights reserved.
//

import UIKit
import PASImageView
class CustomNotificationView: UIView
{
    @IBOutlet weak var imageViewProfile:    PASImageView!
    @IBOutlet weak var labelName:           UILabel!
    @IBOutlet weak var labelMessage:        UILabel!
    @IBOutlet weak var buttonDown:          UIButton!
    @IBOutlet weak var buttonDidSelect:          UIButton!
    @IBOutlet weak var viewBlack:          UIView!
    @IBOutlet weak var viewProgress:          UIView!
    @IBOutlet weak var labelTimer:          UILabel!
    var view :UIView!
    
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup()
    {
        view = loadViewFromNib()
        view.frame = bounds
        view.backgroundColor = UIColor.black
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView
    {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "CustomNotificationView", bundle: bundle)
        
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
}
