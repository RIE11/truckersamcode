//
//  EPContactsPicker.swift
//  EPContacts
//
//  Created by Prabaharan Elangovan on 12/10/15.
//  Copyright © 2015 Prabaharan Elangovan. All rights reserved.
//

import UIKit
import Contacts
import Alamofire
public protocol EPPickerDelegate
{
    func epContactPicker(_: EPContactsPicker, didContactFetchFailed error: NSError)
    func epContactPicker(_: EPContactsPicker, didCancel error: NSError)
    func epContactPicker(_: EPContactsPicker, didSelectContact contact: EPContact)
    func epContactPicker(_: EPContactsPicker, didSelectMultipleContacts contacts: [EPContact])
    func epContactPicker(_: EPContactsPicker, didSelectAllContacts contacts: [CNContact])
}

public extension EPPickerDelegate
{
    func epContactPicker(_: EPContactsPicker, didContactFetchFailed error: NSError) { }
    func epContactPicker(_: EPContactsPicker, didCancel error: NSError) { }
    func epContactPicker(_: EPContactsPicker, didSelectContact contact: EPContact) { }
    func epContactPicker(_: EPContactsPicker, didSelectMultipleContacts contacts: [EPContact]) { }
    func epContactPicker(_: EPContactsPicker, didSelectAllContacts contacts: [CNContact]) { }
}

typealias ContactsHandler = (_ contacts : [CNContact] , _ error : NSError?) -> Void

public enum SubtitleCellValue
{
    case phoneNumber
    case email
    case birthday
    case organization
}

open class EPContactsPicker: UIViewController, UISearchResultsUpdating, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource
{
    
    // MARK: - Properties
    @IBOutlet weak var tableView: UITableView?
    
    open var contactDelegate:           EPPickerDelegate?
    var contactsStore:                  CNContactStore?
    var resultSearchController =        UISearchController()
    var orderedContacts =               [String: [CNContact]]()
    var sortedContactKeys =             [String]()
    
    var selectedContacts =              [EPContact]()
    var filteredContacts =              [CNContact]()
    var allContactsArray =              [CNContact]()
    
    var subtitleCellValue =             SubtitleCellValue.phoneNumber
    var multiSelectEnabled:             Bool = false //Default is single selection contact
    var isAllSelectEnabled:             Bool = false //Default is All selection contact
    var increment = Int()
    // MARK: - Lifecycle Methods
    
    override open func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        //registerContactCell()
        multiSelectEnabled = true
        subtitleCellValue = SubtitleCellValue.phoneNumber
        //inititlizeBarButtons()
        initializeSearchBar()
        reloadContacts()
    }
    override open func viewWillAppear(_ animated: Bool)
    {
        increment = 0
    }
    
    
    func initializeSearchBar()
    {
        self.resultSearchController =
            (
                {
                    let controller = UISearchController(searchResultsController: nil)
                    controller.searchResultsUpdater = self
                    controller.searchBar.barTintColor = UIColor(red: 25.0/255.0, green: 64.0/255.0, blue: 120.0/255.0, alpha: 1.0)
                    controller.dimsBackgroundDuringPresentation = false
                    controller.searchBar.sizeToFit()
                    controller.searchBar.delegate = self
                    self.tableView?.tableHeaderView = controller.searchBar
                    return controller
            })()
    }
    
//    func inititlizeBarButtons()
//    {
//        
//        let btn = UIButton(type: UIButtonType.custom)
//        btn.setTitle("Select All", for: UIControlState.normal)
//        btn.titleLabel?.font = UIFont.systemFont(ofSize: 14)
//        btn.frame = CGRect(x: 0, y: 0, width: 90, height: 22)
//        btn.addTarget(self, action: #selector(onTouchSelectAllButton), for: UIControlEvents.touchUpInside)
//        let barBtnItem = UIBarButtonItem(customView: btn)
//        self.navigationItem.leftBarButtonItem = barBtnItem
//        
//        if multiSelectEnabled
//        {
//            let btn = UIButton(type: UIButtonType.custom)
//            btn.setBackgroundImage(UIImage(named: "close_noti"), for: UIControlState.normal)
//            btn.frame = CGRect(x: 0, y: 0, width: 22, height: 22)
//            btn.addTarget(self, action: #selector(onTouchCrossButton), for: UIControlEvents.touchUpInside)
//            let barBtnItem = UIBarButtonItem(customView: btn)
//            self.navigationItem.rightBarButtonItem = barBtnItem
//        }
//    }
    
    fileprivate func registerContactCell()
    {
        
        let podBundle = Bundle(for: self.classForCoder)
        if let bundleURL = podBundle.url(forResource: EPGlobalConstants.Strings.bundleIdentifier, withExtension: "bundle")
        {
            if let bundle = Bundle(url: bundleURL)
            {
                let cellNib = UINib(nibName: EPGlobalConstants.Strings.cellNibIdentifier, bundle: bundle)
                tableView?.register(cellNib, forCellReuseIdentifier: "Cell")
            }
            else
            {
                assertionFailure("Could not load bundle")
            }
        }
        else
        {
            let cellNib = UINib(nibName: EPGlobalConstants.Strings.cellNibIdentifier, bundle: nil)
            tableView?.register(cellNib, forCellReuseIdentifier: "Cell")
        }
    }
    
    override open func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Contact Operations
    
    open func reloadContacts() {
        getContacts( {(contacts, error) in
            if (error == nil) {
                DispatchQueue.main.async(execute: {
                    self.tableView?.reloadData()
                })
            }
        })
    }
    
    func getContacts(_ completion:  @escaping ContactsHandler) {
        if contactsStore == nil {
            //ContactStore is control for accessing the Contacts
            contactsStore = CNContactStore()
        }
        let error = NSError(domain: "EPContactPickerErrorDomain", code: 1, userInfo: [NSLocalizedDescriptionKey: "No Contacts Access"])
        
        switch CNContactStore.authorizationStatus(for: CNEntityType.contacts) {
        case CNAuthorizationStatus.denied, CNAuthorizationStatus.restricted:
            //User has denied the current app to access the contacts.
            
            let productName = Bundle.main.infoDictionary!["CFBundleName"]!
            
            let alert = UIAlertController(title: "Unable to access contacts", message: "\(productName) does not have access to contacts. Kindly enable it in privacy settings ", preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {  action in
                self.contactDelegate?.epContactPicker(self, didContactFetchFailed: error)
                completion([], error)
                self.dismiss(animated: true, completion: nil)
            })
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
            
        case CNAuthorizationStatus.notDetermined:
            //This case means the user is prompted for the first time for allowing contacts
            contactsStore?.requestAccess(for: CNEntityType.contacts, completionHandler: { (granted, error) -> Void in
                //At this point an alert is provided to the user to provide access to contacts. This will get invoked if a user responds to the alert
                if  (!granted ){
                    DispatchQueue.main.async(execute: { () -> Void in
                        completion([], error! as NSError?)
                    })
                }
                else{
                    self.getContacts(completion)
                }
            })
            
        case  CNAuthorizationStatus.authorized:
            //Authorization granted by user for this app.
            var contactsArray = [CNContact]()
            
            let contactFetchRequest = CNContactFetchRequest(keysToFetch: allowedContactKeys())
            
            do {
                try contactsStore?.enumerateContacts(with: contactFetchRequest, usingBlock: { (contact, stop) -> Void in
                    //Ordering contacts based on alphabets in firstname
                    contactsArray.append(contact)
                    self.allContactsArray = contactsArray
                    var key: String = "#"
                    //If ordering has to be happening via family name change it here.
                    if let firstLetter = contact.givenName[0..<1] , firstLetter.containsAlphabets() {
                        key = firstLetter.uppercased()
                    }
                    var contacts = [CNContact]()
                    
                    if let segregatedContact = self.orderedContacts[key] {
                        contacts = segregatedContact
                    }
                    contacts.append(contact)
                    self.orderedContacts[key] = contacts
                    
                })
                self.sortedContactKeys = Array(self.orderedContacts.keys).sorted(by: <)
                if self.sortedContactKeys.first == "#" {
                    self.sortedContactKeys.removeFirst()
                    self.sortedContactKeys.append("#")
                }
                completion(contactsArray, nil)
            }
                //Catching exception as enumerateContactsWithFetchRequest can throw errors
            catch let error as NSError {
                print(error.localizedDescription)
            }
            
        }
    }
    
    func allowedContactKeys() -> [CNKeyDescriptor]{
        //We have to provide only the keys which we have to access. We should avoid unnecessary keys when fetching the contact. Reducing the keys means faster the access.
        return [CNContactNamePrefixKey as CNKeyDescriptor,
                CNContactGivenNameKey as CNKeyDescriptor,
                CNContactFamilyNameKey as CNKeyDescriptor,
                CNContactOrganizationNameKey as CNKeyDescriptor,
                CNContactBirthdayKey as CNKeyDescriptor,
                CNContactImageDataKey as CNKeyDescriptor,
                CNContactThumbnailImageDataKey as CNKeyDescriptor,
                CNContactImageDataAvailableKey as CNKeyDescriptor,
                CNContactPhoneNumbersKey as CNKeyDescriptor,
                CNContactEmailAddressesKey as CNKeyDescriptor,
        ]
    }
    
    // MARK: - Table View DataSource
    
    
    open func numberOfSections(in tableView: UITableView) -> Int {
        if resultSearchController.isActive { return 1 }
        return sortedContactKeys.count
    }
    
    
    open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if resultSearchController.isActive { return filteredContacts.count }
        if let contactsForSection = orderedContacts[sortedContactKeys[section]] {
            return contactsForSection.count
        }
        return 0
    }
    
    // MARK: - Table View Delegates
    
    
    open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! EPContactCell
        cell.accessoryType = UITableViewCellAccessoryType.none
        cell.imageViewMark.image = UIImage.init(named: "uncheck_blue")
        cell.contactContainerView.layer.masksToBounds = true
        cell.contactContainerView.layer.cornerRadius = cell.contactContainerView.frame.size.width/2
        //Convert CNContact to EPContact
        let contact: EPContact
        
        if resultSearchController.isActive {
            contact = EPContact(contact: filteredContacts[(indexPath as NSIndexPath).row])
        } else {
            guard let contactsForSection = orderedContacts[sortedContactKeys[(indexPath as NSIndexPath).section]] else {
                assertionFailure()
                return UITableViewCell()
            }
            
            contact = EPContact(contact: contactsForSection[(indexPath as NSIndexPath).row])
        }
        if isAllSelectEnabled
        {
            cell.imageViewMark.image = UIImage.init(named: "check_blue")
        }
        else
        {
            
            if multiSelectEnabled  && selectedContacts.contains(where: { $0.contactId == contact.contactId })
            {
                cell.imageViewMark.image = UIImage.init(named: "check_blue")
            }
            
        }
        cell.updateContactsinUI(contact, indexPath: indexPath, subtitleType: subtitleCellValue)
        return cell
    }
    
    
    open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! EPContactCell
        let selectedContact =  cell.contact!
        if multiSelectEnabled
        {
            if isAllSelectEnabled
            {
                for section in 0..<tableView.numberOfSections
                {
                    for row in 0..<tableView.numberOfRows(inSection: section)
                    {
                        let indexPath = NSIndexPath(row: row, section: section )
                        let cell = tableView.cellForRow(at: indexPath as IndexPath) as! EPContactCell
                        let selectedContact =  cell.contact!
                        if (cell.imageViewMark.image == UIImage.init(named: "check_blue"))
                        {
                            selectedContacts.append(selectedContact)
                        }
                    }
                }
            }
            //Keeps track of enable=ing and disabling contacts
            if (cell.imageViewMark.image == UIImage.init(named: "check_blue"))
            {
                //no image set
                isAllSelectEnabled = false
                cell.imageViewMark.image = UIImage.init(named: "uncheck_blue")
                selectedContacts = selectedContacts.filter(){
                    return selectedContact.contactId != $0.contactId
                }
            }
            else {
                cell.imageViewMark.image = UIImage.init(named: "check_blue")
                selectedContacts.append(selectedContact)
            }
        }
        else {
            //Single selection code
            resultSearchController.isActive = false
            self.dismiss(animated: true, completion: {
                DispatchQueue.main.async {
                    self.contactDelegate?.epContactPicker(self, didSelectContact: selectedContact)
                }
            })
        }
    }
    
    
    open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    
    open func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        if resultSearchController.isActive { return 0 }
        tableView.scrollToRow(at: IndexPath(row: 0, section: index), at: UITableViewScrollPosition.top , animated: false)
        return sortedContactKeys.index(of: title)!
    }
    
    
    open func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        if resultSearchController.isActive { return nil }
        return sortedContactKeys
    }
    
    
    open func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if resultSearchController.isActive { return nil }
        return sortedContactKeys[section]
    }
    
    // MARK: - Button Actions
    
    @IBAction func onTouchSelectAllButton(_ sender: AnyObject)
    {
        
        isAllSelectEnabled = true
        self.tableView?.reloadData()
        
    }
    
    @IBAction func onTouchCrossButton(_ sender: AnyObject)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionInviteButton(_ sender: AnyObject)
    {
        if isAllSelectEnabled
        {
            var stringPhone = NSString()
            var stringName = NSString()
            for contact in allContactsArray
            {
                increment = increment + 1
                if(increment == 1)
                {
                    stringPhone = (contact.phoneNumbers[0].value ).value(forKey: "digits") as! NSString
                    stringName = contact.givenName as NSString
                }
                else
                {
                    let stringP        = "," + "\((contact.phoneNumbers[0].value).value(forKey: "digits")!)"
                    stringPhone        = stringPhone.appending(stringP as String) as NSString
                    
                    let stringN         = "," + contact.givenName as NSString
                    stringName        = stringName.appending(stringN as String) as NSString
                }
            }
            var stringRemoveWhiteSpace = NSString()
            stringRemoveWhiteSpace = stringPhone.replacingOccurrences(of: "(", with: "").replacingOccurrences(of: ")", with: "").replacingOccurrences(of: "-", with: "").replacingOccurrences(of: " ", with: "") as NSString
            toInviteFriends(stringPhone: stringRemoveWhiteSpace.trimmingCharacters(in: .whitespaces) as NSString,stringName: stringName.replacingOccurrences(of: " ", with: "%20") as NSString)
        }
        else
        {
            var stringPhone = NSString()
            var stringName = NSString()
            for contact in selectedContacts
            {
                increment = increment + 1
                if(increment == 1)
                {
                    stringPhone = contact.phoneNumbers[0].phoneNumber as NSString
                    stringName = contact.displayName() as NSString
                }
                else
                {
                    let stringP         = "," + contact.phoneNumbers[0].phoneNumber as NSString
                    stringPhone        = stringPhone.appending(stringP as String) as NSString
                    
                    let stringN         = "," + contact.displayName() as NSString
                    stringName        = stringName.appending(stringN as String) as NSString
                }
            }
            var stringRemoveWhiteSpace = NSString()
            stringRemoveWhiteSpace = stringPhone.replacingOccurrences(of: "(", with: "").replacingOccurrences(of: ")", with: "").replacingOccurrences(of: "-", with: "").replacingOccurrences(of: " ", with: "") as NSString
            toInviteFriends(stringPhone: stringRemoveWhiteSpace.trimmingCharacters(in: .whitespaces) as NSString,stringName: stringName.replacingOccurrences(of: " ", with: "%20") as NSString)
        }
    }
    
    func toInviteFriends(stringPhone : NSString,stringName : NSString)
    {
        let defaults = UserDefaults.standard
        let userID = defaults.integer(forKey: "UserID")
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        WebServicesVC.sharedInstance.CallGetAPIFunction(urlString: Constant.GlobalConstants.k_InviteFriendAPi, parameter:"userID=" + "\(userID)" + "&" + "phone=" + "\(stringPhone)" + "&" + "userName=" + "\(stringName)", success: { (responseObject) in
            
            print(responseObject!)
            if let json  = responseObject // check json result
            {
                if let aStatus = json as? NSDictionary //check json result as dictionary and copy that dictionary in aStatus object
                {
                    if (aStatus["status"] as! NSString == "success") //compare of response code for success
                    {
                        let alert = UIAlertController(title: "Trucker Sam", message: (aStatus["details"] as! NSString) as String, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {  action in
                        }))
                        
                        
                        self.dismiss(animated: true, completion: {
                        })
                       
                        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)

                        _ = self.navigationController?.popViewController(animated: true)
                        
                        self.present(alert, animated: true, completion: nil)

                    }
                }
                else
                {
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)

                    self.increment = 0
                    let alert = UIAlertController(title: "Trucker Sam", message: "Failed to send message", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else
            {
                self.increment = 0
                let alert = UIAlertController(title: "Trucker Sam", message: "Failed to send message", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            } // END
            
        }) { (NSError) in
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)

            self.increment = 0
        }
        
    }
    
    // MARK: - Search Actions
    
    open func updateSearchResults(for searchController: UISearchController)
    {
        if let searchText = resultSearchController.searchBar.text , searchController.isActive {
            
            let predicate: NSPredicate
            if searchText.characters.count > 0 {
                predicate = CNContact.predicateForContacts(matchingName: searchText)
            } else {
                predicate = CNContact.predicateForContactsInContainer(withIdentifier: contactsStore!.defaultContainerIdentifier())
            }
            
            let store = CNContactStore()
            do {
                filteredContacts = try store.unifiedContacts(matching: predicate,
                                                             keysToFetch: allowedContactKeys())
                //print("\(filteredContacts.count) count")
                
                self.tableView?.reloadData()
                
            }
            catch {
                print("Error!")
            }
        }
    }
    
    open func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        DispatchQueue.main.async(execute: {
            self.tableView?.reloadData()
        })
    }
    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        DispatchQueue.main.async(execute: {
            self.tableView?.reloadData()
        })
    }// called when keyboard search button pressed

}
