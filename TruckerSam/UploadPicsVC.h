//
//  UploadPicsVC.h
//  DemoMap
//
//  Created by Monica on 2/10/15.
//  Copyright (c) 2015 Monica. All rights reserved.
//

#import "TruckerPicsCell.h"
#import "MBProgressHUD.h"
@interface UploadPicsVC : UIViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextViewDelegate>
{
    MBProgressHUD *HUD;
}

-(IBAction)ImageUploadAction:(id)sender;
-(IBAction)UploadAction:(id)sender;
-(IBAction)CancelAction:(id)sender;
-(IBAction)TermOfServiceAction:(id)sender;

- (IBAction)BackAction:(id)sender;

@end
