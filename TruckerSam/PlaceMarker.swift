//
//  PlaceMarker.swift
//  Feed Me
//
/*
* Copyright (c) 2015 Razeware LLC
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

import UIKit
import GoogleMaps
import PASImageView

class PlaceMarker: GMSMarker
{
    let place: GooglePlace
    var UpView = PASImageView()
    var markerView = UIImageView()
    init(place: GooglePlace)
    {
        self.place = place
        super.init()
        
        DispatchQueue.global().async
            {
               
                if place.selectedType == "NearByBuddies"
                {
                    let markerImage = UIImage(named: "ballon")!.withRenderingMode(.alwaysOriginal)
                    self.markerView.frame = CGRect(x:0, y:0 , width: 45 , height:65)
                    self.markerView.image = markerImage
                    self.UpView.frame = CGRect(x:5, y:5 , width: 35 , height:35)
                    // UpView.cacheEnabled = false
                    self.UpView.progressColor = UIColor.clear
                    self.UpView.backgroundProgressColor = UIColor.white
                    self.UpView.imageURL(URL: NSURL(string:place.photoReference!)! as URL)
                    //self.UpView.contentMode =  .scaleToFill
                    self.UpView.layer.cornerRadius = self.UpView.frame.size.width/2
                    self.UpView.clipsToBounds = true
                }
                else if place.selectedType == "FromServer"
                {
                    let markerImage = UIImage(named: "boxBg")!.withRenderingMode(.alwaysOriginal)
                    self.markerView.frame = CGRect(x:0, y:0 , width: 45 , height:65)
                    self.markerView.image = markerImage
                    self.UpView.frame = CGRect(x:1, y:1 , width: 43 , height:54)
                    // UpView.cacheEnabled = false
                    //self.UpView.contentMode =  .scaleToFill
                    self.UpView.progressColor = UIColor.clear
                    self.UpView.backgroundProgressColor = UIColor.clear
                    self.UpView.backgroundColor = UIColor.white
                    self.UpView.imageURL(URL: NSURL(string: SelectedURL)! as URL)
                }
                else
                {
                    let markerImage = UIImage(named: "ballon")!.withRenderingMode(.alwaysOriginal)
                    self.markerView.frame = CGRect(x:0, y:0 , width: 45 , height:65)
                    self.markerView.image = markerImage
                    self.UpView.frame = CGRect(x:5, y:5 , width: 35 , height:35)
                    // UpView.cacheEnabled = false
                    self.UpView.progressColor = UIColor.clear
                    self.UpView.backgroundProgressColor = UIColor.clear
                    self.UpView.imageURL(URL: NSURL(string: SelectedURL)! as URL)

                }
                
                DispatchQueue.main.async
                    {
                       
                        let DynamicView=UIView(frame:CGRect(x:0, y:0 , width: 45 , height:65))
                        
                        DynamicView.addSubview(self.markerView)
                        DynamicView.insertSubview( self.UpView, aboveSubview: self.markerView)
                        
                        self.position = place.coordinate
                        self.iconView = DynamicView
                        self.groundAnchor = CGPoint(x: 0.5, y: 1)
                        self.infoWindowAnchor = CGPoint(x: 0.5, y: 0.5)
                        self.appearAnimation = kGMSMarkerAnimationPop
                }
        }
        
        
    }
    
}
    
