//
//  TruckerPicsCell.h
//  DemoMap
//
//  Created by Monica on 2/10/15.
//  Copyright (c) 2015 Monica. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface TruckerPicsCell : UITableViewCell

@property(nonatomic,retain)IBOutlet UILabel *lblUserName;
@property(nonatomic,retain)IBOutlet UILabel *lblTime;
@property(nonatomic,retain)IBOutlet UILabel *lblCaption;
@property(nonatomic,retain)IBOutlet UILabel *lbllikeUserCount;
@property(nonatomic,retain)IBOutlet AsyncImageView *imgUploaded;
@property(nonatomic,retain)IBOutlet AsyncImageView *imgUser;
@property(nonatomic,retain)IBOutlet UIButton *btnLike;
@property(nonatomic,retain)IBOutlet UIImageView *imgBg;
@property(nonatomic,retain)IBOutlet UIImageView *imgShare;
@property (nonatomic, retain) NSIndexPath* indexPath;
@property(nonatomic,retain)IBOutlet UIImageView *imgWinner;
@property(nonatomic,retain)IBOutlet UILabel *lblWinner;
@end
