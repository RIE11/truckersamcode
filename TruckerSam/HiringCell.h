//
//  HiringCell.h
//  DemoMap
//
//  Created by Monica on 2/2/15.
//  Copyright (c) 2015 Monica. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HiringCell : UITableViewCell
@property(nonatomic,retain)IBOutlet UIView *viewHiring;
@property(nonatomic,retain)IBOutlet UIButton *closeBtn;
@property(nonatomic,retain)IBOutlet UILabel *lblJobTitle;
@property(nonatomic,retain)IBOutlet UILabel *lblCompanyName;
@property(nonatomic,retain)IBOutlet UILabel *lblJobType;
@property(nonatomic,retain)IBOutlet UILabel *lblJobLocation;
@property(nonatomic,retain)IBOutlet UILabel *lblCompanyName1;
@property(nonatomic,retain)IBOutlet UILabel *lblJobType1;
@property(nonatomic,retain)IBOutlet UILabel *lblJobLocation1;
@property(nonatomic,retain)IBOutlet UILabel *lblJobDescp;
@property(nonatomic,retain)IBOutlet UIWebView *webJobDescp;
@property(nonatomic,retain)IBOutlet UILabel *lblPhone;
@property(nonatomic,retain)IBOutlet UILabel *lblEmail;
@property(nonatomic,retain)IBOutlet UILabel *lblName;
@property(nonatomic,retain)IBOutlet UILabel *lblRefrence;

@property(nonatomic,retain)IBOutlet UILabel *lblPhone1;
@property(nonatomic,retain)IBOutlet UILabel *lblEmail1;
@property(nonatomic,retain)IBOutlet UILabel *lblName1;
@property(nonatomic,retain)IBOutlet UILabel *lblRefrence1;

@property(nonatomic,retain)IBOutlet UIButton *webBtn;
@end
