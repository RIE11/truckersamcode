//
//  TruckAppControllerVC.swift
//  TruckerSam
//
//  Created by Monika on 1/4/17.
//  Copyright © 2017 Monica. All rights reserved.
//

import Foundation
import UIKit
import PASImageView


class TruckAppControllerVC: BaseVC , UITableViewDelegate , UITableViewDataSource , DLHamburguerViewControllerDelegate
{
    @IBOutlet weak var tableView : UITableView!
    var entities : NSMutableArray!
    var categories : NSMutableArray!
    
    var json : [Dictionary<String, AnyObject>]!
   
    @IBOutlet weak var imageViewProfile    : PASImageView!
    @IBOutlet weak var buttonUpdateAccount : UIButton!
    
    
    @IBOutlet weak var headerBackgroundView:UIView!
    @IBOutlet weak var headerLogoView:UIImageView!
    var SideMenuView = SideMenuViewController()


    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        ///set background color
        headerBackgroundView.backgroundColor = Constant.GlobalConstants.kColor_ScreenBackground
        self.view.backgroundColor = Constant.GlobalConstants.kColor_ScreenBackground
        
        self.findHamburguerViewController()?.menuDirection = .right
        self.findHamburguerViewController()?.delegate = self

        entities = NSMutableArray()
        categories = NSMutableArray()
        json =  [Dictionary<String, AnyObject>]()
       
      
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        GetMenuControlAPI()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        let defaults = UserDefaults.standard
        imageViewProfile.imageURL(URL: NSURL(string: (defaults.object(forKey: "UserImage") as! String?)!)! as URL)
        imageViewProfile.layer.cornerRadius = imageViewProfile.frame.size.width/2
        imageViewProfile.clipsToBounds = true

    }

    @IBAction func actionLayersButton(_ sender: AnyObject)
    {
        self.findHamburguerViewController()?.showMenuViewController()

    }
    
    func selectParticularRow()
    {
    
     
        let indexPath = IndexPath(row: SelectedIndex, section: 0)
        self.tableView.selectRow(at: indexPath, animated: true, scrollPosition: UITableViewScrollPosition.middle)
    }

     
    
    //MARK: - table Delegates
    private func numberOfSectionsInTableView(tableView: UITableView!) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.json.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let cell = self.tableView(self.tableView, cellForRowAt: indexPath) as! TruckAppVCCell
        cell.frame.size.height = cell.collectionViewHeightConstraint.constant + 20
        print(cell.frame.size.height)
        return cell.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell: TruckAppVCCell = tableView.dequeueReusableCell(withIdentifier: "TruckAppVCCell") as! TruckAppVCCell
        
        cell.setCollectionViewDataSourceDelegate(dataSourceDelegate: self , index: (indexPath as NSIndexPath).row)
        cell.catNameLbl!.text  = self.json[indexPath.row]["name"] as? String
        
        return cell
    }
    
    //MARK: - API
    func GetMenuControlAPI()
    {
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        WebServicesVC.sharedInstance.CallGetAPIFunction(urlString: Constant.GlobalConstants.k_GetAppControls, parameter:"appID=\(Constant.GlobalConstants.kAppID)" , success: { (responseObject) in
            
            print(responseObject!)
           /* {
                menu =     (
                    {
                        categories =             (
                            {
                                "area_name" = "Women in Trucking";
                                "area_position" = 1;
                                embededCode = 0;
                                "icon_area" = 1;
                                "icon_area_id" = 7;
                                "icon_content" = "http://womenintrucking.org/";
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1472013069390868.png";
                                "icon_name" = "  WEBSITE";
                                "icon_order" = 1;
                                id = 12;
                                "page_content_type" = WEBVIEW;
                                "type_id" = 4;
                                "type_name" = "Content page";
                        },
                            {
                                "area_name" = "Women in Trucking";
                                "area_position" = 1;
                                embededCode = 0;
                                "icon_area" = 1;
                                "icon_area_id" = 7;
                                "icon_content" = 8884649482;
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1472012630959885.png";
                                "icon_name" = "Call Us";
                                "icon_order" = 2;
                                id = 14;
                                "type_id" = 3;
                                "type_name" = CLICK2CALL;
                        },
                            {
                                "area_name" = "Women in Trucking";
                                "area_position" = 1;
                                embededCode = 0;
                                "icon_area" = "<null>";
                                "icon_area_id" = 7;
                                "icon_content" = "http://www.womenintrucking.org/join";
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1483031645611745.png";
                                "icon_name" = " Join Us";
                                "icon_order" = 3;
                                id = 46;
                                "page_content_type" = WEBVIEW;
                                "type_id" = 4;
                                "type_name" = "Content page";
                        },
                            {
                                "area_name" = "Women in Trucking";
                                "area_position" = 1;
                                embededCode = 0;
                                "icon_area" = "<null>";
                                "icon_area_id" = 7;
                                "icon_content" = "https://wt.memberclicks.net/press-releases";
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1482475429797138.png";
                                "icon_name" = " Press Releases";
                                "icon_order" = 4;
                                id = 50;
                                "page_content_type" = WEBVIEW;
                                "type_id" = 4;
                                "type_name" = "Content page";
                        },
                            {
                                "area_name" = "Women in Trucking";
                                "area_position" = 1;
                                embededCode = 0;
                                "icon_area" = "<null>";
                                "icon_area_id" = 7;
                                "icon_content" = "https://wt.memberclicks.net/index.php?option=com_content&view=article&id=22:member-landing-page&cati";
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1482762568237388.png";
                                "icon_name" = "MEMBERS ONLY";
                                "icon_order" = 6;
                                id = 55;
                                "page_content_type" = WEBVIEW;
                                "type_id" = 4;
                                "type_name" = "Content page";
                        },
                            {
                                "area_name" = "Women in Trucking";
                                "area_position" = 1;
                                embededCode = 0;
                                "icon_area" = "<null>";
                                "icon_area_id" = 7;
                                "icon_content" = "https://wt.memberclicks.net/member-of-the-month";
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1483031184124233.png";
                                "icon_name" = " Member Of The Month";
                                "icon_order" = 7;
                                id = 47;
                                "page_content_type" = WEBVIEW;
                                "type_id" = 4;
                                "type_name" = "Content page";
                        },
                            {
                                "area_name" = "Women in Trucking";
                                "area_position" = 1;
                                embededCode = 0;
                                "icon_area" = "<null>";
                                "icon_area_id" = 7;
                                "icon_content" = "http://www.womenintrucking.org/ellen-s-blog";
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1482475210626613.png";
                                "icon_name" = "Ellen's Blog";
                                "icon_order" = 8;
                                id = 56;
                                "page_content_type" = WEBVIEW;
                                "type_id" = 4;
                                "type_name" = "Content page";
                        },
                            {
                                "area_name" = "Women in Trucking";
                                "area_position" = 1;
                                embededCode = 0;
                                "icon_area" = "<null>";
                                "icon_area_id" = 7;
                                "icon_content" = "https://wt.memberclicks.net/events-calendar";
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1482475468348777.png";
                                "icon_name" = "  Event Calender";
                                "icon_order" = 9;
                                id = 49;
                                "page_content_type" = WEBVIEW;
                                "type_id" = 4;
                                "type_name" = "Content page";
                        },
                            {
                                "area_name" = "Women in Trucking";
                                "area_position" = 1;
                                embededCode = 0;
                                "icon_area" = "<null>";
                                "icon_area_id" = 7;
                                "icon_content" = "http://www.truckersagainsttrafficking.org/";
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1477896614386590.png";
                                "icon_name" = "TRUCKERS AGAINST TRAFFICKING";
                                "icon_order" = 10;
                                id = 75;
                                "page_content_type" = WEBVIEW;
                                "type_id" = 4;
                                "type_name" = "Content page";
                        },
                            {
                                "area_name" = "Women in Trucking";
                                "area_position" = 1;
                                embededCode = 0;
                                "icon_area" = 1;
                                "icon_area_id" = 7;
                                "icon_content" = "https://www.youtube.com/user/womenintruckingorg";
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1472012730770454.png";
                                "icon_name" = " Youtube";
                                "icon_order" = 10;
                                id = 19;
                                "page_content_type" = WEBVIEW;
                                "type_id" = 4;
                                "type_name" = "Content page";
                        },
                            {
                                "area_name" = "Women in Trucking";
                                "area_position" = 1;
                                embededCode = 0;
                                "icon_area" = 1;
                                "icon_area_id" = 7;
                                "icon_content" = "https://twitter.com/womenintrucking";
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1472012702898451.png";
                                "icon_name" = " Twitter";
                                "icon_order" = 11;
                                id = 18;
                                "page_content_type" = WEBVIEW;
                                "type_id" = 4;
                                "type_name" = "Content page";
                        },
                            {
                                "area_name" = "Women in Trucking";
                                "area_position" = 1;
                                embededCode = 0;
                                "icon_area" = 1;
                                "icon_area_id" = 7;
                                "icon_content" = "https://www.facebook.com/groups/125991767442727";
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1472012661552493.png";
                                "icon_name" = " FaceBook";
                                "icon_order" = 12;
                                id = 17;
                                "page_content_type" = WEBVIEW;
                                "type_id" = 4;
                                "type_name" = "Content page";
                        }
                        );
                        name = "Women in Trucking";
                },
                    {
                        categories =             (
                            {
                                "area_name" = Social;
                                "area_position" = 2;
                                embededCode = 0;
                                "icon_area" = 2;
                                "icon_area_id" = 1;
                                "icon_content" = "PHOTO_ALBUM";
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1472013933225873.png";
                                "icon_name" = " Pics Wall";
                                "icon_order" = 1;
                                id = 42;
                                "type_id" = 6;
                                "type_name" = "Photo Album";
                        },
                            {
                                "area_name" = Social;
                                "area_position" = 2;
                                embededCode = 0;
                                "icon_area" = 2;
                                "icon_area_id" = 1;
                                "icon_content" = Games;
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1472013543593132.png";
                                "icon_name" = " Games";
                                "icon_order" = 2;
                                id = 29;
                                "page_content_type" = "PAGE_CONTENT";
                                "type_id" = 4;
                                "type_name" = "Content page";
                        },
                            {
                                "area_name" = Social;
                                "area_position" = 2;
                                embededCode = 0;
                                "icon_area" = 2;
                                "icon_area_id" = 1;
                                "icon_content" = "http://dashradio.com/";
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1472013521143592.png";
                                "icon_name" = " Radio";
                                "icon_order" = 3;
                                id = 28;
                                "page_content_type" = WEBVIEW;
                                "type_id" = 4;
                                "type_name" = "Content page";
                        },
                            {
                                "area_name" = Social;
                                "area_position" = 2;
                                embededCode = 0;
                                "icon_area" = "<null>";
                                "icon_area_id" = 1;
                                "icon_content" = Videos;
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1472011846622747.png";
                                "icon_name" = Videos;
                                "icon_order" = 4;
                                id = 11;
                                "page_content_type" = "PAGE_CONTENT";
                                "type_id" = 4;
                                "type_name" = "Content page";
                        },
                            {
                                "area_name" = Social;
                                "area_position" = 2;
                                embededCode = 0;
                                "icon_area" = 2;
                                "icon_area_id" = 1;
                                "icon_content" = "Breaking News";
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1472013495549881.png";
                                "icon_name" = "   News";
                                "icon_order" = 5;
                                id = 27;
                                "page_content_type" = "PAGE_CONTENT";
                                "type_id" = 4;
                                "type_name" = "Content page";
                        },
                            {
                                "area_name" = Social;
                                "area_position" = 2;
                                embededCode = 0;
                                "icon_area" = 2;
                                "icon_area_id" = 1;
                                "icon_content" = 911;
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1472013639657113.png";
                                "icon_name" = " 911";
                                "icon_order" = 6;
                                id = 34;
                                "type_id" = 3;
                                "type_name" = CLICK2CALL;
                        },
                            {
                                "area_name" = Social;
                                "area_position" = 2;
                                embededCode = 0;
                                "icon_area" = 2;
                                "icon_area_id" = 1;
                                "icon_content" = "http://www.couponsherpa.com/category/restaurant/";
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1472013572935479.png";
                                "icon_name" = "  Coupons";
                                "icon_order" = 7;
                                id = 30;
                                "page_content_type" = WEBVIEW;
                                "type_id" = 4;
                                "type_name" = "Content page";
                        },
                            {
                                "area_name" = Social;
                                "area_position" = 2;
                                embededCode = 0;
                                "icon_area" = "<null>";
                                "icon_area_id" = 1;
                                "icon_content" = "Social Online Store";
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1472013595621277.png";
                                "icon_name" = " Online Store";
                                "icon_order" = 8;
                                id = 31;
                                "page_content_type" = "PAGE_CONTENT";
                                "type_id" = 4;
                                "type_name" = "Content page";
                        },
                            {
                                "area_name" = Social;
                                "area_position" = 2;
                                embededCode = 1;
                                "icon_area" = "<null>";
                                "icon_area_id" = 1;
                                "icon_content" = "https://www.truckersam.com/staticpages/content_details/1053?webservice=yes";
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1482475032950890.png";
                                "icon_name" = " ONLINE STORE";
                                "icon_order" = 9;
                                id = 53;
                                "page_content_type" = WEBVIEW;
                                "type_id" = 4;
                                "type_name" = "Content page";
                        }
                        );
                        name = Social;
                },
                    {
                        categories =             (
                            {
                                "area_name" = "Professional Services ";
                                "area_position" = 3;
                                embededCode = 0;
                                "icon_area" = "<null>";
                                "icon_area_id" = 11;
                                "icon_content" = "DAT TruckersEdge";
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1473937476493260.png";
                                "icon_name" = "Get Free Month";
                                "icon_order" = 1;
                                id = 52;
                                "page_content_type" = "PAGE_CONTENT";
                                "type_id" = 4;
                                "type_name" = "Content page";
                        },
                            {
                                "area_name" = "Professional Services ";
                                "area_position" = 3;
                                embededCode = 0;
                                "icon_area" = 3;
                                "icon_area_id" = 11;
                                "icon_content" = "HIRING_CENTER";
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1472013907756428.png";
                                "icon_name" = " Hiring Center";
                                "icon_order" = 2;
                                id = 41;
                                "type_id" = 5;
                                "type_name" = "Hiring Center";
                        },
                            {
                                "area_name" = "Professional Services ";
                                "area_position" = 3;
                                embededCode = 0;
                                "icon_area" = "<null>";
                                "icon_area_id" = 11;
                                "icon_content" = 8;
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1475649944360760.png";
                                "icon_name" = "Trucks Stop";
                                "icon_order" = 3;
                                id = 57;
                                "type_id" = 2;
                                "type_name" = "Specific layer on the map";
                        },
                            {
                                "area_name" = "Professional Services ";
                                "area_position" = 3;
                                embededCode = 0;
                                "icon_area" = "<null>";
                                "icon_area_id" = 11;
                                "icon_content" = 101;
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1484574347900040.png";
                                "icon_name" = "Truck Repair";
                                "icon_order" = 4;
                                id = 58;
                                "type_id" = 2;
                                "type_name" = "Specific layer on the map";
                        },
                            {
                                "area_name" = "Professional Services ";
                                "area_position" = 3;
                                embededCode = 0;
                                "icon_area" = "<null>";
                                "icon_area_id" = 11;
                                "icon_content" = 102;
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1475650063205506.png";
                                "icon_name" = "Truck Towing\t";
                                "icon_order" = 5;
                                id = 63;
                                "type_id" = 2;
                                "type_name" = "Specific layer on the map";
                        },
                            {
                                "area_name" = "Professional Services ";
                                "area_position" = 3;
                                embededCode = 0;
                                "icon_area" = "<null>";
                                "icon_area_id" = 11;
                                "icon_content" = 103;
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1475657065590451.png";
                                "icon_name" = "Truck Tires";
                                "icon_order" = 6;
                                id = 65;
                                "type_id" = 2;
                                "type_name" = "Specific layer on the map";
                        }
                        );
                        name = "Professional Services ";
                },
                    {
                        categories =             (
                            {
                                "area_name" = Finance;
                                "area_position" = 4;
                                embededCode = 0;
                                "icon_area" = 4;
                                "icon_area_id" = 10;
                                "icon_content" = "http://www.kqzyfj.com/click-8118228-11818271-1452787774000";
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1472013766369774.png";
                                "icon_name" = " Working Capital";
                                "icon_order" = 1;
                                id = 32;
                                "page_content_type" = WEBVIEW;
                                "type_id" = 4;
                                "type_name" = "Content page";
                        },
                            {
                                "area_name" = Finance;
                                "area_position" = 4;
                                embededCode = 0;
                                "icon_area" = 4;
                                "icon_area_id" = 10;
                                "icon_content" = "http://www.calculator.net/financial-calculator.html";
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1472013690386624.png";
                                "icon_name" = " Calculator";
                                "icon_order" = 2;
                                id = 33;
                                "page_content_type" = WEBVIEW;
                                "type_id" = 4;
                                "type_name" = "Content page";
                        },
                            {
                                "area_name" = Finance;
                                "area_position" = 4;
                                embededCode = 0;
                                "icon_area" = 4;
                                "icon_area_id" = 10;
                                "icon_content" = "Financial Articles";
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1472013664257735.png";
                                "icon_name" = " Articles";
                                "icon_order" = 3;
                                id = 35;
                                "page_content_type" = "PAGE_CONTENT";
                                "type_id" = 4;
                                "type_name" = "Content page";
                        },
                            {
                                "area_name" = Finance;
                                "area_position" = 4;
                                embededCode = 0;
                                "icon_area" = "<null>";
                                "icon_area_id" = 10;
                                "icon_content" = "https://www.freescore360.com/us/6019/373f031/t420/303/lp/303-a87c/?sid=AFFM005209Z&id=3456&ord=1&app";
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1472013858754793.png";
                                "icon_name" = " Fix Your Credit Score";
                                "icon_order" = 4;
                                id = 38;
                                "page_content_type" = WEBVIEW;
                                "type_id" = 4;
                                "type_name" = "Content page";
                        },
                            {
                                "area_name" = Finance;
                                "area_position" = 4;
                                embededCode = 0;
                                "icon_area" = "<null>";
                                "icon_area_id" = 10;
                                "icon_content" = "https://valuedloans.com/?c=12900&source=160763&leadid=411008638";
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1472013962357615.png";
                                "icon_name" = "Get Valued Loans";
                                "icon_order" = 5;
                                id = 39;
                                "page_content_type" = WEBVIEW;
                                "type_id" = 4;
                                "type_name" = "Content page";
                        },
                            {
                                "area_name" = Finance;
                                "area_position" = 4;
                                embededCode = 0;
                                "icon_area" = "<null>";
                                "icon_area_id" = 10;
                                "icon_content" = 93;
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1475649251794198.png";
                                "icon_name" = ATM;
                                "icon_order" = 6;
                                id = 62;
                                "type_id" = 2;
                                "type_name" = "Specific layer on the map";
                        }
                        );
                        name = Finance;
                },
                    {
                        categories =             (
                            {
                                "area_name" = "Home Services";
                                "area_position" = 5;
                                embededCode = 0;
                                "icon_area" = "<null>";
                                "icon_area_id" = 9;
                                "icon_content" = 2052080129;
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1472013184492873.png";
                                "icon_name" = "Get Contractor -  24/7 SUPPORT";
                                "icon_order" = 1;
                                id = 24;
                                "type_id" = 3;
                                "type_name" = CLICK2CALL;
                        },
                            {
                                "area_name" = "Home Services";
                                "area_position" = 5;
                                embededCode = 0;
                                "icon_area" = "<null>";
                                "icon_area_id" = 9;
                                "icon_content" = "http://contractors99.com/landing/yp-tmp?campaign=TruckerSam";
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1472013385937757.png";
                                "icon_name" = "Get Contractor";
                                "icon_order" = 2;
                                id = 25;
                                "page_content_type" = WEBVIEW;
                                "type_id" = 4;
                                "type_name" = "Content page";
                        },
                            {
                                "area_name" = "Home Services";
                                "area_position" = 5;
                                embededCode = 0;
                                "icon_area" = 5;
                                "icon_area_id" = 9;
                                "icon_content" = "http://www.wikihow.com/Choose-a-Home-Improvement-Contractor";
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1474977797580937.png";
                                "icon_name" = Tips;
                                "icon_order" = 3;
                                id = 59;
                                "page_content_type" = WEBVIEW;
                                "type_id" = 4;
                                "type_name" = "Content page";
                        }
                        );
                        name = "Home Services";
                },
                    {
                        categories =             (
                            {
                                "area_name" = Health;
                                "area_position" = 6;
                                embededCode = 0;
                                "icon_area" = 6;
                                "icon_area_id" = 8;
                                "icon_content" = "Health Online Store";
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1472012893487607.png";
                                "icon_name" = " Online Store";
                                "icon_order" = 1;
                                id = 22;
                                "page_content_type" = "PAGE_CONTENT";
                                "type_id" = 4;
                                "type_name" = "Content page";
                        },
                            {
                                "area_name" = Health;
                                "area_position" = 6;
                                embededCode = 0;
                                "icon_area" = "<null>";
                                "icon_area_id" = 8;
                                "icon_content" = 8885983439;
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1472013417380818.png";
                                "icon_name" = "Health Insurance";
                                "icon_order" = 2;
                                id = 26;
                                "type_id" = 3;
                                "type_name" = CLICK2CALL;
                        },
                            {
                                "area_name" = Health;
                                "area_position" = 6;
                                embededCode = 0;
                                "icon_area" = 6;
                                "icon_area_id" = 8;
                                "icon_content" = "Health Video Tips";
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1472012772838002.png";
                                "icon_name" = "Video Tips";
                                "icon_order" = 3;
                                id = 20;
                                "page_content_type" = "PAGE_CONTENT";
                                "type_id" = 4;
                                "type_name" = "Content page";
                        },
                            {
                                "area_name" = Health;
                                "area_position" = 6;
                                embededCode = 0;
                                "icon_area" = 6;
                                "icon_area_id" = 8;
                                "icon_content" = "https://www.fmcsa.dot.gov/safety/driver-safety/cmv-driving-tips-driver-fatigue";
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1472012973768517.png";
                                "icon_name" = " Tips";
                                "icon_order" = 4;
                                id = 23;
                                "page_content_type" = WEBVIEW;
                                "type_id" = 4;
                                "type_name" = "Content page";
                        },
                            {
                                "area_name" = Health;
                                "area_position" = 6;
                                embededCode = 0;
                                "icon_area" = 6;
                                "icon_area_id" = 8;
                                "icon_content" = 100;
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1474272316517019.png";
                                "icon_name" = "Medical Center";
                                "icon_order" = 5;
                                id = 54;
                                "type_id" = 2;
                                "type_name" = "Specific layer on the map";
                        },
                            {
                                "area_name" = Health;
                                "area_position" = 6;
                                embededCode = 0;
                                "icon_area" = "<null>";
                                "icon_area_id" = 8;
                                "icon_content" = 109;
                                "icon_image" = "https://www.truckersam.com/uploaded_images/appcontrol/1475644848206797.png";
                                "icon_name" = Pharmacy;
                                "icon_order" = 6;
                                id = 61;
                                "type_id" = 2;
                                "type_name" = "Specific layer on the map";
                        }
                        );
                        name = Health;
                }
                );
                status = success;
            }*/
            
            if (responseObject?["status"] as! String == "success")
            {
            self.json = responseObject!["menu"] as! [Dictionary<String, AnyObject>]
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
            self.tableView.reloadData()
                
            }
            else
            {
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)

            }
        
        }) { (NSError) in
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
        }
    }
    
    @IBAction func actionContactsButton(_ sender: AnyObject)
    {
        let viewController = self.storyboard!.instantiateViewController(withIdentifier: "EPContactsPicker") as! EPContactsPicker
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func BackAction(sender: AnyObject)
    {
        
       _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionUpdateAccountButton(_ sender: AnyObject)
    {
        let viewController = self.storyboard!.instantiateViewController(withIdentifier: "UpdateAccountVC") as! UpdateAccountVC
        self.navigationController?.pushViewController(viewController, animated: true)
    }

}

// MARK: - Collection View Data source and Delegate
extension TruckAppControllerVC:UICollectionViewDataSource,UICollectionViewDelegate {
    
    //MARK: - collection view Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return (self.json[collectionView.tag]["categories"] as! [Dictionary<String, AnyObject>]).count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryVCCell", for: indexPath as IndexPath) as! CategoryVCCell
        
        cell.layer.borderWidth = 1.0
        cell.layer.borderColor = UIColor(red: 25.0/255.0, green: 64.0/255.0, blue: 120.0/255.0, alpha: 1.0).cgColor
        cell.layer.cornerRadius = 5.0
        
        let categories = self.json[collectionView.tag]["categories"] as! [Dictionary<String, AnyObject>]
        cell.contentNameLbl.text = (categories[indexPath.row]["icon_name"] as! String).uppercased()
        cell.contentimageView.imageURL(URL: NSURL(string: (categories[indexPath.row]["icon_image"] as! String?)!)! as URL)
        
        return cell
    }
    
   func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
   {
    let categories = self.json[collectionView.tag]["categories"] as! [Dictionary<String, AnyObject>]
    let typeName = categories[indexPath.row]["type_name"] as! String
    print(typeName)
    let typeID = categories[indexPath.row]["type_id"] as! String
    let iconContent = categories[indexPath.row]["icon_content"] as! String

    switch  typeID{
        
    case "2":  //Specific layer on the map
        
        var allLayers : AllLayers
        
        for i in 0..<arrayLayers.count
        {
            allLayers = arrayLayers[i] as! AllLayers
            if allLayers.idString == iconContent
            {
                SelectedParentId = allLayers.parent_id /// 86(Google) or 2(self added)
                SelectedId = allLayers.idString
                SelectedName = allLayers.name
                SelectedURL = allLayers.around_me_logo
                directionEnabled = false
                SelectedIndex = i
               
                if SelectedParentId == "86"
                {
                    let notificationName = Notification.Name("NotificationGoogle")
                    NotificationCenter.default.post(name: notificationName, object: nil)
                }
                else
                {
                    let notificationName = Notification.Name("NotificationServer")
                    NotificationCenter.default.post(name: notificationName, object: nil)
                    
                }
                let notificationName = Notification.Name("ToReloadTableView")
                NotificationCenter.default.post(name: notificationName, object: nil)

                _ = self.navigationController?.popViewController(animated: true)
            }

        }
        break
        
    case "3":  //CLICK2CALL
        self.callNumber(phoneNumber: categories[indexPath.row]["icon_content"] as! String)
        break
        
    case "4":  //Content page
        let page_content_type = categories[indexPath.row]["page_content_type"] as! String
       
        if page_content_type == "WEBVIEW"
        {
            let webViewObj = self.storyboard?.instantiateViewController(withIdentifier: "WebViewVC") as? WebViewVC
            webViewObj?.titleStr = categories[indexPath.row]["icon_name"] as! String
            webViewObj?.linkStr = categories[indexPath.row]["icon_content"] as! String
            self.navigationController?.pushViewController(webViewObj!, animated: true)
        }
        else
        {
            
            let alert = UIAlertController(title:"", message: "In Process", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            
            
//            let contentPage = self.storyboard?.instantiateViewController(withIdentifier: "ContentPageVC") as? ContentPageVC
//            contentPage?.titleStr = categories[indexPath.row]["icon_name"] as! String
//            contentPage?.linkStr = categories[indexPath.row]["icon_content"] as! String
//            self.navigationController?.pushViewController(contentPage!, animated: true)
            
        }
        break
        
    case "5":  //Hiring Center //Objective c
        let webViewObj = self.storyboard?.instantiateViewController(withIdentifier: "HiringCentreVC") as? HiringCentreVC
        self.navigationController?.pushViewController(webViewObj!, animated: true)
        break
        
    case "6":  //Photo Album //Objective c
        
        let tab = self.storyboard?.instantiateViewController(withIdentifier: "tabBar") as? UITabBarController
        tab?.selectedIndex = 2
        self.navigationController?.pushViewController(tab!, animated: true)
        break
        
    default: break
        
    }
    
    }
    
    private func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        return CGSize(width:95.0, height:85.0)
    }
    
    
    func callNumber(phoneNumber:String) {
        if let phoneCallURL:NSURL = NSURL(string: "tel://\(phoneNumber)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL as URL)) {
                application.openURL(phoneCallURL as URL);
            }
        }
    }
}



class TruckAppVCCell: UITableViewCell
{
    @IBOutlet weak var catNameLbl : UILabel!
    @IBOutlet weak var collectionView : UICollectionView!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        collectionView.layer.borderWidth = 2.0
        collectionView.layer.borderColor = UIColor(red: 25.0/255.0, green: 64.0/255.0, blue: 120.0/255.0, alpha: 1.0).cgColor
        collectionView.layer.cornerRadius = 10.0
    }
    
    
    func setCollectionViewDataSourceDelegate(dataSourceDelegate delegate: UICollectionViewDelegate & UICollectionViewDataSource, index: NSInteger) {
        collectionView.dataSource = delegate
        collectionView.delegate = delegate
        collectionView.tag = index
        collectionView.reloadData()
       
        collectionViewHeightConstraint.constant = (95 * CGFloat(collectionView.numberOfItems(inSection: 0)/3)) + 25
    }
    
    func setCollectionViewDataSourceDelegate(dataSourceDelegate delegate: UICollectionViewDelegate & UICollectionViewDataSource, indexPath: IndexPath) {
        collectionView.dataSource = delegate
        collectionView.delegate = delegate
       // collectionView.indexPath = indexPath
        collectionView.tag = indexPath.section
        collectionView.reloadData()
    }
}

class CategoryVCCell: UICollectionViewCell
{
    @IBOutlet weak var contentNameLbl : UILabel!
    @IBOutlet weak var contentimageView : PASImageView!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
}

