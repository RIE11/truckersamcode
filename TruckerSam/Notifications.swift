//
//  Notifications.swift
//  TruckerSam
//
//  Created by Gurleen Singh on 1/27/17.
//  Copyright © 2017 Monica. All rights reserved.
//

import UIKit

class Notifications: NSObject
{

    var type_name              :String!
    var ownerID              :String!
    var id                  :String!
    var image              :String!
    var Name              :String!
    var appID              :String!
    var type              :String!
    var message             :String!
    
    func getAllNotifications(dictionary:NSDictionary)
    {
        
        /*
         {"type_name":"message","ownerID":"1114","id":null,"image":"https:\/\/www.truckersam.com\/image\/1483506757208375.jpeg","Name":"kamal1","appID":"22","type":"1"}         */
        
        type_name = ((dictionary["type_name"] == nil) ? "" : dictionary["type_name"]) as? String
        
        ownerID = ((dictionary["id"]  == nil) ? "" : dictionary["id"]) as? String
        
        id = ((dictionary["notificaton_id"]  == nil) ? "" : dictionary["notificaton_id"]) as? String
        
        image = ((dictionary["image"]  == nil) ? "" : dictionary["image"]) as? String
        
        Name = ((dictionary["Name"]  == nil) ? "" : dictionary["Name"]) as? String
        
        appID = ((dictionary["appID"]  == nil) ? "" : dictionary["appID"]) as? String
        
        type = ((dictionary["type"]  == nil) ? "" : dictionary["type"]) as? String
        
        message = ((dictionary["message"]  == nil) ? "" : dictionary["message"]) as? String
        
    }
}
