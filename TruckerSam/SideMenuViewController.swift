//
//  DLDemoMenuViewController.swift
//  DLHamburguerMenu
//
//  Created by Nacho on 5/3/15.
//  Copyright (c) 2015 Ignacio Nieto Carvajal. All rights reserved.
//

import UIKit
import PASImageView

class SideMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    // outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var switchBuddies: UISwitch!

    let defaults = UserDefaults.standard

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        /// Define identifier
        let notificationName = Notification.Name("ToReloadTableView")
        /// Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(SideMenuViewController.methodOfReceivedNotification), name: notificationName, object: nil)
        
        if UserDefaults.standard.object(forKey: "NearByBuddies") as? Bool == true
        {
           switchBuddies.setOn(true, animated: false)
            let notificationName = Notification.Name("NotificationBuddies")
            NotificationCenter.default.post(name: notificationName, object: nil)
        }
        else
        {
            switchBuddies.setOn(false, animated: false)

        }
        
        // Do any additional setup after loading the view.
    }
    func methodOfReceivedNotification()
    {
        self.tableView.reloadData()
    }
    
    @IBAction func BuddiesSwitchAction(sender: UISwitch)
    {

        if sender.isOn
        {
        defaults.set(true, forKey: "NearByBuddies")
        }
        else
        {
        defaults.set(false, forKey: "NearByBuddies")
        }
        
        
        if SelectedParentId == ""
        {
            let notificationName = Notification.Name("NotificationBuddies")
            NotificationCenter.default.post(name: notificationName, object: nil)
        }
        else
        {
            if SelectedParentId == "86"
            {
                let notificationName = Notification.Name("NotificationGoogle")
                NotificationCenter.default.post(name: notificationName, object: nil)
            }
            else
            {
                
                let notificationName = Notification.Name("NotificationServer")
                NotificationCenter.default.post(name: notificationName, object: nil)
            }
        }
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    /// MARK: UITableViewDelegate&DataSource methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrayLayers.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell: LayersCell = tableView.dequeueReusableCell(withIdentifier: "LayersCell") as! LayersCell
        let allLayers : AllLayers
        
        allLayers = arrayLayers[indexPath.row] as! AllLayers
        cell.labelLayerName?.text = allLayers.name
        cell.imageViewLayers.backgroundColor = UIColor.clear
        cell.imageViewLayers.backgroundProgressColor = UIColor.clear
        cell.imageViewLayers.progressColor = UIColor.clear
        cell.imageViewLayers.contentMode = .scaleToFill
        cell.imageViewLayers.imageURL(URL: NSURL(string: allLayers.around_me_logo)! as URL)
        
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if SelectedIndex == indexPath.row
        {
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: UITableViewScrollPosition.none)
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        /*
         {
         "around_me_logo" = "https://www.truckersam.com/uploaded_images/appimages/appbanner/1475644718502940.png";
         id = 109;
         keyword = Pharmacy;
         masterOfWL = 0;
         name = Pharmacy;
         "parent_id" = 86;
         status = A;
         }
         */
        
        
        if SelectedIndex == indexPath.row
        {
            tableView.deselectRow(at: indexPath, animated: true)

            SelectedIndex = -1
            SelectedParentId = ""/// 86(Google) or 2(self added)
            SelectedId = ""
            SelectedName = ""
            SelectedURL = ""
            directionEnabled = false
        
            let notificationName = Notification.Name("NotificationBuddies")
            NotificationCenter.default.post(name: notificationName, object: nil)
            
        }
       else
        {
           
            let allLayers : AllLayers
            allLayers = arrayLayers[indexPath.row] as! AllLayers
            
            SelectedIndex = indexPath.row
            SelectedParentId = allLayers.parent_id /// 86(Google) or 2(self added)
            SelectedId = allLayers.idString
            SelectedName = allLayers.name
            SelectedURL = allLayers.around_me_logo
            directionEnabled = false
            
            if SelectedParentId == "86"
            {
                let notificationName = Notification.Name("NotificationGoogle")
                NotificationCenter.default.post(name: notificationName, object: nil)
            }
            else
            {
                let notificationName = Notification.Name("NotificationServer")
                NotificationCenter.default.post(name: notificationName, object: nil)
                
            }
        }
        
        if let hamburguerViewController = self.findHamburguerViewController()
        {
            
            hamburguerViewController.hideMenuViewControllerWithCompletion(nil)
            
        }

       
    }
  
    /// MARK: - Navigation
    func mainNavigationController() -> DLHamburguerNavigationController
    {
        return self.storyboard?.instantiateViewController(withIdentifier: "DLDemoNavigationViewController") as! DLHamburguerNavigationController
    }
    
}

class LayersCell: UITableViewCell
{
    @IBOutlet weak var imageViewLayers: PASImageView!
    @IBOutlet weak var labelLayerName : UILabel!
    
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
}
