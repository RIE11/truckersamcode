//
//  RegistrationVC.swift
//  TruckerSam
//
//  Created by Monika on 12/26/16.
//  Copyright © 2016 Monica. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import CoreLocation
import CoreTelephony

class RegistrationVC: BaseVC  , UITextFieldDelegate
{
    
    @IBOutlet weak var headerBackgroundView:UIView!
    @IBOutlet weak var headerLogoView:UIImageView!
    @IBOutlet weak var scrollView:UIScrollView!
    
    @IBOutlet weak var mynameTxt:UITextField!
    @IBOutlet weak var countrycodeTxt:UITextField!
    @IBOutlet weak var phoneTxt:UITextField!
    @IBOutlet weak var emailTxt:UITextField!
    @IBOutlet weak var ques1Lbl:UILabel!
    @IBOutlet weak var ques2Lbl:UILabel!
    @IBOutlet weak var ans1Btn:UIButton!
    @IBOutlet weak var ans2Btn:UIButton!
    @IBOutlet weak var continueBtn:UIButton!
    
    @IBOutlet weak var ques1Stackview:UIStackView!
    @IBOutlet weak var ques2Stackview:UIStackView!
    var popUpVerificationCodetextField: UITextField!
    
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    var latitude: Double!
    var longitude: Double!
    //    let userModel = UserModel()
    let loc = GetLocation()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.findHamburguerViewController()?.gestureEnabled = false // To disable side view bar
        countrycodeTxt.delegate = self
        phoneTxt.delegate = self
        
        //set background color
        headerBackgroundView.backgroundColor = Constant.GlobalConstants.kColor_ScreenBackground
        scrollView.backgroundColor = Constant.GlobalConstants.kColor_ScreenBackground
        ans1Btn.tag = Constant.GlobalConstants.kAnswer1_ButtonTag
        ans2Btn.tag = Constant.GlobalConstants.kAnswer2_ButtonTag
        
        // get country code for current location
        let currentLocale = NSLocale.current
        let countryCode = (currentLocale as AnyObject).object(forKey:NSLocale.Key.countryCode) as! String//get the set country name, code of your iphone
        print("country code is \(countryCode)")
        countrycodeTxt.text = getCountryCallingCode(countryRegionCode: countryCode)
        
        
        latitude  =  0.00
        longitude =  0.00
        locManager.delegate = self
        locManager.desiredAccuracy = kCLLocationAccuracyBest
        locManager.requestWhenInUseAuthorization()
        locManager.requestAlwaysAuthorization()
        locManager.startMonitoringSignificantLocationChanges()
        
        if CLLocationManager.locationServicesEnabled() {
            locManager.startUpdatingLocation()
        }
        
        self.HideQuestionsView()
        //Call Question Api
        CheckQuestionAPI()
    }
    
    override func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocation = manager.location
        print(currentLocation.coordinate.latitude)
        print(currentLocation.coordinate.longitude)
        
        latitude = currentLocation.coordinate.latitude
        longitude = currentLocation.coordinate.longitude
        
        self.locManager.stopUpdatingLocation()
    }
    
    @IBAction func AnswerAction(sender: UIButton)
    {
        switch  sender.tag {
        case Constant.GlobalConstants.kAnswer1_ButtonTag:
            let btnTitlestr:String = (sender.titleLabel?.text)! as String
            switch  btnTitlestr {
            case "?":
                sender .setTitle("YES", for: UIControlState.normal)
                break
            case "YES":
                sender .setTitle("NO", for: UIControlState.normal)
                break
            case "NO":
                sender .setTitle("YES", for: UIControlState.normal)
                break
            default:
                break
            }
        case Constant.GlobalConstants.kAnswer2_ButtonTag:
            let btnTitlestr:String = (sender.titleLabel?.text)! as String
            switch  btnTitlestr {
            case "?":
                sender .setTitle("YES", for: UIControlState.normal)
                break
            case "YES":
                sender .setTitle("NO", for: UIControlState.normal)
                break
            case "NO":
                sender .setTitle("YES", for: UIControlState.normal)
                break
            default:
                break
            }
        default:
            break
        }
        
    }
    
    @IBAction func ContinueAction(sender: AnyObject)
    {
        if mynameTxt.text!.isEmpty || countrycodeTxt.text!.isEmpty || phoneTxt.text!.isEmpty
        {
            self.AlertViewOneButtonPress(ButtonTitle: "OK", message: "Please fill the empty fields.")
            return
        }
        if phoneTxt.text!.characters.count < 10
        {
            self.AlertViewOneButtonPress(ButtonTitle: "OK", message: "Please enter valid phone number")
            return
        }
        if ans1Btn.titleLabel?.text == "?"
        {
            self.AlertViewOneButtonPress(ButtonTitle: "OK", message: "Please answer first question")
            return
        }
        if ans2Btn.titleLabel?.text == "?"
        {
            self.AlertViewOneButtonPress(ButtonTitle: "OK", message: "Please answer second question")
            return
        }
        if !(emailTxt.text!.isEmpty){
            if !isValidEmail(testStr: emailTxt.text!)
            {
                self.AlertViewOneButtonPress(ButtonTitle: "OK", message: "Please enter valid email id.")
                return
            }}
        if latitude == 0.00 || longitude == 0.00
        {
            self.AlertViewOneButtonPress(ButtonTitle: "OK", message: "Please enable your location services from Setting")
            return
        }
        
        UserRegistrationAPI()
    }
    
    func UserRegistrationAPI()
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        WebServicesVC.sharedInstance.CallPostAPIFunction(urlString:Constant.GlobalConstants.k_RegistrationApi , parameter:
            [ "truck_type":"",
              "email":self.emailTxt.text!,
              "i_am_a":"",
              "username":"",
              "full_name":self.mynameTxt.text!,
              "state_base":"",
              "dob":"",
              "app_id":"\(Constant.GlobalConstants.kAppID)",
                "answer1":(self.ans1Btn.titleLabel?.text!)! as String,
                "answer2":(self.ans2Btn.titleLabel?.text!)! as String,
                "question1":self.ques1Lbl.text!,
                "question2":self.ques2Lbl.text!,
                "phone": self.countrycodeTxt.text! + "-" + self.phoneTxt.text!,
                "id":"",
                "device_user_agent":"",
                "web_user_id":"",
                "how_hear_about_us":"",
                "deviceID":UIDevice.current.identifierForVendor!.uuidString,
                "latitude":"\(latitude!)",
                "longitude":"\(longitude!)",
                "token":((appDelegate.deviceTokenToPass) != nil) ? (appDelegate.deviceTokenToPass) as String:"123456",
                "appName":Constant.GlobalConstants.kAppName,
                "image":"",
                "ios":"1"
                ]
            , success: { (responseObject) in
                
                //print(responseObject!)
                
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                
                if (responseObject?["status"] as! String == "success")
                {
                    let json =  responseObject as! NSDictionary
                    
                    let defaults = UserDefaults.standard
                    defaults.set(json["full_name"] as! String, forKey: "UserName")
                    defaults.set(json["code"] as! Int, forKey: "code")
                    defaults.set(json["email"] as! String, forKey: "email")
                    defaults.set(self.countrycodeTxt.text! + "-" + self.phoneTxt.text!, forKey: "phone")
                    defaults.set(json["question1"] as! String, forKey: "question1")
                    defaults.set(json["question2"] as! String, forKey: "question2")
                    defaults.set(json["answer1"] as! String, forKey: "answer1")
                    defaults.set(json["answer2"] as! String, forKey: "answer2")
                    defaults.set(json["app_id"] as! String, forKey: "app_id")
                    defaults.set(true, forKey: "UserTrackLocation")
                    defaults.set(true, forKey: "NearByBuddies")
                    self.SetIntegerUserDefault(ForKey:"UserID", ForValue:json["userId"] as! Int)
                    
                    self.ResendPopView()
                }
                
        }) { (NSError) in
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
 
        }
    }
    
    func ResendPopView() {
        
        let alert = UIAlertController(title: "VERIFY PHONE NUMBER", message: "Hi \(mynameTxt.text!) \n Please enter below the verification code received on mobile number \(countrycodeTxt.text!)-\(phoneTxt.text!)", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addTextField(configurationHandler: configurationTextField)
        alert.addAction(UIAlertAction(title: "Resend", style: UIAlertActionStyle.default, handler: { action in
            
            self.ResendCodeAPI()
        }))
        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Verify", style: UIAlertActionStyle.default, handler: { action in
            
            self.PushToNextView()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func ResendCodeAPI()
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)

        let defaults = UserDefaults.standard
        
        WebServicesVC.sharedInstance.CallGetAPIFunction(urlString: Constant.GlobalConstants.k_ResendCodeApi, parameter:"id=" + "\(defaults.object(forKey: "UserID") as! Int)" + "&&" + "appID=" + "\( defaults.object(forKey: "app_id") as! String)" + "&&" + "fullName=" + "\(defaults.object(forKey: "UserName") as! String)" + "&&" + "appName=" + "Women_in_Trucking" + "&&" + "phone=" + "\(defaults.object(forKey: "phone") as! String)", success: { (responseObject) in
            
            print(responseObject!)
            
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
            
            if (responseObject?["status"] as! String == "success")
            {
                let json =  responseObject?["msg"] as! NSDictionary
                
                let defaults = UserDefaults.standard
                defaults.set(json["code"] as! Int, forKey: "code")
                
                self.ResendPopView()
            }
            
        }) { (NSError) in
           
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)

        }
    }
    
    func PushToNextView()
    {
        popUpVerificationCodetextField.resignFirstResponder()
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let defaults = UserDefaults.standard
        
        if (popUpVerificationCodetextField.text! == "\(defaults.object(forKey: "code") as! Int)")
        {
            //Save isverification bit to user default when verification code is verified
            SetIntegerUserDefault(ForKey:"isVerified", ForValue:1)
            
            // then push view to next screen
            let uploadPicVCObj = self.storyboard?.instantiateViewController(withIdentifier: "UploadPicVC") as? UploadPicVC
            self.navigationController?.pushViewController(uploadPicVCObj!, animated: true)
            
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
            
        }else{
            
            
            let alert = UIAlertController(title: "Verification Failed", message:"Please enter correct Verification code", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
                self.ResendPopView()
            }))
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)

            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func CheckQuestionAPI()
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        WebServicesVC.sharedInstance.CallGetAPIFunction(urlString: Constant.GlobalConstants.k_QuestionApi, parameter:"appID=\(Constant.GlobalConstants.kAppID)" , success: { (responseObject) in
            
            print(responseObject!)
            
            
            
            let json = responseObject as! NSDictionary
            
            if (json["status"] as! String == "success")
            {
                self.ShowQuestionsView()
                
                let questions =  json["questions"] as! NSDictionary
                
                if (questions["question1"] != nil){
                    self.ques1Lbl.text = questions["question1"] as AnyObject? as? String}
                
                if (questions["question2"] != nil){
                    self.ques2Lbl.text = questions["question2"] as AnyObject? as? String
                }
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)

            }else{
                
                self.HideQuestionsView()
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)

            }
            
            
        }) { (NSError) in
            
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
            
        }
    }
    
    
    func HideQuestionsView(){
        
        self.ques1Stackview.isHidden = true
        self.ques2Stackview.isHidden = true
    }
    
    func ShowQuestionsView(){
        
        self.ques1Stackview.isHidden = false
        self.ques2Stackview.isHidden = false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if(textField == countrycodeTxt)
        {
            if ((textField.text?.characters.count)! > 3  && range.length == 0)
            {
                return false // return NO to not change text
            }
        }
        if(textField == phoneTxt)
        {
            if ((textField.text?.characters.count)! > 9  && range.length == 0)
            {
                return false // return NO to not change text
            }
        }
        
        return true
    }
    
    
    func configurationTextField(textField: UITextField!)
    {
        print("generating the TextField")
        textField.placeholder = "Enter verification code"
        popUpVerificationCodetextField = textField
        popUpVerificationCodetextField.keyboardType = UIKeyboardType.phonePad

    }
}
