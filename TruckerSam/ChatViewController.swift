//
//  ChatViewController.swift
//  TruckerSam
//
//  Created by Vikas Singh on 1/9/17.
//  Copyright © 2017 Monica. All rights reserved.
//

import UIKit
import PASImageView


class ChatViewController: BaseVC,UITableViewDelegate,UITableViewDataSource {

    var fromNotificationView : Bool!
    var arrayMessage  = NSMutableArray()
    @IBOutlet weak var textField:UITextField!
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var viewTextField:UIView!
    @IBOutlet weak var buttonWithBulb:UIButton!
    
    var selectedOwnerId = String()
    var selectedUserName = String()
    var selectedUserImage = String()

    @IBOutlet weak var app_userImage: PASImageView!
    @IBOutlet weak var app_userName : UILabel!

    
    override func viewDidLoad()
    {
        super.viewDidLoad()

       
        if fromNotificationView == true
        {
            buttonWithBulb.isHidden = true
        }
        
        isAdminbool = false
        
       
        
       
        app_userName?.text = "\(selectedUserName)"
        
        
        app_userImage.backgroundColor = UIColor.clear
        app_userImage.layer.cornerRadius = app_userImage.frame.size.width/2
        app_userImage.clipsToBounds = true
        app_userImage.imageURL(URL: NSURL(string: selectedUserImage) as! URL)
        
        
        
        let placesData = UserDefaults.standard.object(forKey: String("\(self.selectedOwnerId)" + "\(self.GetIntegerUserDefault(ForKey: "UserID"))")) as? NSData
        arrayMessage = NSMutableArray()
        arrayMessage = (NSKeyedUnarchiver.unarchiveObject(with: placesData as! Data) as? NSMutableArray)!
        
        self.viewTextField.layer.borderWidth = 1.0
        self.viewTextField.layer.borderColor = UIColor.lightGray.cgColor
        self.viewTextField.layer.cornerRadius = 8.0
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrayMessage.count
    }
    
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let dictionary = self.arrayMessage.object(at: indexPath.row) as! NSDictionary
        let cell = UITableViewCell()
        if let from = dictionary["from"]
        {
            let cell: OpponentChatViewCell = tableView.dequeueReusableCell(withIdentifier: "OpponentChatViewCell") as! OpponentChatViewCell
            
            let chatBubbleDataOpponent = ChatBubbleData(text: from as? String, image:UIImage(named: "chatImage3.jpg"), date: NSDate(), type: .Opponent)
            let chatBubbleOpponent = ChatBubble(data: chatBubbleDataOpponent, startY: 5)
            cell.addSubview(chatBubbleOpponent)
            
            return cell
        }
        
        if let to = dictionary["to"]
        {
            let cell: MineChatViewCell = tableView.dequeueReusableCell(withIdentifier: "MineChatViewCell") as! MineChatViewCell
            
            let chatBubbleDataMine = ChatBubbleData(text: to as? String, image: nil, date: NSDate(), type: .Mine)
            let chatBubbleMine = ChatBubble(data: chatBubbleDataMine, startY: 4)
            cell.addSubview(chatBubbleMine)
            return cell
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
    }
    @IBAction func CrossSenderAction(sender: UIButton)
    {
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers ;
        for aViewController in viewControllers {
            if(aViewController is ViewController)
            {
                self.navigationController!.popToViewController(aViewController, animated: true);
            }
        }
    }
    @IBAction func BulbSenderAction(sender: UIButton)
    {
        self.navigationController!.popViewController(animated: true)
    }
    
    @IBAction func SendMsgAction(sender: UIButton)
    {
        if textField.text!.isEmpty
        {
            self.AlertViewOneButtonPress(ButtonTitle: "OK", message: "Kindly write some text to send")
            return
        }
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
      
        WebServicesVC.sharedInstance.CallPostAPIFunction(urlString:Constant.GlobalConstants.k_SendMessage , parameter:
            [ "message":"\(textField.text!)",
                "id":"\(self.GetIntegerUserDefault(ForKey: "UserID"))",
                "buddies":"\(selectedOwnerId)",
                "appId":"\(Constant.GlobalConstants.kAppID)",
                "hasAdmin":"0",
                "appName":"\(Constant.GlobalConstants.kAppName)"]
            
            , success: { (responseObject) in
                
                print(responseObject!)
                if (responseObject?["status"] as! String == "success")
                {
                    var namesDictionary: Dictionary<String, String> = [:]
                    namesDictionary["to"] = self.textField.text!
                    
                    let key = "\(self.selectedOwnerId)" + "\(self.GetIntegerUserDefault(ForKey: "UserID"))"
                    print(key)
                    
                    if UserDefaults.standard.object(forKey: key) != nil
                    {
                        let placesData = UserDefaults.standard.object(forKey: key) as? NSData
                        
                        if let placesData = placesData
                        {
                            let placesArray = NSKeyedUnarchiver.unarchiveObject(with: placesData as Data) as? NSMutableArray
                            placesArray?.add(namesDictionary)
                            self.arrayMessage = placesArray!
                            let placesData = NSKeyedArchiver.archivedData(withRootObject: placesArray!)
                            UserDefaults.standard.set(placesData, forKey: key)
                            self.tableView.reloadData()
                            
                        }
                    }
                    else
                    {
                        let array = NSMutableArray()
                        array.add(namesDictionary)
                        self.arrayMessage = array
                        
                        let placesData = NSKeyedArchiver.archivedData(withRootObject: array)
                        UserDefaults.standard.set(placesData, forKey:key)
                        self.tableView.reloadData()
                        
                    }
                    
                    
                    
                    let alert = UIAlertController(title: "Trucker Sam", message: (responseObject?["msg"] as! NSString) as String, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {  action in
                        _ =  self.navigationController?.popViewController(animated: true)
                    }))
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                    self.present(alert, animated: true, completion: nil)
                    
                }
                else
                {
                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)

                    self.AlertViewOneButtonPress(ButtonTitle: "OK", message: responseObject?["msg"] as! String)
                    
                }
                
        }) { (NSError) in
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
        }
    }
}
class MineChatViewCell: UITableViewCell
{
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
}
class OpponentChatViewCell: UITableViewCell
{
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
}
