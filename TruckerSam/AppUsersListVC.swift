//
//  AppUsersListVC.swift
//  TruckerSam
//
//  Created by Monika on 12/30/16.
//  Copyright © 2016 Monica. All rights reserved.
//

import Foundation
import UIKit
import PASImageView

protocol AppUsersListVCDelegate
{
    func GetApp_UsersID(app_userID : String , app_userName : String , isAdmin : Bool)
}

class AppUsersListVC: BaseVC, UITableViewDelegate , UITableViewDataSource
{
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var inviteBtn : UIButton!
    
    var entities : NSMutableArray!
    var userImageAry : NSMutableArray!
    var delegate : AppUsersListVCDelegate?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        entities = NSMutableArray()
        userImageAry = NSMutableArray()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        inviteBtn.sizeToFit()
        inviteBtn.titleEdgeInsets = UIEdgeInsetsMake(0, -(inviteBtn.imageView?.frame.size.width)!, 0, (inviteBtn.imageView?.frame.size.width)!);
        inviteBtn.imageEdgeInsets = UIEdgeInsetsMake(0, (inviteBtn.titleLabel?.frame.size.width)!, 0, -(inviteBtn.titleLabel?.frame.size.width)!);
        inviteBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        
        //Add Admin User
        let app_userModel = AppUsersModel()
        
        app_userModel.App_UserIdStr      = "0"
        app_userModel.App_UserNameStr    = Constant.GlobalConstants.kAppName
        app_userModel.App_UserPhoneStr   = Constant.GlobalConstants.kAppName
        
        self.userImageAry.add("")
        self.entities.add(app_userModel)
        
        GetUsersList()
    }
    
    //MARK: - table Delegates
    private func numberOfSectionsInTableView(tableView: UITableView!) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.entities!.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell: AppUsersVCCell = tableView.dequeueReusableCell(withIdentifier: "AppUsersVCCell") as! AppUsersVCCell
        
        let appUserModel = (entities[indexPath.row] as! AppUsersModel)
        
        cell.app_userName!.text  = appUserModel.App_UserNameStr
        cell.app_userPhone!.text = appUserModel.App_UserPhoneStr
        
        
            DispatchQueue.main.async
                {
                    if(indexPath.row == 0)
                    {
                        cell.app_adminImage.image = UIImage(named: "splash_logo")
                        cell.app_userImage.isHidden = true
                        cell.app_adminImage.isHidden = false

                    }
                    else
                    {
                         cell.app_adminImage.isHidden = true
                         cell.app_userImage.isHidden = false

                        cell.app_userImage.backgroundColor = UIColor.clear
                        cell.app_userImage.layer.cornerRadius = cell.app_userImage.frame.size.width/2
                        cell.app_userImage.clipsToBounds = true
                        cell.app_userImage.imageURL(URL: NSURL(string: self.userImageAry.object(at: indexPath.row) as! String)! as URL)
                    }
                }
      
      
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let appUserModel = (entities[indexPath.item] as! AppUsersModel)
        
        if((self.delegate) != nil)
        {
            if(indexPath.row == 0)
            {
               self.delegate?.GetApp_UsersID(app_userID: appUserModel.App_UserIdStr, app_userName: appUserModel.App_UserNameStr, isAdmin:true)
            }
            else
            {
                self.delegate?.GetApp_UsersID(app_userID: appUserModel.App_UserIdStr, app_userName: appUserModel.App_UserNameStr, isAdmin:false)
            }
           _ = self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    //MARK: - API method
    func GetUsersList()
    {
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        WebServicesVC.sharedInstance.CallGetAPIFunction(urlString: Constant.GlobalConstants.k_GetAppUsers, parameter:"id=\(self.GetIntegerUserDefault(ForKey: "UserID"))" + "&" + "appID=\(Constant.GlobalConstants.kAppID)", success: { (responseObject) in
            
            print(responseObject!)
            
            for Json in responseObject?["details"] as! [Dictionary<String, AnyObject>]
            {
                let app_userModel = AppUsersModel()
                
                app_userModel.App_UserIdStr      = Json["id"] as! String!
                app_userModel.App_UserNameStr    = Json["full_name"] as! String!
                app_userModel.App_UserImgStr     = Json["image"] as! String!
                app_userModel.App_UserPhoneStr   = Json["phone"] as! String!
                
                self.entities.add(app_userModel)
                self.userImageAry.add(Json["image"] as! String!)
            }
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
            self.tableView.reloadData()
           
        }) { (NSError) in
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
        }
    }
    
    //MARK: - Button actions
    @IBAction func actionContactsButton(_ sender: AnyObject)
    {
        let viewController = self.storyboard!.instantiateViewController(withIdentifier: "EPContactsPicker") as! EPContactsPicker
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func BackAction(sender: AnyObject)
    {
       _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

class AppUsersVCCell: UITableViewCell
{
    @IBOutlet weak var app_userImage: PASImageView!
    @IBOutlet weak var app_userName : UILabel!
    @IBOutlet weak var app_userPhone: UILabel!
    @IBOutlet weak var app_adminImage: UIImageView!

    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
}
