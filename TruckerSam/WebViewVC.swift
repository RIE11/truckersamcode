//
//  WebViewVC.swift
//  TruckerSam
//
//  Created by Monika on 12/28/16.
//  Copyright © 2016 Monica. All rights reserved.
//

import Foundation
import UIKit

@objc class WebViewVC: BaseVC , UIWebViewDelegate , UIScrollViewDelegate
{
    @IBOutlet weak var headerBackgroundView:UIView!
    @IBOutlet weak var headerLogoView:UIImageView!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var btnBackward: UIButton!
    @IBOutlet weak var btnForward: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    
    var titleStr: String!
    var linkStr: String!
   
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //set background color
        headerBackgroundView.backgroundColor = Constant.GlobalConstants.kColor_ScreenBackground
        self.view.backgroundColor = Constant.GlobalConstants.kColor_ScreenBackground
        titleLbl.text = titleStr
        
        webView.delegate = self
        if let url = URL(string: linkStr.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!) 
        {
           
            let request = URLRequest(url: url)
            webView.loadRequest(request)
        }
        btnBackward.isEnabled = false
        btnForward.isEnabled = false
    }
    
    func webViewDidStartLoad(_ webView: UIWebView)
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
    }
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
        
        if webView.canGoBack == true{
            btnBackward.isEnabled = true
        }else{
            btnBackward.isEnabled = false
        }
        if webView.canGoForward == true
        {
            btnForward.isEnabled = true
        }else{
            btnForward.isEnabled = false
        }
        
        webView.scrollView.delegate = self // set delegate method of UISrollView
        webView.scrollView.maximumZoomScale = 10 // set as you want.
        webView.scrollView.minimumZoomScale = 1 // set as you want.
        
        //// Below two line is for iOS 6, If your app only supported iOS 7 then no need to write this.
        webView.scrollView.zoomScale = 2
        webView.scrollView.zoomScale = 1
    }
    
    @IBAction func LeftArrowAction(sender: AnyObject)
    {
        webView.goBack()
    }
    
    @IBAction func RightArrowAction(sender: AnyObject)
    {
        webView.goForward()
    }
    
    @IBAction func BackAction(sender: AnyObject)
    {
       _ = self.navigationController?.popViewController(animated: true)
    }
}
