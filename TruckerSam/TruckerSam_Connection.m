//
//  Compassion_Connection.m
//  Compassion App
//
//  Created by Monica on 10/13/14.
//
//

#import "TruckerSam_Connection.h"
#import "AFNetworking.h"

static TruckerSam_Connection *_sharedManager = nil;

@implementation TruckerSam_Connection

+ (TruckerSam_Connection *)sharedManager
{
    @synchronized([TruckerSam_Connection class])
    {
        if (!_sharedManager)
            _sharedManager = [[self alloc] init];
        
        return _sharedManager;
    }
    
    return nil;
}
+(id)alloc
{
    @synchronized([TruckerSam_Connection class])
    {
        NSAssert(_sharedManager == nil, @"Attempted to allocate a second instance of a singleton.");
        _sharedManager = [[super alloc] init];
        return _sharedManager;
    }
    
    return nil;
}

-(id)init
{
    return [super init];
}

#pragma mark- Web service Common method

-(void)getDataForUrl:(NSString *)URLString
          parameters:(NSDictionary *)parameters
             success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
             failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager POST:URLString parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(operation,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Enter into coonection class  is %@",error.localizedDescription);
         failure(operation,error);
         
     }];
}

@end
