//
//  UserModel.swift
//  TruckerSam
//
//  Created by Monika on 12/27/16.
//  Copyright © 2016 Monica. All rights reserved.
//

import Foundation
import UIKit

class UserModel:NSObject
{
    var UserIdInt           :Int!
    var UserNameStr         :String!
    var UserImgStr          :String!
    var verificationCodeInt :Int!
    var countryCodeStr      :String!
    var phoneStr            :String!
    var emailStr            :String!
    var ques1Str            :String!
    var ques2Str            :String!
    var ans1Str             :String!
    var ans2Str             :String!
    var appIDStr            :String!
    var appNameStr          :String!
    var UserTrackLocation   :Bool!

}
